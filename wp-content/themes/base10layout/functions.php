<?php
	add_image_size('image-bannner', 960, 350, true);
	add_image_size('image-galeria', 240, 185, true);
	add_image_size('image-galeria-thumb', 75, 60, true);
	add_image_size('image-frota', 130, 100, true);
	add_image_size('image-filiais', 165, 130, true);
	add_image_size('image-noticia', 100, 80, true);

	add_filter('pre_site_transient_update_core', create_function('$a', "return null;"));



	function hide_help() {
	    echo '<style type="text/css">
	            #contextual-help-link-wrap { display: none !important; }
	            #wp-admin-bar-new-content { display: none !important; }
	          </style>';
	}
	add_action('admin_head', 'hide_help');

	function wps_admin_bar() {
	    global $wp_admin_bar;
	    $wp_admin_bar->remove_menu('wp-logo');
	    $wp_admin_bar->remove_menu('about');
	    $wp_admin_bar->remove_menu('wporg');
	    $wp_admin_bar->remove_menu('documentation');
	    $wp_admin_bar->remove_menu('support-forums');
	    $wp_admin_bar->remove_menu('feedback');
	    $wp_admin_bar->remove_menu('view-site');
	}
	add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );

	class SH_Walker_TaxonomyDropdown extends Walker_CategoryDropdown{
 
    function start_el(&$output, $category, $depth, $args) {
        $pad = str_repeat('&nbsp;', $depth * 3);
        $cat_name = apply_filters('list_cats', $category->name, $category);
 
        if( !isset($args['value']) ){
            $args['value'] = ( $category->taxonomy != 'category' ? 'slug' : 'id' );
        }
 
        $value = ($args['value']=='slug' ? $category->slug : $category->term_id );
 
        $output .= "\t<option class=\"level-$depth\" value=\"".$value."\"";
        if ( $value === (string) $args['selected'] ){ 
            $output .= ' selected="selected"';
        }
        $output .= '>';
        $output .= $pad.$cat_name;
        if ( $args['show_count'] )
            $output .= '&nbsp;&nbsp;('. $category->count .')';
 
        $output .= "</option>\n";
        }
 
	}

	add_action( 'restrict_manage_posts', 'filtro_categorias_empresa' );
 
	function filtro_categorias_empresa() {
	  // Acesso às variáveis do tipo de post atual e o array do pedido
	  global $typenow, $wp_query;
	 
	  // Se o tipo de post for diferente de % não faz nada
	  if ( $typenow != 'sobre-a-empresa' )
	   return false;
	 
	  // Imprime uma select box com todos os termos da taxonomia %
	  wp_dropdown_categories(array(
	  	'walker'=> new SH_Walker_TaxonomyDropdown(),
            'value'=>'slug',
	   'show_option_all' =>  'Mostrar tudo',
	   'taxonomy'        =>  'a-empresa',
	   'name'            =>  'a-empresa',
	   'orderby'         =>  'name',
	   'id'              =>  $wp_query->query['term'],
	   'selected'        =>  $wp_query->query['name'],
	   'hierarchical'    =>  true,
	   'depth'           =>  5,
	   'show_count'      =>  true,
	   'hide_empty'      =>  true,
	   )
	  );
	}

	
	
	function the_thumb($idpost)
	{
	  global $wpdb, $post;

	  $thumb = $wpdb->get_row("SELECT ID, post_title FROM {$wpdb->posts} WHERE post_parent = {$idpost} AND post_mime_type LIKE 'image%' ORDER BY menu_order");

	  if(!empty($thumb))
	  {
	    $image = image_downsize($thumb->ID, 'thumb');

	    echo $image[0];

	  }
	}

	function the_medium($idpost)
	{
	  global $wpdb, $post;

	  $thumb = $wpdb->get_row("SELECT ID, post_title FROM {$wpdb->posts} WHERE post_parent = {$idpost} AND post_mime_type LIKE 'image%' ORDER BY menu_order");

	  if(!empty($thumb))
	  {
	    $image = image_downsize($thumb->ID, 'medium');

	    echo $image[0];

	  }
	}


	function the_large($idpost)
	{
	  global $wpdb, $post;

	  $thumb = $wpdb->get_row("SELECT ID, post_title FROM {$wpdb->posts} WHERE post_parent = {$idpost} AND post_mime_type LIKE 'image%' ORDER BY menu_order");

	  if(!empty($thumb))
	  {
	    $image = image_downsize($thumb->ID, 'large');

	    echo $image[0];

	  }
	}



	if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Base10 Sidebar',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
}



//add_action('widgets_init', create_function('', 'return register_widget("BarraLateralSeLiga");'));

// Verificar widgets nas áreas de widgets
function is_sidebar_active($index) {
    global $wp_registered_sidebars;

    $widgetcolums = wp_get_sidebars_widgets();

    if ($widgetcolums[$index])
        return true;

    return false;
}


// end is_sidebar_active
// Widget Sidebar

function sidebarwidget_config() {

    // Criarmos a array vazia
    $options = array();

    // Se o formulário for submetido, salvamos as infomações
    if ($_POST['salvar']):
        $opcoes['descricao'] = $_POST['descricao'];


        update_option('sidebarwidget_config', $opcoes);
    endif;

    // Lemos as informações armazenadas
    $opcoes = get_option('sidebarwidget_config');

    // Mostramos o formulário de configuração <input type="text" name="descricao"  value="'.$opcoes['descricao'].'" class="widefat" />
    echo 'Sidebar da pagina empresa';
}

function sidebarwidget($args) {

    // Lemos as informações armazenadas
    $opcoes = get_option('sidebarwidget_config');

    // Mostramos o nosso widget widget
    //echo '<div class="widget">';

	         
	        switch (PAGIN) {
	            case 'Empresa': $tap="atual"; break;
	            case 'Segurança': $seguranca="atual"; break;
	            case 'Imprensa': $imprensa="atual"; break;
	            case 'Trabalhe Conosco': $trabalhe="atual"; break;
	            case 'Ações Sociais': $sociais="atual"; break;
	            case 'Filiais': $filiais="atual"; break;
	            case 'Frotas': $frotas="atual"; break;
	            case 'Missão, Visão e Valores': $missao="atual"; break;
	            case 'A Tap Transportes': $tap="atual"; break;	
	            case 'Cidades Atendidas': $cidades="atual"; break;           
	    	}

	    	$idPost = get_the_ID();
		    $catsy = get_the_category();
		    $myCat = $catsy[0]->cat_ID;    

		    $term_list = wp_get_post_terms($idPost, 'a-empresa', array("fields" => "names"));
		    foreach ($term_list as $termo) {
		        # code...
		        $categoria = $termo;
		    }

		    switch ($categoria) {
		        case 'A Tap Transportes': $tap="atual"; break;
		        case 'Ações Sociais': $sociais="atual";  break;
		        case 'Filais': $filiais="atual"; break;
		        case 'Frota': $frotas="atual"; break;
		        case 'Imprensa': $imprensa="atual"; break;
		        case 'Missão, Visão e Valores': $missao="atual"; break;
		        case 'Segurança': $seguranca="atual"; break;
		        case 'Cidades Atendidas': $cidades="atual"; break;

		    }

    	
    ?>
    <ul id="menu-sidebar">
        <li><a href="<?php echo home_url('empresa')?>" class="<?php echo $tap; ?>">A Tap Transportes</a></li>
        <li><a href="<?php echo home_url('empresa/missao-visao-e-valores')?>" class="<?php echo $missao; ?>">Missão, visão e valores</a></li>
        <li><a href="<?php echo home_url('empresa/frotas')?>" class="<?php echo $frotas; ?>">Frota</a></li>
        <li><a href="<?php echo home_url('empresa/filiais')?>" class="<?php echo $filiais; ?>">Filiais</a></li>
        <li><a href="<?php echo home_url('empresa/cidades-atendidas')?>" class="<?php echo $cidades; ?>">Cidades Atendidas</a></li-->
        <!--li><a href="<?php echo home_url('empresa/acoes-sociais')?>" class="<?php echo $sociais; ?>">Ações sociais</a></li-->
        <li><a href="<?php echo home_url('empresa/trabalhe-conosco')?>" class="<?php echo $trabalhe; ?>">Trabalhe conosco</a></li>
        <!--li><a href="<?php echo home_url('empresa/imprensa')?>" class="<?php echo $imprensa; ?>">Imprensa</a></li-->
        <li><a href="<?php echo home_url('empresa/seguranca')?>" class="<?php echo $seguranca; ?>">Segurança</a></li>
    </ul>

    <?php
    //echo '</div>';
}

function sidebarwidget_register() {

    register_sidebar_widget('Base10 Sidebar', 'sidebarwidget');
    register_widget_control('Base10 Sidebar', 'sidebarwidget_config');
}

add_action('widgets_init', 'sidebarwidget_register');
?>