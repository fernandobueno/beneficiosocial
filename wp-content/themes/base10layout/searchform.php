<?php

/**
Template Name: Search Form
**/
?>


<div id="pesquisa">
    <form method="get" accept-charset="utf-8" id="searchform" role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    	<label>
        	<input type="text" name="s" id="s" class="search-query" value="<?php the_search_query(); ?>" />
        </label>
        <div class="btn">
        	<button type="submit" id="searchsubmit" class="img-button">
            	<img src="<?php bloginfo('template_directory')?>/imgs/btn-pesquisar.jpg" alt="Pesquisar" />
            </button>
        </div>
    </form>
</div> 
