<?php
    get_header(); 

    $ambiente = "Prod";
    $urlBsf = "https://api.bsfonline.com.br";
    if($ambiente == "teste"){
      $urlBsf = "https://api.preprod.bsfonline.com.br";
    }


    if(isset($_REQUEST['doc']) && !isset($_REQUEST['associar']))
    {
      $doc = $_REQUEST['doc'];
      $birthDate = $_REQUEST['birthDate'];
      $cnpjEntidade = $_REQUEST['cnpjEntidade'];
      $idTrabalhador = $_REQUEST['idTrabalhador'];

      $postData = array(
        "doc"=>$doc,
        "birthDate"=>$birthDate,
        "cnpjEntidade"=>$cnpjEntidade,
        "idTrabalhador"->$idTrabalhador
      );

      // Setup cURL
      $ch = curl_init($urlBsf.'/trabalhadores/auth');
      curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
      ));

      // Send the request
      $response = curl_exec($ch);

      // Check for errors
      if($response === FALSE){
        die(curl_error($ch));
      }

      $responseData = json_decode($response, TRUE);

      $contador = 0;
      
      $employee = array();    
      $employer = array();
      $syndicate = array();

      //foreach ($responseData as $_responseData) { 
        $employee = $responseData['employee'];
        $employer = $responseData['employer'];
        $syndicate = $responseData['syndicate'];
      //} 
      
    }



    

  if(AMBIENTE == "sandbox") {
    $plan = "2454FC8A646461D884CEDFA0E9E4F144";
    $email = "c13130046833224601086@sandbox.pagseguro.com.br";
  }

 // $numCelular = str_replace("-", "", $numCelular);



//   // $dadosPayload = json_encode($dadosPayload);

//   $curl = curl_init();
//     curl_setopt_array($curl, array(
//     CURLOPT_URL => URL_WS."/pre-approvals?email=".EMAIL_PGS."&token=".TOKEN_PGS,
//     CURLOPT_RETURNTRANSFER => true,
//     CURLOPT_ENCODING => "",
//     CURLOPT_MAXREDIRS => 10,
//     CURLOPT_TIMEOUT => 30,
//     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//     CURLOPT_CUSTOMREQUEST => "POST",
//     CURLOPT_POSTFIELDS => $dadosPayload,
//         CURLOPT_HTTPHEADER => array(
//       'Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1',
//               'Content-Type: application/json; charset=UTF-8'  
//     ),

//   ));

//   $response = curl_exec($curl);
//   $err = curl_error($curl);

//   curl_close($curl);

//   if ($err) {
//     echo "cURL Error #:" . $err;
//   } else {
//     $sessionJson = json_decode($response);
//     var_dump($sessionJson);
//     if($sessionJson->code){
//       $sessionID = (string)$sessionJson->code;
//       $sessionID = "sucesso_".$sessionID;

//     }
//   }


?>

<!--******************** INICIO CONTEUDO ********************-->

<div class="container">
    <div class="row">
      <div class="buscaform">
        <div class="col-xs-12 col-md-12">

            <form class="jotform-form" style="margin-top: 20px;" action="<?php echo home_url('dependentes'); ?>" method="post" name="form_73455838382669" id="73455838382669" accept-charset="utf-8">
              <?php 
                if($syndicate['atvo_filiacao'] == false) {
                  echo "<h2>Procure seu sindicato para mais informações.</h2>";
                } else {
              ?>

              <input type="hidden" name="formID" value="73455838382669" />

              <input type="hidden" name="employee_id" value="<?php if (!is_null($employee['id'])) { echo $employee['id']; } ?>" />
              <input type="hidden" name="employee_name" value="<?php if (!is_null($employee['name'])) { echo $employee['name']; } ?>" />
              <input type="hidden" name="employee_doc" value="<?php if (!is_null($employee['doc'])) { echo $employee['doc']; } ?>" />
              <input type="hidden" name="employee_birthDate" value="<?php if (!is_null($employee['birthDate'])) { echo $employee['birthDate']; } ?>" />
              <input type="hidden" name="employer_id" value="<?php if (!is_null($employer['id'])) { echo $employer['id']; } ?>" />
              <input type="hidden" name="employer_name" value="<?php if (!is_null($employer['name'])) { echo $employer['name']; } ?>" />
              <input type="hidden" name="employer_doc" value="<?php if (!is_null($employer['doc'])) { echo $employer['doc']; } ?>" />

              <input type="hidden" name="employer_cep" value="<?php if (!is_null($employer['cep'])) { echo $employer['cep']; } ?>" />
              <input type="hidden" name="employer_street" value="<?php if (!is_null($employer['street'])) { echo $employer['street']; } ?>" />
              <input type="hidden" name="employer_number" value="<?php if (!is_null($employer['number'])) { echo $employer['number']; } ?>" />
              <input type="hidden" name="employer_complement" value="<?php if (!is_null($employer['complement'])) { echo $employer['complement']; } ?>" />
              <input type="hidden" name="employer_neighborhood" value="<?php if (!is_null($employer['neighborhood'])) { echo $employer['neighborhood']; } ?>" />
              <input type="hidden" name="employer_city" value="<?php if (!is_null($employer['city'])) { echo $employer['city']; } ?>" />
              <input type="hidden" name="employer_uf" value="<?php if (!is_null($employer['uf'])) { echo $employer['uf']; } ?>" />

              <input type="hidden" name="syndicate_id" value="<?php if (!is_null($syndicate['id'])) { echo $syndicate['id']; } ?>" />
              <input type="hidden" name="syndicate_company" value="<?php if (!is_null($syndicate['company'])) { echo $syndicate['company']; } ?>" />
              <input type="hidden" name="syndicate_name" value="<?php if (!is_null($syndicate['name'])) { echo $syndicate['name']; } ?>" />
              <input type="hidden" name="syndicate_doc" value="<?php if (!is_null($syndicate['doc'])) { echo $syndicate['doc']; } ?>" />
              <input type="hidden" name="syndicate_email" value="<?php if (!is_null($syndicate['email'])) { echo $syndicate['email']; } ?>" />

               <input type="hidden" name="syndicate_plano" value="<?php if (!is_null($syndicate['plano'])) { echo $syndicate['plano']; } ?>" />
               <input type="hidden" name="syndicate_assinatura" value="<?php if (!is_null($syndicate['assinatura'])) { echo $syndicate['assinatura']; } ?>" />

              <input type="hidden" name="syndicate_plano_valor" value="<?php if (!is_null($syndicate['valor'])) { echo number_format((float)$syndicate['valor'], 2, '.', ''); } ?>" />

              <input type="hidden" name="entidade" value="siemaco" />
    
              <div class="form-all">
              
                <ul class="form-section page-section">
                  <li id="cid_87" class="form-input-wide" data-type="control_head">
                    <div class="form-header-group ">
                      <div class="header-text httac htvam">
                        <h3 id="header_87" class="form-header" data-component="header">
                          FILIAÇÃO
                        </h3>
                        <p><?php echo $mensagem; ?></p>
                      </div>
                    </div>
                  </li>                  
                  
                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Nome
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_39" name="nome" data-type="input-textbox" class="form-textbox validate[required]" size="20" data-component="textbox" required="" value="<?php if (!is_null($employee['name'])) { echo $employee['name']; } ?>" readonly />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Empresa
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_39" name="nome_empresa" data-type="input-textbox" class="form-textbox validate[required]" size="20" data-component="textbox" required="" value="<?php if (!is_null($employer['name'])) { echo $employer['name']; } ?>" readonly />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      CNPJ
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_cnpj" name="cnpj" data-type="input-textbox" class="form-textbox validate[required]" size="20" ata-component="textbox" value="<?php if (!is_null($employer['doc'])) { echo $employer['doc']; } ?>" readonly required="" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Sindicato
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_39" name="nome_sindicato" data-type="input-textbox" class="form-textbox validate[required]" size="20" data-component="textbox" required="" value="<?php if (!is_null($syndicate['company'])) { echo $syndicate['company']; } ?>" readonly />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>


                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      CPF
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_cpf" name="cpf" data-type="input-textbox" class="form-textbox validate[required]" size="20" data-component="textbox" required="" value="<?php if (!is_null($employee['doc'])) { echo $employee['doc']; } ?>" readonly />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      RG
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_rg" name="rg" data-type="input-textbox" class="form-textbox validate[required]" size="20" data-component="textbox" value="" />                        
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      CNH
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_cnh" name="cnh" data-type="input-textbox" class="form-textbox validate[required]" size="20" data-component="textbox" value="" />                        
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Categoria da CNH
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_cnh_cat" name="cnh_cat" data-type="input-textbox" class="form-textbox validate[required]" size="20" data-component="textbox" value="" />                        
                      </span>
                    </div>
                  </li>
                  <?php /*
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      RG
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_rg" name="rg" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" required="" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>
                   */ ?>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Sexo
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <select id="input_sexo" name="sexo" class="form-textbox validate[required]" size="20" value="" data-component="" required="">
                          <option value="Masculino">Masculino</option>
                          <option value="Feminino">Feminino</option>
                        </select>
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      CEP
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_cep" name="cep" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" required="" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>

                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Logradouro
                      <span class="form-required">
                        
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_logradouro" name="logradouro" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"></label>
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Numero
                      <span class="form-required">
                     
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="number" id="input_numero" name="numero" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"></label>
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Complemento
                      <span class="form-required">
                    
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_comp" name="comp" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox"/>
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"></label>
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Bairro
                      <span class="form-required">
                     
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_bairro" name="bairro" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"></label>
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Cidade
                      <span class="form-required">
                   
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_cidade" name="cidade" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> </label>
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Estado
                      <span class="form-required">
                       
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_uf" name="uf" maxlength="2" minlength="2" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> </label>
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Telefone
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="phone" id="input_telefone" name="telefone" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" required="" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Celular
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="phone" id="input_celular" name="celular" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" required="" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Email
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="email" id="input_email" name="email" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" required="" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Data Nascimento
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="date" id="input_dtnasc" name="dtnasc" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" required="" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Numero CTPS
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_ctps" name="ctps" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Série CTPS
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_ctps_serie" name="ctps_serie" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>

                   <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Data de Emissão CTPS
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="date" id="input_ctps_emissao" name="ctps_emissao" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      PIS
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_pis" name="pis" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Cargo
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_cargo" name="cargo" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Data de Admissão
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="date" id="input_dataadmissao" name="dataadmissao" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                     Cidade de Nascimento
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_cidnascimento" name="cidnascimento" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                     Nacionalidade
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_nacionalidade" name="nacionalidade" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                     Escolaridade
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_escolaridade" name="escolaridade" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>

                  <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                     Estado Civil
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_estadocivil" name="estadocivil" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>
                  <?php /*
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Data de Admissão
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="date" id="input_dtadmis" name="dtadmis" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" required="" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>
                   */ ?>
                   <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Local de Trabalho
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_endtrab" name="localtrab" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" required="" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>

                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Endereço de Trabalho
                      <span class="form-required">
                        *
                      </span>
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_endtrab" name="endtrab" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" required="" />
                        <label class="form-sub-label" for="input_39" style="min-height:13px;"> * campo obrigatório </label>
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Nome Conjuge
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_nome_conj" name="nome_conj" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Nome do Pai
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_nome_pai" name="nome_pai" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>
                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                    <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                      Nome da Mãe
                    </label>
                    <div id="cid_39" class="input-field ">
                      <span class="form-sub-label-container" style="vertical-align:top;">
                        <input type="text" id="input_nome_mae" name="nome_mae" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" />
                      </span>
                    </div>
                  </li>
                  

                  <li class="input-field col-xs-12 col-md-12 m12 control_pagebreak" data-type="control_button" id="id_85">
                    <div id="cid_85" class="form-input-wide">
                      <div style="" class="form-buttons-wrapper">
                        <button id="input_85" type="submit" class="form-submit-button btn btn-default" data-component="button" name="associar">
                          Associar
                        </button>
                      </div>
                    </div>
                  </li>  

                </ul>
              </div>
            <?php } ?>
            </form>

          
        </div>
      </div>
    </div>
</div>

<?php
    get_footer();
?>
<script src="/wp-content/themes/base10layout/js/jquery.mask.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {

    $('#input_cnpj').mask('00.000.000/0000-00', {reverse: true});
     $('#input_telefone').mask('(00) 0000-0000');

      $('#input_celular').mask('(00) 0000-00009');
      $('#input_celular').blur(function(event) {
         if($(this).val().length == 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
            $('#input_celular').mask('(00) 00000-0009');
         } else {
            $('#input_celular').mask('(00) 0000-00009');
         }
      });


    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#input_logradouro").val("");
        $("#input_bairro").val("");
        $("#input_cidade").val("");
        $("#input_uf").val("");
        

    }
    
    //Quando o campo cep perde o foco.
    $("#input_cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#input_logradouro").val("...");
                $("#input_bairro").val("...");
                $("#input_cidade").val("...");
                $("#input_uf").val("...");
                

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#input_logradouro").val(dados.logradouro);
                        $("#input_bairro").val(dados.bairro);
                        $("#input_cidade").val(dados.localidade);
                        $("#input_uf").val(dados.uf);
                        
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });
  });
</script>