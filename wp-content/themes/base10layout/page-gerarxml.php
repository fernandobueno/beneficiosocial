<?php
include_once('./wp-config.php');
include_once('./wp-load.php');
include_once('./wp-includes/wp-db.php');
global $wpdb;

if(($_REQUEST["idPost"] != ""))
{

	ini_set('memory_limit', '16M');
	set_time_limit(0);
	require('PHPExcel/Classes/PHPExcel.php');

	$idPost = $_REQUEST["idPost"];

	$query = "post_type=mapa&post_status=publish&p=".$idPost;

	$filename = '';

	query_posts($query);
    if (have_posts()):
        while (have_posts()) : the_post();
        {                                          
         
            $idPost = get_the_ID(); 
            $filename = get_post_meta($idPost, 'wpcf-mapa-xls', true);
		}
        endwhile;                      
    endif;   
    wp_reset_query();

    $filename = str_replace("http://carteirinha1.hospedagemdesites.ws/", "/home/storage/5/38/5d/carteirinha1/public_html/", $filename);

	$objReader = new PHPExcel_Reader_Excel5();
	$objReader->setReadDataOnly(true);
	$objPHPExcel = $objReader->load( $filename );

	$rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();

	$sheet = $objPHPExcel->getActiveSheet();


	//var_dump($array_data[2]);


	//Cria novo documento xml
	$doc = new DOMDocument();
	$doc->formatOutput = true;

	//Cria o n� pai
	$markers = $doc->createElement("markers");
	$doc->appendChild($markers);

	//Seta o tipo de cabe�alho para texto/XML
	header("Content-type: text/xml");

	foreach($rowIterator as $row){
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
		if(1 == $row->getRowIndex()) continue;//skip first row
		$rowIndex = $row->getRowIndex();
		$array_data[$rowIndex] = array('name'=>'', 'address'=>'','regiao'=>'','trabalhadores'=>'');

		//Cria um n� filho
		$mark = $doc->createElement("marker");
		
		//Insere o n� filho ao n� pai
		$newMark = $markers->appendChild( $mark );
		
		foreach ($cellIterator as $cell) {
			if('A' == $cell->getColumn()){
				$newMark->setAttribute('name', $cell->getCalculatedValue());
			} else if('B' == $cell->getColumn()){			
				$newMark->setAttribute('address', $cell->getCalculatedValue());
			} else if('C' == $cell->getColumn()){
				$newMark->setAttribute('regiao', $cell->getCalculatedValue());			
			} else if('D' == $cell->getColumn()){
				$newMark->setAttribute('trabalhadores', $cell->getCalculatedValue());			
			}
		}
	}


	//Imprime o documento XML
	 echo $doc->saveXML();
}