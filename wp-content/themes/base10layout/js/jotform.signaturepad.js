jQuery(document).ready(function(){

    jQuery(".pad").each(function(idx,el){
        var pad = jQuery(el);
        var pwidth = pad.data("width");
        var pheight = pad.data("height");
        pad.width(pwidth).height(pheight);
        pad.jSignature({width: pwidth, height: pheight});

        // bind changes - emits 'change' event immediately after a stroke
        pad.bind('change', function(){
            var thispad = jQuery(this);
            var qid = thispad.attr('data-id');
            if ( thispad.jSignature('getData','base30')[1].length > 0 ) {
                var sigdata = thispad.jSignature('getData');
                jQuery("#input_" + qid).val(sigdata);
                //JotForm.triggerWidgetCondition(qid);
            }
        });

        // a prototype event that will resize canvas onresize
        el.observe('on:sigresize', function(event){
            var qid = el.readAttribute('data-id');
            var newwidth = $('signature_pad_' + qid).getWidth() + 'px';
            el.setStyle({'width': newwidth})
              .select('canvas.jSignature').first()
              .setStyle({'width': newwidth})
              .writeAttribute('width', newwidth);

            // use jquery to reset jSignature
            pad.jSignature("reset");
        });
    });
    // if (typeof FlashCanvas != "undefined") {

    jQuery(".clear-pad").click(function(e){
        var pad = jQuery(this).prev().find(".pad");
        pad.jSignature("reset");

        // clear input field as well
        var qid = pad.attr('data-id');
        jQuery("#input_" + qid).val('');
        //JotForm.triggerWidgetCondition(qid);
    });

    jQuery(".jotform-form").submit(function(e){

        jQuery(".pad").each(function(idx, el){
            var pad = jQuery(el);
            if ( !pad.hasClass('edit-signature') && pad.jSignature('getData','base30')[1].length > 0 ) {
                var id = pad.attr("data-id");
                jQuery("#input_" + id).val(pad.jSignature('getData'));
            }
        });
    });

    //@diki
    //edit mode
    
});
