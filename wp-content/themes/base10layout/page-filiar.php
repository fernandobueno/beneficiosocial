<?php 
header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");
header("access-control-allow-origin: http://sandbox.pagseguro.uol.com.br");  

$autorizaPagamento = $_REQUEST['autorizaPagamento'];
$autorizaPagamento = $autorizaPagamento[0];

require_once 'pagseguro/config.php';

// $ambiente = "teste";
// $urlBsf = "https://cobranca.bsfonline.com.br";
// if($ambiente == "teste"){
//   $urlBsf = "https://cobranca.preprod.bsfonline.com.br";
// }


  if(isset($_REQUEST['gerarFicha']) || ($autorizaPagamento == "Não")){
    get_header();

    require_once('jSignature_Tools_Base30.php');
    function base30_to_jpeg($base30_string, $output_file) {

        $data = str_replace('image/jsignature;base30,', '', $base30_string);
        $converter = new jSignature_Tools_Base30();
        $raw = $converter->Base64ToNative($data);
        
        //Calculate dimensions
        //var_dump($base30_string);
        $width = 0;
        $height = 0;
        foreach($raw as $line)
        {
            if (max($line['x'])>$width)$width=max($line['x']);
            if (max($line['y'])>$height)$height=max($line['y']);
        }

        // Create an image
        $im = imagecreatetruecolor($width+20,$height+20);

        // Save transparency for PNG
        imagesavealpha($im, true);
        // Fill background with transparency
        $trans_colour = imagecolorallocatealpha($im, 255, 255, 255, 127);
        imagefill($im, 0, 0, $trans_colour);
        // Set pen thickness
        imagesetthickness($im, 2);
        // Set pen color to black
        $black = imagecolorallocate($im, 0, 0, 0);

    //Create Image
        $ifp = fopen($output_file, "wb"); 
        imagepng($im, $output_file);
        fclose($ifp);  
        imagedestroy($im);

        return $output_file; 
    }

    global $wpdb;

    if(isset($_REQUEST['entidade']))    
      $entidade = $_REQUEST['entidade'];
    else
      $entidade = $_REQUEST['syndicate_doc'];

    if($entidade == "")
      $entidade = $_REQUEST['syndicate_doc'];

    $idAssinatura = $_REQUEST['idAssinatura'];

    $cnpj = $_REQUEST['cnpj'];
    $cpf = $_REQUEST['cpf'];    
    $sexo = $_REQUEST['sexo'];    
    $nome = $_REQUEST['nome'];
    $logradouro = $_REQUEST['logradouro'];
    $numero = $_REQUEST['numero'];
    $comp = $_REQUEST['comp'];
    $bairro = $_REQUEST['bairro'];
    $cidade = $_REQUEST['cidade'];
    $uf = $_REQUEST['uf'];
    $telefone = $_REQUEST['telefone'];
    $celular = $_REQUEST['celular'];
    $email = $_REQUEST['email'];
    $dtnasc = $_REQUEST['dtnasc'];    
    $endtrab = $_REQUEST['endtrab'];
    $nome_conj = $_REQUEST['nome_conj'];
    $nome_pai = $_REQUEST['nome_pai'];
    $nome_mae = $_REQUEST['nome_mae'];
    $foto = $_REQUEST['foto'];

    $ctps = $_REQUEST['ctps'];
    $cargo = $_REQUEST['cargo'];
    $dataadmissao = $_REQUEST['dataadmissao'];
    $cidnascimento = $_REQUEST['cidnascimento'];
    $estadocivil = $_REQUEST['estadocivil'];

    $cnh = $_REQUEST['cnh'];
    $cnh_cat = $_REQUEST['cnh_cat'];
    $ctps_serie = $_REQUEST['ctps_serie'];
    $ctps_emissao = $_REQUEST['ctps_emissao'];
    $pis = $_REQUEST['pis'];
    $nacionalidade = $_REQUEST['nacionalidade'];
    $escolaridade = $_REQUEST['escolaridade'];
    $localtrab = $_REQUEST['localtrab'];

    $dtnasc  = date("d/m/Y",strtotime($dtnasc));
    $dtadmis = date("d/m/Y",strtotime($dtadmis));

    $rg = $_REQUEST['rg'];
    $cep = $_REQUEST['cep'];
    
    $funcao = "" ;
    $escolaridade = "" ;
    $cbo = "" ;
    //$dtadmis = "" ;

    $employee_id = $_REQUEST['employee_id'];
    $employee_name = $_REQUEST['employee_name'];
    $employee_doc = $_REQUEST['employee_doc'];
    $employee_birthDate = $_REQUEST['employee_birthDate'];

    $employer_id = $_REQUEST['employer_id'];
    $employer_name = $_REQUEST['employer_name'];
    $employer_doc = $_REQUEST['employer_doc'];
    $syndicate_id = $_REQUEST['syndicate_id'];
    $syndicate_company = $_REQUEST['syndicate_company'];
    $syndicate_name = $_REQUEST['syndicate_name'];
    $syndicate_doc = $_REQUEST['syndicate_doc']; 
    $syndicate_email = $_REQUEST['syndicate_email']; 

    $syndicate_plano = $_REQUEST['syndicate_plano'];
    $syndicate_plano_valor = $_REQUEST['syndicate_plano_valor'];
    $syndicate_assinatura = $_REQUEST['syndicate_assinatura'];

    $employer_cep = $_REQUEST['employer_cep'];
    $employer_street = $_REQUEST['employer_street'];
    $employer_number = $_REQUEST['employer_number'];
    $employer_complement = $_REQUEST['employer_complement'];
    $employer_neighborhood = $_REQUEST['employer_neighborhood'];
    $employer_city = $_REQUEST['employer_city'];
    $employer_uf = $_REQUEST['employer_uf']; 


    if(isset($_REQUEST['dependenteNome']))    
      $dependenteNome = $_REQUEST['dependenteNome'];
    else
      $dependenteNome = "";

    // var_dump($dependenteNome);

    if(isset($_REQUEST['dependenteDataNascimento']))    
      $dependenteDataNascimento = $_REQUEST['dependenteDataNascimento'];
    else
      $dependenteDataNascimento = "";

    if(isset($_REQUEST['dependenteParentesco']))    
      $dependenteParentesco = $_REQUEST['dependenteParentesco'];
    else
      $dependenteParentesco = ""; 

    if(isset($_REQUEST['dependenteSexo']))    
      $dependenteSexo = $_REQUEST['dependenteSexo'];
    else
      $dependenteSexo = ""; 

    if($dependenteNome[0] == ""){
      $dependenteNome = [];
    }

    $autorizaPagamento = $_REQUEST['autorizaPagamento'];
    $q83_assinatura = $_REQUEST['q83_assinatura'];

    $arquivoAssinatura = "assinatura30666153000113.jpg";     
    //var_dump($arquivoAssinatura);

    // open the output file for writing
    $ifp = fopen($_SERVER['DOCUMENT_ROOT']."/wp-content/themes/base10layout/templates/imagens/".$arquivoAssinatura, 'wb' ); 

    // split the string on commas
    // $data[ 0 ] == "data:image/png;base64"
    // $data[ 1 ] == <actual base64 string>
    $data = explode(',',$q83_assinatura);

    // we could add validation here with ensuring count( $data ) > 1
    fwrite($ifp,base64_decode($data[1]));

    // clean up the file resource
    fclose($ifp); 
    
    define('UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/base10layout/templates/imagens/');
    $img = $q83_assinatura;
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $imgName = uniqid() . '.png';
    $file = UPLOAD_DIR .$imgName;
    $success = file_put_contents($file, $data);    
    
    $emailenvio = "fernando.compuway@gmail.com";
               
    $ip = $_SERVER["REMOTE_ADDR"];        
    $data = date("d/m/Y - H:i:s", time()); 

    $nomeAnexo = "";

    $_templateSindicato = "";

    $now = DateTime::createFromFormat('U.u', microtime(true));
    $_protocolo = $now->format("mdYHisu");
     //$hash = hash_hmac('sha256', $time, "1");

    include("mpdf61/mpdf.php"); 


    function render_email_anexo($employee_id,$employer_id,$employer_name,$employer_doc,$employer_cep,$employer_street,$employer_number,$employer_complement,$employer_neighborhood,$employer_city,$employer_uf,$syndicate_id,$syndicate_name,$syndicate_doc,$cnpj,$cpf,$rg,$sexo,$funcao,$escolaridade,$cbo,$nome,$logradouro,$numero,$comp,$bairro,$cidade,$uf,$telefone,$celular,$email,$dtnasc,$dtadmis,$endtrab,$nome_conj,$nome_pai,$nome_mae,$dependenteNome,$dependenteDataNascimento,$dependenteParentesco,$imgName,$foto,$ctps,$cargo,$dataadmissao,$cidnascimento,$estadocivil,$dependenteSexo,$cep,$cnh,$cnh_cat,$ctps_serie,$ctps_emissao,$pis,$nacionalidade,$localtrab,$syndicate_company,$entidade,$_protocolo,$syndicate_plano_valor,$autorizaPagamento) {
        ob_start();

        $_templateSindicato = "templates/email-template-".$entidade.".phtml";

        include ''.$_templateSindicato.'';

        $fichaanexo = ob_get_contents(); 

        echo $fichaanexo;

          // exit();

        ob_end_clean();
        ob_start();
        
              
        $mpdf=new mPDF(['debug' => true,
                        'format' => 'letter-L',
                        'margin_left' => 13,
                        'margin_right' => 13,
                        'margin_top' => 13,
                        'margin_bottom' => 13]); 
        $mpdf->SetDisplayMode('fullpage');

        $_footer = "<table width=\"100%\"><tr><td style='font-size: 12px; padding-bottom: 20px;' align=\"right\">Página {PAGENO} de {nb}</td></tr></table>";
        $mpdf->SetHTMLFooter($_footer);

        $mpdf->WriteHTML($fichaanexo);
        ob_clean();
        $time = date("dmYHis", time());

        $mpdf->showImageErrors = true;
        $nomeAnexo = "Ficha-".uniqid().".pdf";
        $mpdf->Output($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/base10layout/templates/fichas/'.$nomeAnexo, 'F');

        //$ficha_anexo = ob_get_contents(); 
        ob_end_clean();
        return $nomeAnexo;        
    }

    function render_email($employee_id,$employer_id,$employer_name,$employer_doc,$employer_cep,$employer_street,$employer_number,$employer_complement,$employer_neighborhood,$employer_city,$employer_uf,$syndicate_id,$syndicate_name,$syndicate_doc,$cnpj,$cpf,$rg,$sexo,$funcao,$escolaridade,$cbo,$nome,$logradouro,$numero,$comp,$bairro,$cidade,$uf,$telefone,$celular,$email,$dtnasc,$dtadmis,$endtrab,$nome_conj,$nome_pai,$nome_mae,$dependenteNome,$dependenteDataNascimento,$dependenteParentesco,$q83_assinatura,$ctps,$cargo,$dataadmissao,$cidnascimento,$estadocivil,$dependenteSexo,$cep,$cnh,$cnh_cat,$ctps_serie,$ctps_emissao,$pis,$nacionalidade,$localtrab,$syndicate_company,$_protocolo,$syndicate_plano_valor,$autorizaPagamento) {
        ob_start();
        
        include "templates/email-template-padrao.phtml";
        $emailPadrao = ob_get_contents();

        ob_end_clean();

        return $emailPadrao;
    }


    $mens = render_email($employee_id,$employer_id,$employer_name,$employer_doc,$employer_cep,$employer_street,$employer_number,$employer_complement,$employer_neighborhood,$employer_city,$employer_uf,$syndicate_id,$syndicate_name,$syndicate_doc,$cnpj,$cpf,$rg,$sexo,$funcao,$escolaridade,$cbo,$nome,$logradouro,$numero,$comp,$bairro,$cidade,$uf,$telefone,$celular,$email,$dtnasc,$dtadmis,$endtrab,$nome_conj,$nome_pai,$nome_mae,$dependenteNome,$dependenteDataNascimento,$dependenteParentesco,$q83_assinatura,$ctps,$cargo,$dataadmissao,$cidnascimento,$estadocivil,$dependenteSexo,$cep,$cnh,$cnh_cat,$ctps_serie,$ctps_emissao,$pis,$nacionalidade,$localtrab,$syndicate_company,$_protocolo,$syndicate_plano_valor,$autorizaPagamento);

    require("phpmailer/PHPMailerAutoload.php");        
    try {
          if($syndicate_email != "")
              $emailContato = $syndicate_email;
          else
            $emailContato = "itdev@beneficiosocial.com.br";



          $mail = new PHPMailer(true);    
          // Tell PHPMailer to use SMTP
          $mail->isSMTP();          
          //$mail->Username = "AKIAIQXP4STRWT6I4NUA";
          //$mail->Password = "ArqyCGexrIu2LeCM4G7YRI40DF6K3WzOccCIirk2cCh9";
          $mail->Username = "AKIAR7PRGLXMPQGFTA2E";
          $mail->Password = "BML8IMJMIbRzFpMaGzTFirYVbt3+cO5bEGuOkpT03E8l";              
          //$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
          $mail->Debugoutput = "html";
          $mail->Host = "email-smtp.us-east-1.amazonaws.com";          
          $mail->SMTPAuth = true;
          $mail->SMTPSecure = "tls";
          //$mail->SMTPDebug = 3;
          $mail->Port = 587;
          $mail->AltBody = "FILIAÇÃO BSFOnLine";

          $mail->isHTML(true);

          $mail->From = "filiacao@bsfonline.com.br";
          $mail->FromName = "BSFOnLine";
          $mail->CharSet = "UTF-8"; // Charset da mensagem (opcional) 
          $mail->AddAddress($emailContato);
          $mail->AddBCC("itdev@beneficiosocial.com.br");
          $mail->AddBCC("fernando.compuway@gmail.com");
          $mail->AddBCC("aplicativosindicato@gmail.com");
          $mail->Subject = "FILIAÇÃO BSFOnLine";    

          $templateSindicato = "templates/email-template-".$entidade.".phtml";   


        if(file_exists($_SERVER['DOCUMENT_ROOT']."/wp-content/themes/base10layout/".$templateSindicato)){
    
          $_nomeAnexo = render_email_anexo($employee_id,$employer_id,$employer_name,$employer_doc,$employer_cep,$employer_street,$employer_number,$employer_complement,$employer_neighborhood,$employer_city,$employer_uf,$syndicate_id,$syndicate_name,$syndicate_doc,$cnpj,$cpf,$rg,$sexo,$funcao,$escolaridade,$cbo,$nome,$logradouro,$numero,$comp,$bairro,$cidade,$uf,$telefone,$celular,$email,$dtnasc,$dtadmis,$endtrab,$nome_conj,$nome_pai,$nome_mae,$dependenteNome,$dependenteDataNascimento,$dependenteParentesco,$imgName,$foto,$ctps,$cargo,$dataadmissao,$cidnascimento,$estadocivil,$dependenteSexo,$cep,$cnh,$cnh_cat,$ctps_serie,$ctps_emissao,$pis,$nacionalidade,$localtrab,$syndicate_company,$entidade,$_protocolo,$syndicate_plano_valor,$autorizaPagamento);

          $file_to_attach = $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/base10layout/templates/fichas/'.$_nomeAnexo;

          $mail->AddAttachment($file_to_attach,"Ficha_de_Filiação.pdf");

        }        

          //$mail->addStringEmbeddedImage(base64_decode($q83_assinatura), "assinatura.png", "assinatura.png","base64","image/png");           
          //$mail->Body = $mens;

          $mail->MsgHTML($mens);
          
          if(!$mail->send()) {

               $mensagem = "<p >Desculpe o transtorno, ocorreu um problema ao enviar sua filiação, tente novamente mais tarde. Obrigado.</p>";
               $mensagem .= $mail->ErrorInfo;
          } else {
              $mensagem = "<p><h4>Filiação enviada com sucesso</h4><br/>Agradecemos o contato. <br/>Em breve entraremos em contato. <br/></p>"; 
              //echo "Email sent!" , PHP_EOL;
          }

      }
      catch (phpmailerException $e) {
          $mensagem = "<p >Desculpe o transtorno, ocorreu um problema ao enviar sua filiação, tente novamente mais tarde. Obrigado.</p>";
          $mensagem .= $e->errorMessage();
      }
      catch (Exception $e) {
          $mensagem = "<p >Desculpe o transtorno, ocorreu um problema ao enviar sua filiação, tente novamente mais tarde. Obrigado.</p>";
          $mensagem .= "<p >".$e->getMessage()."</p>";
      }

      $_contDependente = 0;
      $relacaodependentes = array();

      foreach ($dependenteNome as $_dependenteNome) {
          $_dependenteDataNascimento = date("d/m/Y",strtotime($dependenteDataNascimento[$_contDependente]));
          $relacaodependentes[$_contDependente]["Nome"] = $_dependenteNome;
          $relacaodependentes[$_contDependente]["DT_Nascimento"] = $_dependenteDataNascimento;
          $relacaodependentes[$_contDependente]["Parentesco"] = $dependenteParentesco[$cont];
          $_contDependente++;
      }


      $_celular = explode(")", $celular);
      $ddd = str_replace("(", "", $_celular[0]);
      $numCelular = str_replace(" ", "", $_celular[1]);

      $_telefone = explode(")", $telefone);
      $dddTelefone = str_replace("(", "", $_telefone[0]);
      $numTelefone = str_replace(" ", "", $_telefone[1]);
   
      $postData = array(
        "employee" => array ("employee_id" => $employee_id,
                             "employee_name" => $employee_name,
                             "employee_doc" => $employee_doc
                            ), 
        "pessoa"=>"2",
        "cnpj" => $cnpj,
        "razao"=>$syndicate_company,
        "email_diretoria"=>$syndicate_email, 
        "email_cobranca" => $syndicate_email, 
        "cep"=>$cep,
        "endereco"=>$logradouro,
        "numero"=>$numero, 
        "complemento"=>$comp,
        "bairro"=>$bairro,
        "cidade"=>$cidade,
        "uf"=>$uf,
        "ddd1"=>$dddTelefone,
        "telefone1"=>$numTelefone,
        "tipo1"=>"1",
        "ddd2"=>$ddd,
        "telefone2"=>$numCelular,
        "tipo2"=>"2",
        "syndicate"=> array ( 
          "id" => $syndicate_id,
          "company" => $syndicate_company,
          "name" => $syndicate_name,
          "doc" => $syndicate_doc,
          "email" => $syndicate_email,
          "plano"=>$syndicate_plano,
          "valormensal"=>$syndicate_plano_valor
          ),
        "employer_cep" => $employer_cep,
        "employer_street" => $employer_street,
        "employer_number" => $employer_number,
        "employer_complement" => $employer_complement,
        "employer_neighborhood" => $employer_neighborhood,
        "employer_city" => $employer_city,
        "employer_uf" => $employer_uf,
        "relacaodependentes" => $relacaodependentes,  
        "cpf" => $cpf,
        "rg" => $rg,
        "sexo" => $sexo,
        "funcao" => $funcao,
        "escolaridade" => $escolaridade,
        "cbo" => $cbo,           
        "email" => $email,
        "endtrab" => $endtrab,
        "nome_conj" => $nome_conj,
        "nome_pai" => $nome_pai,
        "nome_mae" => $nome_mae,
        "dtnasc" => $dtnasc,
        "dtadmis" => $dtadmis,
        "cnh" => $cnh,
        "cnh_cat" => $cnh_cat,
        "ctps_serie" => $ctps_serie,
        "ctps_emissao" => $ctps_emissao,
        "pis" => $pis,
        "nacionalidade" => $nacionalidade,
        "localtrab" => $localtrab,
        "idAssinatura" => str_replace("sucesso_", "", $idAssinatura)
      );

      //echo json_encode($postData);

      // Setup cURL
      $ch = curl_init(urlBsf.'/api-v1/empresa/cadastro');
      curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
      ));

      // Send the request
      $response = curl_exec($ch);

      // var_dump($response);

      $now2 = DateTime::createFromFormat('U.u', microtime(true));
      $_protocolo2 = $now2->format("mdYHisu");

      if($autorizaPagamento == "Sim") {

        $ID_Planos_Adesao = "";

        $idAdesaoFormatado = str_replace("sucesso_", "", $idAssinatura);
        $idAdesaoFormatado = str_replace(" ", "", $idAdesaoFormatado);

        $sql = "SELECT * FROM Planos_Adesao where CPF = '".$cpf."' AND ID_Plano = '".$syndicate_assinatura."' AND ID_Adesao = '".$idAdesaoFormatado."'";
        $resultados = $wpdb->get_results($sql);
        foreach ($resultados as $value) {
          $ID_Planos_Adesao = $value->ID_Planos_Adesao;
        }

        // var_dump($sql);


         $postData = array(        
          "cpf" => $cpf,
          "sindicato" => $syndicate_name,
          "plano"=>$syndicate_plano,
          "vencimento" => date('m-Y'),
          "situacao" => "3",
          "pagamento" => date('Y-m-d'),
          "valorpago" => str_replace(".", "", $syndicate_plano_valor),
          "idAssinatura" => str_replace("sucesso_", "", $idAssinatura),
          "transacao" => $_protocolo2,
          "IdApp" => $ID_Planos_Adesao
        );

          // Setup cURL
          $chPagamentos = curl_init(urlBsf.'/api-v1/empresa/pagamento');
          curl_setopt_array($chPagamentos, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
          ));

          //var_dump(json_encode($postData));

          // Send the request
          $responsePagamentos = curl_exec($chPagamentos);

          curl_close($chPagamentos);

          // Check for errors
          if($responsePagamentos === FALSE){
            // die(curl_error($ch));
          }
          else{
            //$pagina = home_url('filiar')."/?retorno=".$mensagem;    
            //header('Location: '.$pagina);       
          }
        }

      // Check for errors
      if($response === FALSE){
        die(curl_error($ch));
        curl_close($ch);
      }
      else{
        curl_close($ch);
        $pagina = home_url('filiar')."/?retorno=ok";  
        //echo $mensagem;  
        header('Location: '.$pagina);       
      }
    
    //var_dump($mensagem);

     
  }
  else{

    if(isset($_REQUEST['retorno'])){
    
    get_header();
  ?>

    <div class="container" id="finalizacao" style="">
      <div class="row">
        <div class="buscaform">
          <div class="col-xs-12 col-md-12">
              <form class="jotform-form" action="https://www.beneficiosocial.com.br" method="post" name="form_73455838382669" id="73455838382669" accept-charset="utf-8">
                <input type="hidden" name="formID" value="73455838382669" />
                <div class="form-all">
                
                  <ul class="form-section page-section">                  
                    
                        
                 
                    <li id="cid_86" class="form-input-wide" data-type="control_head">
                      <div class="form-header-group ">
                        <div class="header-text httac htvam">
                          <h3 id="header_86" class="form-header" data-component="header">
                            <p><h4>Filiação enviada com sucesso</h4><br/>Agradecemos o contato. <br/>Em breve entraremos em contato. <br/></p>
                          </h3>
                        </div>
                      </div>
                    </li>
                    
                  </ul>
                </div>
                
              </form>
          </div>
        </div>
      </div>
  </div>
    <style type="text/css">
      #bodyCell{display: none;}
    </style>

<?php
 
  get_footer();

} else {

    if(isset($_REQUEST['entidade']))    
      $entidade = $_REQUEST['entidade'];
    else
      $entidade = $_REQUEST['syndicate_doc'];

    if($entidade == "")
      $entidade = $_REQUEST['syndicate_doc'];

    $cnpj = $_REQUEST['cnpj'];
    $cpf = $_REQUEST['cpf'];    
    $sexo = $_REQUEST['sexo'];    
    $nome = $_REQUEST['nome'];
    $logradouro = $_REQUEST['logradouro'];
    $numero = $_REQUEST['numero'];
    $comp = $_REQUEST['comp'];
    $bairro = $_REQUEST['bairro'];
    $cidade = $_REQUEST['cidade'];
    $uf = $_REQUEST['uf'];
    $telefone = $_REQUEST['telefone'];
    $celular = $_REQUEST['celular'];
    $email = $_REQUEST['email'];
    $dtnasc = $_REQUEST['dtnasc'];    
    $endtrab = $_REQUEST['endtrab'];
    $nome_conj = $_REQUEST['nome_conj'];
    $nome_pai = $_REQUEST['nome_pai'];
    $nome_mae = $_REQUEST['nome_mae'];
    $foto = $_REQUEST['foto'];

    $ctps = $_REQUEST['ctps'];
    $cargo = $_REQUEST['cargo'];
    $dataadmissao = $_REQUEST['dataadmissao'];
    $cidnascimento = $_REQUEST['cidnascimento'];
    $estadocivil = $_REQUEST['estadocivil'];

    $cnh = $_REQUEST['cnh'];
    $cnh_cat = $_REQUEST['cnh_cat'];
    $ctps_serie = $_REQUEST['ctps_serie'];
    $ctps_emissao = $_REQUEST['ctps_emissao'];
    $pis = $_REQUEST['pis'];
    $nacionalidade = $_REQUEST['nacionalidade'];
    $escolaridade = $_REQUEST['escolaridade'];
    $localtrab = $_REQUEST['localtrab'];

    $dtnasc  = date("d/m/Y",strtotime($dtnasc));
    $dtadmis = date("d/m/Y",strtotime($dtadmis));

    $rg = $_REQUEST['rg'];
    $cep = $_REQUEST['cep'];
    //$funcao = $_REQUEST['funcao'];
    //$escolaridade = $_REQUEST['escolaridade'];
    //$cbo = $_REQUEST['cbo'];
    //$dtadmis = $_REQUEST['dtadmis'];

    //$dtadmis = "" ;
    //$rg = "" ;
    $funcao = "" ;
    $escolaridade = "" ;
    $cbo = "" ;
    //$dtadmis = "" ;

    $employee_id = $_REQUEST['employee_id'];
    $employee_name = $_REQUEST['employee_name'];
    $employee_doc = $_REQUEST['employee_doc'];
    $employee_birthDate = $_REQUEST['employee_birthDate'];

    $employer_id = $_REQUEST['employer_id'];
    $employer_name = $_REQUEST['employer_name'];
    $employer_doc = $_REQUEST['employer_doc'];
    $syndicate_id = $_REQUEST['syndicate_id'];
    $syndicate_company = $_REQUEST['syndicate_company'];
    $syndicate_name = $_REQUEST['syndicate_name'];
    $syndicate_doc = $_REQUEST['syndicate_doc']; 
    $syndicate_email = $_REQUEST['syndicate_email']; 

    $syndicate_plano = $_REQUEST['syndicate_plano'];
    $syndicate_plano_valor = $_REQUEST['syndicate_plano_valor'];
    $syndicate_assinatura = $_REQUEST['syndicate_assinatura'];

    $employer_cep = $_REQUEST['employer_cep'];
    $employer_street = $_REQUEST['employer_street'];
    $employer_number = $_REQUEST['employer_number'];
    $employer_complement = $_REQUEST['employer_complement'];
    $employer_neighborhood = $_REQUEST['employer_neighborhood'];
    $employer_city = $_REQUEST['employer_city'];
    $employer_uf = $_REQUEST['employer_uf']; 

    $_celular = explode(")", $celular);
    $ddd = str_replace("(", "", $_celular[0]);
    $numCelular = str_replace(" ", "", $_celular[1]);


    if(isset($_REQUEST['dependenteNome']))    
      $dependenteNome = $_REQUEST['dependenteNome'];
    else
      $dependenteNome = "";

    if(isset($_REQUEST['dependenteDataNascimento']))    
      $dependenteDataNascimento = $_REQUEST['dependenteDataNascimento'];
    else
      $dependenteDataNascimento = "";

    if(isset($_REQUEST['dependenteParentesco']))    
      $dependenteParentesco = $_REQUEST['dependenteParentesco'];
    else
      $dependenteParentesco = ""; 

    if(isset($_REQUEST['dependenteSexo']))    
      $dependenteSexo = $_REQUEST['dependenteSexo'];
    else
      $dependenteSexo = ""; 

    $autorizaPagamento = $_REQUEST['autorizaPagamento'];
    $q83_assinatura = $_REQUEST['q83_assinatura'];

    $valorFiliacao = $_REQUEST['syndicate_plano_valor'];

    $autorizaPagamento = $autorizaPagamento[0];


    ?>

    <!doctype html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
        <head>
         <meta charset="utf-8">
         <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
         <meta name="description" content="">
         <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
         <link href="<?php echo home_url('/'); ?>wp-content/themes/base10layout/css/bootstrap.css" rel="stylesheet" type="text/css" />
         <link href="<?php echo home_url('/'); ?>wp-content/themes/base10layout/css/main.css" rel="stylesheet" type="text/css" />
         <script src="<?php echo home_url('/'); ?>wp-content/themes/base10layout/js/vendor/modernizr-2.6.2.min.js"></script>
         <script src="<?php echo home_url('/'); ?>wp-content/themes/base10layout/js/jquery-1.10.2.min.js"></script>

         <link href="<?php echo home_url('/'); ?>wp-content/themes/base10layout/css/pagseguro.css" rel="stylesheet" type="text/css" />

         <script src="<?php echo home_url('/'); ?>wp-content/themes/base10layout/js/plugins.js"></script>
         <script src="<?php echo home_url('/'); ?>wp-content/themes/base10layout/js/vendor/bootstrap.min.js"></script>

         <style type="text/css">
            .formaPagamento label{
              float: left;
              margin-left: 5px;
              margin-right: 12px;
            }

            .formaPagamento input {
              float: left;
              box-shadow: 0;
            }
            .creditCardFields{
              width: 100%;
              float: left;
            }
         </style>

         <!-- roimeter -->
         <title>Beneficio Social</title>
      </head>
      <body>
        <?php // require_once 'pagseguro/config.php'; ?>             

        <div class="container" id="pagando">
            <div class="row">
               <div class="buscaform">
                  <div class="col-xs-12 col-md-12">
                     <section id="all">
                        <aside>
                           <div id="cart" class="cart">
                              <h2>Resumo da compra</h2>
                              <h3>Autorizo a instituição <?php echo $syndicate_name; ?> a debitar, imediatamente o valor abaixo em meu cartão de crédito e também a fazer débitos recorrentes mensais. Esses débitos irão ocorrer até que eu solicite o cancelamento.</h3>
                              <table>
                                 <thead>
                                    <tr>
                                       <th>Descrição</th>
                                       <th>Valor</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <h3 title="Boné lettering">Filiação Sindical <?php echo $syndicate_name; ?></h3>
                                          Quantidade: 1<br>
                                          Valor do item: R$ <?php echo $valorFiliacao; ?><br>
                                          Periodicidade: mensal
                                       </td>
                                       <td>R$<strong><?php echo $valorFiliacao; ?></strong></td>
                                    </tr>
                                 </tbody>
                                 <tfoot>
                                    <tr id="totalRow">
                                       <td>
                                          <h3>Total a pagar</h3>
                                       </td>
                                       <td>
                                          <span id="cartTotalAmount">R$<strong><?php echo $valorFiliacao; ?></strong></span>
                                       </td>
                                    </tr>
                                 </tfoot>
                              </table>
                           </div>
                        </aside>
                        <div id="content">
                           <div class="sms-component--msg-anchor"></div>
                           <form id="payment" action="<?php echo home_url('filiar'); ?>" method="post" class="all tsmcustom">

                              <input type="hidden" id="entidade" name="entidade" value="<?php echo $entidade; ?>" />
                              <input type="hidden" id="syndicate_doc" name="syndicate_doc" value="<?php echo $syndicate_doc; ?>" />
                              <input type="hidden" id="cnpj" name="cnpj" value="<?php echo $cnpj; ?>" />
                              <input type="hidden" id="cpf" name="cpf" value="<?php echo $cpf; ?>" />
                              <input type="hidden" id="sexo" name="sexo" value="<?php echo $sexo; ?>" />
                              <input type="hidden" id="nome" name="nome" value="<?php echo $nome; ?>" />
                              <input type="hidden" id="logradouro" name="logradouro" value="<?php echo $logradouro; ?>" />
                              <input type="hidden" id="numero" name="numero" value="<?php echo $numero; ?>" />
                              <input type="hidden" id="comp" name="comp" value="<?php echo $comp; ?>" />
                              <input type="hidden" id="bairro" name="bairro" value="<?php echo $bairro; ?>" />
                              <input type="hidden" id="cidade" name="cidade" value="<?php echo $cidade; ?>" />
                              <input type="hidden" id="uf" name="uf" value="<?php echo $uf; ?>" />
                              <input type="hidden" id="telefone" name="telefone" value="<?php echo $telefone; ?>" />
                              <input type="hidden" id="celular" name="celular" value="<?php echo $celular; ?>" />
                              <input type="hidden" id="email" name="email" value="<?php echo $email; ?>" />
                              <input type="hidden" id="dtnasc" name="dtnasc" value="<?php echo $dtnasc; ?>" />
                              <input type="hidden" id="endtrab" name="endtrab" value="<?php echo $endtrab; ?>" />
                              <input type="hidden" id="nome_conj" name="nome_conj" value="<?php echo $nome_conj; ?>" />
                              <input type="hidden" id="nome_pai" name="nome_pai" value="<?php echo $nome_pai; ?>" />
                              <input type="hidden" id="nome_mae" name="nome_mae" value="<?php echo $nome_mae; ?>" />
                              <input type="hidden" id="foto" name="foto" value="<?php echo $foto; ?>" />
                              <input type="hidden" id="ctps" name="ctps" value="<?php echo $ctps; ?>" />
                              <input type="hidden" id="cargo" name="cargo" value="<?php echo $cargo; ?>" />
                              <input type="hidden" id="dataadmissao" name="dataadmissao" value="<?php echo $dataadmissao; ?>" />
                              <input type="hidden" id="cidnascimento" name="cidnascimento" value="<?php echo $cidnascimento; ?>" />
                              <input type="hidden" id="estadocivil" name="estadocivil" value="<?php echo $estadocivil; ?>" />
                              <input type="hidden" id="cnh" name="cnh" value="<?php echo $cnh; ?>" />
                              <input type="hidden" id="cnh_cat" name="cnh_cat" value="<?php echo $cnh_cat; ?>" />
                              <input type="hidden" id="ctps_serie" name="ctps_serie" value="<?php echo $ctps_serie; ?>" />
                              <input type="hidden" id="ctps_emissao" name="ctps_emissao" value="<?php echo $ctps_emissao; ?>" />
                              <input type="hidden" id="pis" name="pis" value="<?php echo $pis; ?>" />
                              <input type="hidden" id="nacionalidade" name="nacionalidade" value="<?php echo $nacionalidade; ?>" />
                              <input type="hidden" id="escolaridade" name="escolaridade" value="<?php echo $escolaridade; ?>" />
                              <input type="hidden" id="localtrab" name="localtrab" value="<?php echo $localtrab; ?>" />
                              <input type="hidden" id="rg" name="rg" value="<?php echo $rg; ?>" />
                              <input type="hidden" id="cep" name="cep" value="<?php echo $cep; ?>" />
                              <input type="hidden" id="employee_id" name="employee_id" value="<?php echo $employee_id; ?>" />
                              <input type="hidden" id="employee_name" name="employee_name" value="<?php echo $employee_name; ?>" />
                              <input type="hidden" id="employee_doc" name="employee_doc" value="<?php echo $employee_doc; ?>" />
                              <input type="hidden" id="employee_birthDate" name="employee_birthDate" value="<?php echo $employee_birthDate; ?>" />
                              <input type="hidden" id="employer_id" name="employer_id" value="<?php echo $employer_id; ?>" />
                              <input type="hidden" id="employer_name" name="employer_name" value="<?php echo $employer_name; ?>" />
                              <input type="hidden" id="employer_doc" name="employer_doc" value="<?php echo $employer_doc; ?>" />
                              <input type="hidden" id="syndicate_id" name="syndicate_id" value="<?php echo $syndicate_id; ?>" />
                              <input type="hidden" id="syndicate_company" name="syndicate_company" value="<?php echo $syndicate_company; ?>" />
                              <input type="hidden" id="syndicate_name" name="syndicate_name" value="<?php echo $syndicate_name; ?>" />
                              <input type="hidden" id="syndicate_doc" name="syndicate_doc" value="<?php echo $syndicate_doc; ?>" />
                              <input type="hidden" id="syndicate_email" name="syndicate_email" value="<?php echo $syndicate_email; ?>" />
                              <input type="hidden" id="employer_cep" name="employer_cep" value="<?php echo $employer_cep; ?>" />
                              <input type="hidden" id="employer_street" name="employer_street" value="<?php echo $employer_street; ?>" />
                              <input type="hidden" id="employer_number" name="employer_number" value="<?php echo $employer_number; ?>" />
                              <input type="hidden" id="employer_complement" name="employer_complement" value="<?php echo $employer_complement; ?>" />
                              <input type="hidden" id="employer_neighborhood" name="employer_neighborhood" value="<?php echo $employer_neighborhood; ?>" />
                              <input type="hidden" id="employer_city" name="employer_city" value="<?php echo $employer_city; ?>"
                               />

                              <input type="hidden" id="valorFiliacao" name="valorFiliacao" value="<?php echo $valorFiliacao; ?>" />

                              <?php
                                $cont = 0;
                                foreach ($dependenteNome as $_dependenteNome) {                                                                       
                                  ?>
                                    <input type="hidden" id="dependenteNome" name="dependenteNome[]" value="<?php echo $_dependenteNome; ?>" />
                                    <input type="hidden" id="dependenteDataNascimento" name="dependenteDataNascimento[]" value="<?php echo $dependenteDataNascimento[$cont]; ?>" />
                                    <input type="hidden" id="dependenteParentesco" name="dependenteParentesco[]" value="<?php echo $dependenteParentesco[$cont]; ?>" />

                                    <input type="hidden" id="dependenteSexo" name="dependenteSexo[]" value="<?php echo $dependenteSexo[$cont]; ?>" />
                                  <?php

                                $cont++;
                                }
                                
                                ?>

                              <input type="hidden" id="employer_uf" name="employer_uf" value="<?php echo $employer_uf; ?>" />
                              
                              
                              <input type="hidden" id="autorizaPagamento" name="autorizaPagamento" value="<?php echo $autorizaPagamento; ?>" />
                              <input type="hidden" id="q83_assinatura" name="q83_assinatura" value="<?php echo $q83_assinatura; ?>" />

                              <input type="hidden" id="idAssinatura" name="idAssinatura" value="" />
                              <input type="hidden" name="gerarFicha" id="gerarFicha" value="gerarFicha" />

                              <input type="hidden" id="input_syndicate_plano" name="syndicate_plano" value="<?php echo $syndicate_plano; ?>" />

                              <input type="hidden" id="input_syndicate_plano_valor" name="syndicate_plano_valor" value="<?php echo $syndicate_plano_valor; ?>" />

                              <input type="hidden" id="dataAdesao" name="dataAdesao" value="" />

                              <input type="hidden" id="referencia" name="referencia" value="<?php echo $syndicate_name.'-'.$cpf; ?>" />
                              <input type="hidden" id="ddd" name="ddd" value="<?php echo $ddd; ?>" />
                              <input type="hidden" id="numCelular" name="numCelular" value="<?php echo $numCelular; ?>" />
                              <input type="hidden" id="numip" name="numip" value="<?php echo $_SERVER["REMOTE_ADDR"]; ?>" />
                              <input type="hidden" id="input_syndicate_assinatura" name="syndicate_assinatura" value="<?php echo $syndicate_assinatura; ?>" />

                              <fieldset class="payment-step content" data-omniture-section="Meios de pagamento">
                                 <h1>Forma de Pagamento</h1>
                                 
                                 <div class="creditCardFields">
                                   <div class="step" id="cardDataContainer" step-title="Forma de Pagamento">
                                      <div class="double-columns first-block">
                                        <div id="ccBrandConsult" class="field">
                                             <label for="creditCardNumber">Informe como deseja usar o cartão</label>

                                             <div class="ccBrandConsultWrapper">
                                               <div class="form-single-column formaPagamento" data-component="radio">
                                                 
                                                  <input type="radio" class="form-radio" id="formaPagamentoC" name="formaPagamento[]" value="C" required="" checked="" />
                                                  <label id="label_formaPagamentoC" for="formaPagamentoC">Crédito</label>
                                               
                                                  <input type="radio" class="form-radio" id="formaPagamentoD" name="formaPagamento[]" value="D" required="" />
                                                  <label id="label_formaPagamentoD" for="formaPagamentoD">Débito</label>

                                                  <label id="label_formaPagamentoD" for="formaPagamentoD" style="font-size: 12px;font-weight: bold;margin: 0;">Verifique junto ao seu banco a disponibilidade do pagamento com CARTÃO DE DÉBITO</label>
                                                
                                              </div>
                                             </div>                                             
                                          </div>
                                      </div>
                                    </div>

                                  </div>

                                 
                                 
                                 <div class="creditCardFields">
                                    
                                    <div class="step subcontent" id="cardDataContainer" step-title="Alterar o cartão">
                                       <h1>Pagamento</h1>
                                       <div class="double-columns first-block">
                                          <div id="ccBrandConsult" class="field">
                                             <label for="creditCardNumber">Número do cartão</label>
                                             <div class="ccBrandConsultWrapper">
                                                <input data-title="Número do cartão" type="text" id="creditCardNumber" value="" maxlength="16" autocomplete="off">
                                                <input type="hidden" id="creditCardToken" value="">
                                                <p class="insertion-guide">APENAS NUMEROS</p>
                                             </div>
                                             
                                          </div>                                          


                                          <div class="field cardDueDate ">
                                             <label for="creditCardDueDate_Month">Data de validade</label>
                                             <input data-title="Data de validade do cartão" type="text" data-autotab-target="creditCardDueDate_Year" placeholder="MM" maxlength="2" class="small" id="creditCardDueDate_Month" required="true">
                                             <input data-title="Data de validade do cartão" type="text" placeholder="AA" maxlength="2" class="small" id="creditCardDueDate_Year" required="true">
                                          </div>

                                          <div class="clear-all"></div>

                                          <div class="field cardHolderName  a">
                                             <label for="creditCardHolderName">
                                             Nome do dono do cartão
                                             </label>
                                             <input data-title="Nome do dono do cartão" class="bigger" type="text" id="creditCardHolderName" required="true" name="holderName">
                                             <p class="insertion-guide">Ex.: CARLOS A F DE OLIVEIRA</p>
                                          </div>

                                          <div class="field cardCvv ">
                                             <label for="creditCardCVV">Código de segurança <a class="cvvDetail questionMark omniture-click" data-omniture-action="Código de segurança (CVV) - Tooltip de ajuda" href="#creditCardCvvInfo" rel="tooltip-0">?</a></label>
                                             <input data-title="Código de segurança do cartão" class="medium" type="text" id="creditCardCVV" required="true" value="" autocomplete="off" maxlength="3">
                                          </div>

                                          
                                       </div>
                                    </div>
                                    <!-- step -->
                                    <div class="clear-all"></div>
                                    <div class="step subcontent" id="cardHolderContainer" step-title="Informar dados do dono do cartão">
                                       <div id="cardHolderDataFields">
                                          <div id="cardHolderData" class="holderData double-columns first-block">
                                             <h1>Dados do dono do cartão</h1>
                                             <div class="holderCPFField field  a ">
                                                <label for="holderCPF">
                                                CPF do dono do cartão
                                                </label>
                                                <input class="bigger" type="text" id="holderCPF" name="holderCPF" required="true" value="" data-title="CPF do dono do cartão" maxlength="14" autocomplete="off">
                                             </div>
                                             <div class="holderPhoneField field ">
                                                <label for="holderAreaCode">
                                                Celular do dono do cartão
                                                </label>
                                                <input class="small" type="text" data-autotab-target="holderPhone" maxlength="2" id="holderAreaCode" name="holderAreaCode" value="" data-title="Celular do dono do cartão">
                                                <input class="big" type="text" maxlength="9" id="holderPhone" required="true" name="holderPhone" value="" data-title="Celular do dono do cartão">
                                             </div>
                                             <input type="hidden" name="holderCanEditPhone" value="true">
                                             <div class="holderBornDateField field  a ">
                                                <label for="holderBornDate">
                                                Data de nascimento
                                                </label>
                                                <input class="medium" type="text" id="holderBornDate" required="true" name="holderBornDate" value="" data-title="Data de nascimento">
                                                <p class="insertion-guide">Ex.: 20/05/1980</p>
                                             </div>
                                             <div class="clear-all"></div>
                                          </div>
                                       </div>
                                      
                                      
                                       <div class="clear-all"></div>
                                    </div>
                                    <!-- step -->
                                 </div>
                                 
                              </fieldset>
                              <div id="mainAction" class="content">
                                 

                                 <div id="progressLoader" class="loadBox">
                                    <div id="progress-message" class="uolMsg uolMsg-medium uolMsg-loading ">
                                       <h3>Processando o seu pagamento. Aguarde...</h3>
                                    </div>
                                    <div id="credicCardProgress">
                                       <div class="bar">
                                          <div class="pct themeBgColor themeTextColor" style="width:0%;" data-progress="0"></div>
                                       </div>
                                       <p><strong class="rounded themeTextColor">0</strong></p>
                                    </div>
                                 </div>

                                 <button type="button" id="continueToPayment" class="pagseguro-button mainActionButton large">Confirmar o pagamento</button>
                                 <span id="avisoAssinatura" style="color: red;text-align: center;width: 100%;float: left;font-size: 12px;font-weight: bold; display: none;"><br>POR FAVOR, PREENCHA OS CAMPOS ACIMA CORRETAMENTE
                                  <br><p id="erroApi"></p>
                                 </span>
                                 <p class="mainActionObs">Seu pagamento passará por uma análise interna e estará sujeito à confirmação feita por telefone ou e-mail. Você sempre será notificado, por e-mail, sobre qualquer alteração no fluxo de processamento do seu pedido.</p>

                              </div>
                           </form>
                        </div>
                     </section>
                  </div>
               </div>
            </div>
        </div>

          <script type="text/javascript" src="<?php echo URL_STC; ?>/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>

          <?php

              // $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/pre-approvals/request';
              // $data['email'] = 'fernando.compuway@gmail.com';
              // $data['token'] = '18BC8067AE8A4C8F8EAC6D22142F6070';
              // $data['currency'] = 'BRL';
              // $data['senderName'] = "Fernando";
              // $data['senderEmail'] = "base10it@gmail.com";                    
              // $data['preApprovalCharge'] = 'auto';
              // $data['preApprovalName'] = utf8_encode("FILIAÇÃO BSF");
              // $data['preApprovalDetails'] = utf8_encode("Todo dia 28 será cobrado o valor de R$60,00 referente a contrib. sindical");
              // $data['preApprovalAmountPerPayment'] = 59.99;
              // $data['preApprovalPeriod'] = 'MONTHLY';
              // $data['preApprovalMaxTotalAmount'] = '99999.00';
              // $data = http_build_query($data);
              // $curl = curl_init($url);
              // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
              // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
              // curl_setopt($curl, CURLOPT_POST, true);
              // curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
              // curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
              // $xml= curl_exec($curl);
              
              // if($xml == 'Unauthorized'){
              //   echo "Unauthorized";
              //   exit();
              // }
              // curl_close($curl);
              // $xml= simplexml_load_string($xml);
              // if(count($xml->error) > 0){
              //   echo "XML ERRO";
              //   exit();
              // }
            
              // var_dump($xml);
            
              

              $curl = curl_init();
            
              curl_setopt_array($curl, array(
                  CURLOPT_URL => URL_WS."/v2/sessions?email=".EMAIL_PGS."&token=".TOKEN_PGS,
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS => ""
              ));

              $getSession = curl_exec($curl);
              $err = curl_error($curl);
              $sessionID = "";

              curl_close($curl);

              if ($err) {
                  // echo "cURL Error #:" . $err;
              } else {
                 $xmlSession = simplexml_load_string($getSession);
                 $sessionID = (string)$xmlSession->id;
              }

          ?>
              <script type="text/javascript">
                  PagSeguroDirectPayment.setSessionId('<?php echo $sessionID; ?>');

                  var bandeiraCartao = "";
                  var card_Token = "";
                  var hash = "";
                  var codigoAssinatura = "";

                  PagSeguroDirectPayment.onSenderHashReady(function(response){                      
                      hash = response.senderHash; 
                  });

                  function get_Brand(cardBin) {
                      
                      PagSeguroDirectPayment.getBrand({
                          cardBin: cardBin,
                          success: function(response) {
                            bandeiraCartao = response.brand.name;
                            $('#creditCardToken').val(bandeiraCartao);
                          },
                          error: function(response) {
                            //tratamento do erro
                          },
                          complete: function(response) {
                            //tratamento comum para todas chamadas
                          }
                      });
                  }

                  function cardToken(callback) {
                      PagSeguroDirectPayment.createCardToken({
                          cardNumber: $("#creditCardNumber").val(), // Número do cartão de crédito
                          brand: bandeiraCartao, // Bandeira do cartão bandeiraCartao
                          cvv: $("#creditCardCVV").val(), // CVV do cartão
                          expirationMonth: $("#creditCardDueDate_Month").val(), // Mês da expiração do cartão
                          expirationYear: '20'+$("#creditCardDueDate_Year").val(), // Ano da expiração do cartão, é necessário os 4 dígitos.
                          success: function(response) {
                              // Retorna o cartão tokenizado.
                              card_Token = response.card.token;
                              //console.dir(card_Token);
                              callback();
                              // 
                          },
                          error: function(response) {
                            // Callback para chamadas que falharam.
                            console.log("error: ");
                            console.dir(response);

                            $("#erroApi").text('Cartão de Crédito Inválido');
                            $("#erroApi").show();
                            $("#avisoAssinatura").show();
                            $("#progressLoader").hide();
                          },
                          complete: function(response) {
                              
                          }
                      });
                  }

                  function efetivaTransacao () {
                      console.log("efetivaTransacao");
                      $.post("<?php echo home_url('cadastro');?>",
                      {                                        
                          cardToken: card_Token,
                          processaPagamento: 'processaPagamento',
                          hash: hash,
                          plan: $("#input_syndicate_assinatura").val(),
                          reference: $("#referencia").val(),
                          nomePessoa: $("#nome").val(),
                          email: $("#email").val(),
                          ip: $("#numip").val(),
                          areaCode: $("#ddd").val(),
                          numCelular: $("#numCelular").val(),
                          street: $("#logradouro").val(),
                          number: $("#numero").val(),
                          complement: $("#comp").val(),
                          district: $("#bairro").val(),
                          city: $("#cidade").val(),
                          state: $("#uf").val(),
                          country: 'BRA',
                          postalCode: $("#cep").val(),
                          valueCPF: $("#cpf").val(),
                          paymentMethodBirthDate: $("#holderBornDate").val(),
                          paymentMethodCPF: $("#holderCPF").val(),
                          paymentName: $("#creditCardHolderName").val(),
                          cnpj: $("#cnpj").val(),
                          sindicatonome: $("#syndicate_name").val(),
                          valorplano: $("#input_syndicate_plano_valor").val(),
                          syndicate_plano: $("#input_syndicate_plano").val(),
                          flag: $('#creditCardToken').val(),
                          creditCardNumber: $("#creditCardNumber").val(), 
                          paymentMethodType: $('input[name=formaPagamento]:checked').val(),
                          cvv: $("#creditCardCVV").val(),
                          validate: $("#creditCardDueDate_Month").val() +'/20'+$("#creditCardDueDate_Year").val(),
                          valor: $("#valorFiliacao").val()
                      },
                          function(data){
                              console.log(data); 
                              $("#progressLoader").hide();
                              codigoAssinatura = data; 
                              console.log(codigoAssinatura);
                              if(codigoAssinatura.indexOf("sucesso_") >= 0) { 
                                $("#pagando").hide();
                                $("#finalizacao").show(); 
                                $("#idAssinatura").val(codigoAssinatura);
                                $('form#payment').submit();
                              } else {
                                $("#erroApi").text(data);
                                $("#erroApi").show();
                                $("#avisoAssinatura").show();
                              }

                                                  
                          }
                      )
                  }

                  
                  
                  $('#creditCardNumber').on('input',function(e){
                      var numeroCartao = $('#creditCardNumber').val();

                      if(numeroCartao.length == 6){
                          get_Brand(numeroCartao);
                      }
                  });

                  $('#continueToPayment').on( "click", function(e){


                      var required = $('[required="true"]'); // change to [required] if not using true option as part of the attribute as it is not really needed.
                      var error = false;

                      for(var i = 0; i <= (required.length - 1);i++)
                      {
                        if(required[i].value == '') // tests that each required value does not equal blank, you could put in more stringent checks here if you wish.
                        {
                            required[i].style.backgroundColor = 'rgb(255,155,155)';
                            error = true; // if any inputs fail validation then the error variable will be set to true;     
                        }
                      }

                      if(error) // if error is true;
                      {
                          $("#avisoAssinatura").show();
                            return false; // stop the form from being submitted.
                      } else {
                          e.preventDefault();
                          $("#erroApi").hide();
                          $("#avisoAssinatura").hide();
                          $("#progressLoader").show();
                          cardToken(
                              function () { 
                                  efetivaTransacao(); 
                              }
                          );
                      }                            
                  });  
              </script>
    <?php
    

    get_footer();

  }
}



?>  