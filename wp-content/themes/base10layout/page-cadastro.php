<?php
	//get_header();
require_once 'pagseguro/config.php';

global $wpdb;

// $ambiente = "teste";
// $urlBsf = "https://api.bsfonline.com.br";
// if($ambiente == "teste"){
//   $urlBsf = "https://cobranca.preprod.bsfonline.com.br";
// }

if(isset($_REQUEST['cancelarAdesao'])){
  $idAdesao = $_REQUEST['idAdesao']; 
  $ID_Planos_Adesao = $_REQUEST['ID_Planos_Adesao']; 

  $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL => urlBsf."/api-v1/gateway/pre-approvals/cancel/".$idAdesao."?email=".EMAIL_PGS."&token=".TOKEN_PGS,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET"
  ));

  $response = curl_exec($curl);
  //var_dump($response);
  $err = curl_error($curl);

  curl_close($curl);


  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
    $sessionJson = json_encode($response);
    // var_dump($sessionJson);
    if($sessionJson->result->status){

      $sql = "INSERT INTO `Planos_Adesao_Status` (`ID_Adesao`,`ID_Planos_Adesao`,`DT_Status`,`Status`) ";
      $sql .= "values ('".$idAdesao."','".$ID_Planos_Adesao."','".date('d/m/Y')."','Cancelamento');";

      $executarsql = $wpdb->get_results($sql);

      echo "cancelado";
    }
  }
}


if(isset($_REQUEST['processaPagamento'])){
  $cardToken = $_REQUEST['cardToken'];
  $hash = $_REQUEST['hash'];

  $plan = $_REQUEST['plan']; 
  $reference = $_REQUEST['reference']; 
  $nomePessoa = $_REQUEST['nomePessoa']; 
  $email = $_REQUEST['email']; 
  $ip = $_REQUEST['ip']; 
  $areaCode = $_REQUEST['areaCode']; 
  $numCelular = $_REQUEST['numCelular']; 
  $street = $_REQUEST['street']; 
  $number = $_REQUEST['number']; 
  $complement = $_REQUEST['complement']; 
  $district = $_REQUEST['district']; 
  $city = $_REQUEST['city']; 
  $state = strtoupper($_REQUEST['state']); 
  $country = $_REQUEST['country']; 
  $postalCode = $_REQUEST['postalCode']; 
  $valueCPF = $_REQUEST['valueCPF']; 
  $paymentMethodBirthDate = $_REQUEST['paymentMethodBirthDate']; 
  $paymentMethodCPF = $_REQUEST['paymentMethodCPF']; 
  $paymentName = $_REQUEST['paymentName']; 
  $cnpj = $_REQUEST['cnpj']; 
  $sindicatonome = $_REQUEST['sindicatonome']; 
  $valorplano = $_REQUEST['valorplano'];
  $syndicate_plano = $_REQUEST['syndicate_plano'];
  $creditCardNumber = $_REQUEST['creditCardNumber'];
  $flag = $_REQUEST['flag'];
  $validate = $_REQUEST['validate'];
  $cvv = $_REQUEST['cvv'];
  $valor = $_REQUEST['valor'];
  $paymentMethodType = $_REQUEST['paymentMethodType'];

  // if(AMBIENTE == "sandbox") {
  //   $plan = "2454FC8A646461D884CEDFA0E9E4F144";
  //   $email = "c13130046833224601086@sandbox.pagseguro.com.br";
  // }

  $numCelular = str_replace("-", "", $numCelular);

   $dadosPayload =  array (
    'plan' => $plan,
    'reference' => $reference,
    'sender' => 
    array (
      'name' => $nomePessoa,
      'email' => $email,
      'ip' => $ip,
      'hash' => $hash,
      'phone' => 
      array (
        'areaCode' => $areaCode,
        'number' => $numCelular,
      ),
      'address' => 
      array (
        'street' => $street,
        'number' => $number,
        'complement' => $complement,
        'district' => $district,
        'city' => $city,
        'state' => $state,
        'country' => $country,
        'postalCode' => $postalCode,
      ),
      'documents' => 
      array (
        0 => 
        array (
          'type' => 'CPF',
          'value' => $valueCPF,
        ),
      ),
    ),
    'paymentMethod' => 
    array (
      'type' => 'CREDITCARD',
      'creditCard' => 
      array (
        'type' => $paymentMethodType,
        'number' => $creditCardNumber,
        'flag' => $flag,
        'validate' => $validate,
        'cvv' => $cvv,
        'valor' => str_replace(".", "", $valorplano),
        'holder' => 
        array (
          'name' => $paymentName,
          'birthDate' => $paymentMethodBirthDate,
          'documents' => 
          array (
            0 => 
            array (
              'type' => 'CPF',
              'value' => $paymentMethodCPF,
            ),
          ),
          'billingAddress' => 
          array (
            'street' => $street,
            'number' => $number,
            'complement' => $complement,
            'district' => $district,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'postalCode' => $postalCode,
          ),
          'phone' => 
          array (
            'areaCode' => $areaCode,
            'number' => $numCelular,
          ),
        ),
      ),
    ),
  );

  $dadosPayload = json_encode($dadosPayload);

  // // CURLOPT_URL => URL_WS."/pre-approvals?email=".EMAIL_PGS."&token=".TOKEN_PGS,
  // $curl = curl_init();
  //   curl_setopt_array($curl, array(
  //   CURLOPT_URL => urlBsf."/api-v1/gateway/filiacao/pre-approvals",
  //   CURLOPT_RETURNTRANSFER => true,
  //   CURLOPT_ENCODING => "",
  //   CURLOPT_MAXREDIRS => 10,
  //   CURLOPT_TIMEOUT => 30,
  //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  //   CURLOPT_CUSTOMREQUEST => "POST",
  //   CURLOPT_POSTFIELDS => $dadosPayload,
  //   CURLOPT_HTTPHEADER => array(
  //     'Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1',
  //             'Content-Type: application/json; charset=UTF-8'  
  //   ),
  // ));


  $ch = curl_init(urlBsf."/api-v1/gateway/filiacao/pre-approvals");
        curl_setopt_array($ch, array(
          CURLOPT_POST => TRUE,
          CURLOPT_RETURNTRANSFER => TRUE,
          CURLOPT_HTTPHEADER => array(
              'Content-Type: application/json'
          ),
          CURLOPT_POSTFIELDS => $dadosPayload
        ));

  $response = curl_exec($ch);

  // var_dump($dadosPayload);

  ///var_dump($response);

  $http = curl_getinfo($ch);
  curl_close($ch);   

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
    $xmlResponse = simplexml_load_string($response);
    $xmlCode = (string)$xmlResponse->code;
    
    if($xmlResponse->code){
      $sessionID = (string)$xmlResponse->code;      
  
      $sql = "INSERT INTO `Planos_Adesao` (`Nome`,`CPF`,`Sindicato_CNPJ`,`Sindicato`,`ID_Plano`, `ID_Adesao`, `DT_Adesao`, `syndicate_name`,`syndicate_plano`,`syndicate_plano_valor`,`body_enviado`)";
      $sql .= "values ('".$nomePessoa."','".$valueCPF."','".$cnpj."','".$sindicatonome."','".$plan."','".(string)$xmlResponse->code."','".date('d/m/Y')."','".$sindicatonome."','".$syndicate_plano."','".$valorplano."','".$dadosPayload."');";
      
      $executarsql = $wpdb->get_results($sql);

      $sql = "SELECT * FROM Planos_Adesao where ID_Plano = '".$plan."'";
      $resultados = $wpdb->get_results($sql);
      $ID_Planos_Adesao = "";
      foreach ($resultados as $value) {
        $ID_Planos_Adesao = $value->ID_Planos_Adesao;
      }

      $sql = "INSERT INTO `Planos_Adesao_Status` (`ID_Planos_Adesao`,`DT_Status`,`Status`) ";
      $sql .= "values ('".$ID_Planos_Adesao."','".date('d/m/Y')."','Adesão do Plano');";

      // echo $sql;
      $executarsql = $wpdb->get_results($sql);

      $sql = "SELECT * FROM usuarios where email = '".$email."' AND ID_Plano = '".$plan."'";
      $resultados = $wpdb->get_results($sql);
      $usuExistente = false;
      foreach ($resultados as $value) {
        $usuExistente = true;
      }

      if($usuExistente == false){
        $sql = "INSERT INTO `usuarios` (`nome`,`email`,`senha`,`idPlano`,`situacoe_id`,`niveis_acesso_id`,`created`,`modified`) ";
        $sql .= "values ('".$nomePessoa."','".$email."','".$valueCPF."','".$plan."','1','1','".date('d/m/Y')."','".date('d/m/Y')."');";
        $executarsql = $wpdb->get_results($sql);
      }      


      $sessionID = "sucesso_".$sessionID;
      echo $sessionID;
    } else{
      $sessionID = json_decode($response, true);
      //var_dump($sessionID);
      echo $sessionID['message']." - Status:".$sessionID['status'];
      // $erros = $sessionID['errors'];
      // foreach ($erros as $_erros) {
      //   echo $_erros." ";
      // }
      //var_dump($sessionID);
    }
  }

  curl_close($ch);
}


if(isset($_REQUEST['pagar'])){

   

  $cardToken = $_REQUEST['cardToken'];
  $hash = $_REQUEST['hash'];

  $plan = $_REQUEST['plan']; 
  $reference = $_REQUEST['reference']; 
  $nomePessoa = $_REQUEST['nomePessoa']; 
  $email = $_REQUEST['email']; 
  $ip = $_REQUEST['ip']; 
  $areaCode = $_REQUEST['areaCode']; 
  $numCelular = $_REQUEST['numCelular']; 
  $street = $_REQUEST['street']; 
  $number = $_REQUEST['number']; 
  $complement = $_REQUEST['complement']; 
  $district = $_REQUEST['district']; 
  $city = $_REQUEST['city']; 
  $state = strtoupper($_REQUEST['state']); 
  $country = $_REQUEST['country']; 
  $postalCode = $_REQUEST['postalCode']; 
  $valueCPF = $_REQUEST['valueCPF']; 
  $paymentMethodBirthDate = $_REQUEST['paymentMethodBirthDate']; 
  $paymentMethodCPF = $_REQUEST['paymentMethodCPF']; 
  $paymentName = $_REQUEST['paymentName']; 
  $cnpj = $_REQUEST['cnpj']; 
  $sindicatonome = $_REQUEST['sindicatonome']; 
  $valorplano = $_REQUEST['valorplano'];
  $syndicate_plano = $_REQUEST['syndicate_plano'];
  $creditCardNumber = $_REQUEST['creditCardNumber'];
  $flag = $_REQUEST['flag'];
  $validate = $_REQUEST['validate'];
  $cvv = $_REQUEST['cvv'];
  $valor = $_REQUEST['valor'];
  $paymentMethodType = $_REQUEST['paymentMethodType'];

  // if(AMBIENTE == "sandbox") {
  //   $plan = "2454FC8A646461D884CEDFA0E9E4F144";
  //   $email = "c13130046833224601086@sandbox.pagseguro.com.br";
  // }

  $numCelular = str_replace("-", "", $numCelular);

   $dadosPayload =  array (
    'plan' => $plan,
    'reference' => $reference,
    'sender' => 
    array (
      'name' => $nomePessoa,
      'email' => $email,
      'ip' => $ip,
      'hash' => $hash,
      'phone' => 
      array (
        'areaCode' => $areaCode,
        'number' => $numCelular,
      ),
      'address' => 
      array (
        'street' => $street,
        'number' => $number,
        'complement' => $complement,
        'district' => $district,
        'city' => $city,
        'state' => $state,
        'country' => $country,
        'postalCode' => $postalCode,
      ),
      'documents' => 
      array (
        0 => 
        array (
          'type' => 'CPF',
          'value' => $valueCPF,
        ),
      ),
    ),
    'paymentMethod' => 
    array (
      'type' => 'CREDITCARD',
      'creditCard' => 
      array (
        'type' => 'D',
        'number' => $creditCardNumber,
        'flag' => $flag,
        'validate' => $validate,
        'cvv' => $cvv,
        'valor' => str_replace(".", "", $valorplano),
        'holder' => 
        array (
          'name' => $paymentName,
          'birthDate' => $paymentMethodBirthDate,
          'documents' => 
          array (
            0 => 
            array (
              'type' => 'CPF',
              'value' => $paymentMethodCPF,
            ),
          ),
          'billingAddress' => 
          array (
            'street' => $street,
            'number' => $number,
            'complement' => $complement,
            'district' => $district,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'postalCode' => $postalCode,
          ),
          'phone' => 
          array (
            'areaCode' => $areaCode,
            'number' => $numCelular,
          ),
        ),
      ),
    ),
  );

  $dadosPayload = json_encode($dadosPayload);


  $ch = curl_init(urlBsf."/api-v1/gateway/filiacao/pre-approvals");
        curl_setopt_array($ch, array(
          CURLOPT_POST => TRUE,
          CURLOPT_RETURNTRANSFER => TRUE,
          CURLOPT_HTTPHEADER => array(
              'Content-Type: application/json'
          ),
          CURLOPT_POSTFIELDS => $dadosPayload
        ));

  $response = curl_exec($ch);

  // var_dump($dadosPayload);

  ///var_dump($response);

  $http = curl_getinfo($ch);
  curl_close($ch);   

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
    $xmlResponse = simplexml_load_string($response);
    $xmlCode = (string)$xmlResponse->code;
    
    if($xmlResponse->code){
      $sessionID = (string)$xmlResponse->code;      

      $sessionID = "sucesso_".$sessionID;
      echo $sessionID;
    } else{
      $sessionID = json_decode($response, true);
      //var_dump($sessionID);
      echo $sessionID['message']." - Status:".$sessionID['status'];
     
    }
  }

  curl_close($ch);

}


if(isset($_REQUEST['getPagamentos'])){

                  $email = EMAIL_PGS;
                  $token = TOKEN_PGS;
                  $notificationCode = $_POST['notificationCode']; //"8F7B07FE9AB89AB83375547A0FAECE51E192";
                  $url = urlBsf."/api-v1/gateway/transactions/notifications/".$notificationCode."?email=".$email."&token=".$token;
 
                  $curl = curl_init($url);
                  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                  //curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/vnd.pagseguro.com.br.v3+xml;charset=ISO-8859-1',
                  //                                             'Content-Type: application/xml; charset=UTF-8'));

                  $response = curl_exec($curl);
                  
                  $http = curl_getinfo($curl);

                  $_response = simplexml_load_string($response);
 
                  if($response == 'Unauthorized'){
                            print_r($response);
                            exit;
                  }
                                   
 
                  if(count($_response -> error) > 0){
                            print_r($_response);
                            exit;
                  }

                  curl_close($curl);                  

                  $dadosReferencia = explode("-", $_response->reference);
                  
                  $syndicate_name = $dadosReferencia[0];               
                  $CPF = $dadosReferencia[1];               

                  $ID_Adesao = str_replace("-", "", $_response->code);

                  $sql = "SELECT * FROM Planos_Adesao WHERE CPF = '".$CPF."' AND Sindicato = '".$syndicate_name."'";
                  $resultados = $wpdb->get_results($sql);

                  $ID_Planos_Adesao = "";

                  foreach ($resultados as $value) {
                    $ID_Planos_Adesao = $value->ID_Planos_Adesao;
                    $ID_Adesao = $value->ID_Adesao;
                    $CPF = $value->CPF;
                    $syndicate_name = $value->syndicate_name;
                    $syndicate_plano = $value->syndicate_plano;
                    $ID_Plano = $value->ID_Plano;
                    $syndicate_plano_valor = $value->syndicate_plano_valor;
                  }

                  $statusTransacao = "";

                  switch ($_response->status) {
                    case '1':
                      $statusTransacao = "Aguardando pagamento";
                      break;
                    case '2':
                      $statusTransacao = "Em análise";
                      break;
                    case '3':
                      $statusTransacao = "Paga";
                      break;
                    case '4':
                      $statusTransacao = "Disponível";
                      break;
                    case '5':
                      $statusTransacao = "Em disputa";
                      break;
                    case '6':
                      $statusTransacao = "Devolvida";
                      break;
                    case '7':
                      $statusTransacao = "Cancelada";
                      break;
                  }

                  // echo $_response->grossAmount."<br>";
                  $situacao = "".json_decode($_response->status);
                  $valorpago = "".json_decode($_response->grossAmount);
                  $transacao = "".$_response->code;

                  $postData = array(        
                    "cpf" => $CPF,
                    "sindicato" => $syndicate_name,
                    "plano"=> $syndicate_plano,
                    "vencimento" => date('m-Y'),
                    "situacao" => $situacao,
                    "pagamento" => date('Y-m-d'),
                    "valorpago" => $valorpago,
                    "idAssinatura" => $ID_Adesao,
                    "transacao" => $transacao,
                    "IdApp" => $ID_Planos_Adesao
                  );

                  // Setup cURL
                  $ch = curl_init(urlBsf.'/api-v1/empresa/pagamento');
                  curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($postData)
                  ));

                  $response = curl_exec($ch);

                  
                  $http = curl_getinfo($ch);
                  curl_close($ch);  

                  //var_dump(json_encode($postData));

                  $sql = "INSERT INTO Planos_Adesao_Notificacoes (`cpf`,`sindicato`,`plano`,`data`,`idAssinatura`,`notificationCode`,`transationCode`,`status`)
                          VALUES ('".$CPF."','".$syndicate_name."','".$ID_Plano."','".date('Y-m-d')."','".$ID_Adesao."','".$notificationCode."','".$_response->code."','".$_response->status."')";
                  $resultados = $wpdb->get_results($sql);
 
                   // $file = fopen($_SERVER['DOCUMENT_ROOT']."/LogPagSeguro.$today.txt", "ab");

                  //  $path = $_SERVER['DOCUMENT_ROOT'] . '/LogPagSeguro.$today.txt';
                  //  $file = fopen($path, "a+");
                  //  $hour = date("H:i:s T");
                  //  fwrite($file,"Log de Notificações e consulta\\\\r\\\\n");
                  //  fwrite($file,"Hora da consulta: $hour \\\\r\\\\n");
                  //  fwrite($file,"HTTP: ".$http['http_code']." \\\\r\\\\n");
                  //  fwrite($file,"Código de Notificação:".$notificationCode." \\\\r\\\\n");
                  //  fwrite($file, "Código da transação:".$response->code."\\\\r\\\\n");
                  //  fwrite($file, "Status da transação:".$response->status."\\\\r\\\\n");
                  // fwrite($file,"____________________________________ \\\\r\\\\n");
                  //  fclose($file);

                  //  var_dump($response);
                  // echo "<br><br>";
                  //  var_dump($postData);
 




  // $sql = "SELECT * FROM Planos_Adesao";
  // $resultados = $wpdb->get_results($sql);
  // foreach ($resultados as $value) {
  //   $ID_Planos_Adesao = $value->ID_Planos_Adesao;
  //   $ID_Adesao = $value->ID_Adesao;
  //   $CPF = $value->CPF;
  //   $syndicate_name = $value->syndicate_name;
  //   $syndicate_plano = $value->syndicate_plano;
  //   $syndicate_plano_valor = $value->syndicate_plano_valor;


  //   echo $ID_Adesao."<br>";

  //   curl_setopt_array($curl, array(
  //       CURLOPT_URL => URL_WS."/pre-approvals/".$ID_Adesao."?email=".EMAIL_PGS."&token=".TOKEN_PGS,
  //       CURLOPT_RETURNTRANSFER => true,
  //       CURLOPT_ENCODING => "",
  //       CURLOPT_MAXREDIRS => 10,
  //       CURLOPT_TIMEOUT => 30,
  //       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  //       CURLOPT_CUSTOMREQUEST => "GET",
  //       CURLOPT_POSTFIELDS => "",
  //       CURLOPT_HTTPHEADER => array(
  //         'Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1',
  //         'Content-Type: application/json; charset=UTF-8'  
  //       )
  //   ));

  //   $getSession = curl_exec($curl);
  //   $err = curl_error($curl);
  //   $sessionID = "";

  //   var_dump($curl);

  //   curl_close($curl);

  //   if ($err) {
  //       echo "cURL Error #:" . $err;
  //   } else {
  //      $xmlSession = json_decode($getSession, true);
  //      var_dump($xmlSession['paymentOrders']["transactions"]);
  //      //$sessionID = (string)$xmlSession->id;

  //      foreach ($xmlSession['paymentOrders'] as $value) {
  //       echo "valor ".$value["amount"]."<br>";

  //       echo "data evento ".$value["lastEventDate"]."<br>";

  //       echo "agendamento ".$value["schedulingDate"]."<br>";  

  //       foreach ($value["transactions"] as $transactions) {
  //         $data = strtotime($transactions["date"]);
  //         echo "data transacao ".date('d/m/Y',$data)."<br>";  
  //       echo "status transacao ".$transactions["status"]."<br>";  
  //        echo "code transacao ".$transactions["code"]."<br>"; 
  //       }
                       
        
  //      }

    //    echo "<br><br><br>";

    //    foreach ($xmlSession['paymentOrders'] as $value) {
    //     var_dump($value["transactions"]);
    //     return;
    //    }

    //    foreach ($xmlSession['paymentOrders'] as $value) {
    //       foreach ($value as $_value) {
    //          # code...
    //         if(count($_value) > 1){
    //           foreach ($_value as $__value) {
    //             echo $__value;
    //             echo "<br>";
    //           }
    //         }                        
    //         else
    //           echo $_value;
    //         echo "<br>";
    //        }
    //     // var_dump($value);
    //    }
    //}

  //}

}


?>


