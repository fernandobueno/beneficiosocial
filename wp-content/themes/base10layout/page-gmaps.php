<?php ?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Google Maps</title>
    <script type="text/javascript" src="https://mapeamento.bsfonline.com.br/wp-content/themes/base10layout/js/jquery-1.10.2.min.js"></script>
    <script src="https://mapeamento.bsfonline.com.br/wp-content/themes/base10layout/js/vendor/modernizr-2.6.2.min.js"></script>

    <!-- <link href="https://mapeamento.bsfonline.com.br/wp-content/themes/base10layout/css/bootstrap.css" rel="stylesheet" type="text/css" />
    
    <script src="https://mapeamento.bsfonline.com.br/wp-content/themes/base10layout/js/vendor/bootstrap.min.js"></script> -->


    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>


    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <script src="/wp-content/themes/base10layout/js/jquery.mask.min.js"></script>

    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>


    <?php
    //the_post();
    require_once 'pagseguro/config.php';
    
    if(isset($_REQUEST['sigla'])){

        $sigla = $_REQUEST['sigla'];

        $postData = array(
            'sigla'=>$sigla
        );

        // Setup cURL
        $ch = curl_init(urlBsf.'/api-v1/integracao/auth');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));

        // Send the request
        $response = curl_exec($ch);

        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }
        
        $responseData = json_decode($response, TRUE);

        $contador = 0;

        $entidade_sigla = "";
        $entidade_nome = "";
        $entidade_cnpj = "";

        foreach ($responseData as $_responseData) { 
            $token = $_responseData['token'];
            $geolocalicazao = $_responseData['geo'];
            $entidade_sigla = $_responseData['entd']['sigla'];
            $entidade_nome = $_responseData['entd']['razao'];
            $entidade_cnpj = $_responseData['entd']['cnpj'];
        } 

        //var_dump($responseData);


        $urlapi = urlBsf.'/api-v1/integracao/empresas/naolocalizada';

        // Setup cURL
       
        $ch2 = curl_init();

        //$headers    = [];
        //$headers[]  = 'Authorization : '.$token;

        $headr = array();
        $headr[] = 'Content-length: 0';
        $headr[] = 'Content-type: application/json';
        $headr[] = 'Authorization: Bearer '.$token;       

        curl_setopt( $ch2, CURLOPT_URL, $urlapi );
        curl_setopt( $ch2, CURLOPT_HTTPHEADER,$headr);
        curl_setopt( $ch2, CURLOPT_RETURNTRANSFER, true );
        //curl_setopt( $ch2, CURLOPT_HTTPAUTH, CURLAUTH_NTLM );
        //curl_setopt( $ch2, CURLOPT_USERPWD, 'DOMAIN\\'.userERP.':'.passERP.'' );
        curl_setopt( $ch2, CURLOPT_TIMEOUT, 120 );
        //curl_setopt( $ch2, CURLOPT_VERBOSE, true );

        $response2 = curl_exec($ch2);

        //print_r(curl_getinfo($ch2));
        //echo curl_error($ch2);
        curl_close($ch2);

        
        // Send the request
        //$response2 = curl_exec($ch2);

        //var_dump($response2);

        // Check for errors
        if($response2 === FALSE){
            die(curl_error($ch2));
        }
        
        $response2Data = json_decode($response2, TRUE);

        //var_dump($response);

        //$contador = 0;

        $totalEmpresasSemEndereco = count($response2Data['content']);
    }
    else{
        $sigla = "";
        $token = $_REQUEST["token"]; 
    }
    ?>
        <!--******************** INICIO CONTEUDO ********************-->
        <?php  ?>
        <style type="text/css">
            .navbar {
                padding: 15px 10px;
                background: #256CC9;
                border: none;
                border-radius: 0;
                box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
            }

            .navbar-btn {
                box-shadow: none;
                outline: none !important;
                border: none;
            }

            .line {
                width: 100%;
                height: 1px;
                border-bottom: 1px dashed #ddd;
                margin: 40px 0;
            }

            /* ---------------------------------------------------
                SIDEBAR STYLE
            ----------------------------------------------------- */

            #sidebar {
                width: 320px;
                top: 0;
                left: -320px;
                height: 100vh;
                z-index: 999;
                background: #fff;
                color: #256CC9;
                transition: all 0.3s;
                overflow-y: scroll;
                box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.2);
            }

            #sidebar.active {
                left: 0;
            }

            #dismiss {
                width: 35px;
                height: 35px;
                line-height: 35px;
                text-align: center;
                position: absolute;
                top: 10px;
                right: 10px;
                cursor: pointer;
                -webkit-transition: all 0.3s;
                -o-transition: all 0.3s;
                transition: all 0.3s;
                display: none;

            }

            #dismiss:hover {
                color: #7386D5;
            }

            .overlay {
                display: none;
                position: fixed;
                width: 100vw;
                height: 100vh;
                background: rgba(0, 0, 0, 0.7);
                z-index: 998;
                opacity: 0;
                transition: all 0.5s ease-in-out;
            }
            .overlay.active {
                display: block;
                opacity: 1;
            }

            .sidebar-header{display: none;}

            #sidebar .sidebar-header {
                padding: 20px 20px 12px 20px;
                background: #256CC9;
                color: #fff;
            }

            #sidebar ul.components {
                padding: 0px 10px;
            }

            #sidebar ul.components li {
                    margin-bottom: 15px;
            }

            #sidebar ul p {
                color: #256CC9;
            }

            #sidebar ul li a {
                padding: 10px;
                display: block;
                color: #256CC9;
            }

            #sidebar ul li a:hover {
                color: #7386D5;
                background: #fff;
            }

            #sidebar ul li.active>a,
            a[aria-expanded="true"] {
                color: #fff;
                background: #6d7fcc;
            }

            a[data-toggle="collapse"] {
                position: relative;
            }

            .dropdown-toggle::after {
                display: block;
                position: absolute;
                top: 50%;
                right: 20px;
                transform: translateY(-50%);
            }

            ul ul a {
                font-size: 0.9em !important;
                padding-left: 30px !important;
                background: #6d7fcc;
            }

            ul.CTAs {
                padding: 20px;
            }

            ul.CTAs a {
                text-align: center;
                font-size: 0.9em !important;
                display: block;
                border-radius: 5px;
                margin-bottom: 5px;
            }

            a.download {
                background: #fff;
                color: #7386D5;
            }

            a.article,
            a.article:hover {
                background: #6d7fcc !important;
                color: #fff !important;
            }

            /* ---------------------------------------------------
                CONTENT STYLE
            ----------------------------------------------------- */

            #content {
                width: 100%;
                transition: all 0.3s;
                top: 0;
                right: 0;
            }
            .wrapper {
                display: flex;
                align-items: stretch;
            }

            

            .overlay {
                display: none;
                position: fixed;
                /* full screen */
                width: 100vw;
                height: 100vh;
                /* transparent black */
                background: rgba(0, 0, 0, 0.7);
                /* middle layer, i.e. appears below the sidebar */
                z-index: 998;
                opacity: 0;
                /* animate the transition */
                transition: all 0.5s ease-in-out;
            }
            /* display .overlay when it has the .active class */
            .overlay.active {
                display: block;
                opacity: 1;
            }

            #dismiss {
                width: 35px;
                height: 35px;
                position: absolute;
                /* top right corner of the sidebar */
                top: 10px;
                right: 10px;
                color:#fff;
            }
            /* Dropdown Button */
        .dropbtn {
            font-size: 16px;
            border: none;
            cursor: pointer;
            width: 4%;
            float: left;
        }
        
        /* Dropdown button on hover & focus */
        .dropbtn:hover, .dropbtn:focus {
          background-color: #3e8e41;
        }
        
        /* The search field */
        #myInput {
              box-sizing: border-box;
            background-image: url(searchicon.png);
            background-position: 14px 12px;
            background-repeat: no-repeat;
            font-size: 15px;
            padding: 7px 7px 7px 7px;
           
            border: 1px solid;
            /* float: left; */
            width: 100%;
            border-radius: 30px;
        }
        
        /* The search field when it gets focus/clicked on */
        #myInput:focus {}
        
        /* The container <div> - needed to position the dropdown content */
        .dropdown {
              /* position: absolute; */
            /* display: inline-block; */
            z-index: 99999;
            /* float: left; */
            width: 100%;
                position: relative;
                margin-top: 20px;
        }
        
        /* Dropdown Content (Hidden by Default) */
        .dropdown-content {
            display: none;
            background-color: #000000;
            min-width: 230px;
            border: 1px solid #000;
            z-index: 1;
            float: left;
            width: 100%;
            top: 50px;
            height: 195px;
            overflow: auto;
        }
        
        /* Links inside the dropdown */
        .dropdown-content a {
            color: #fff;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
            width: 100%;
            float: left;
            font-size: 14px !important;
            padding: 5px 5px !important;
        }
        
        /* Change color of dropdown links on hover */
        .dropdown-content a:hover {background-color: #f1f1f1; color: #000;}
        
        /* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
        .showdrop {display:block;}
        .hide {display:none;}
        
            .erromsg{
               color: #f30; /* cor do texto */
            }
            .lockdiv{
                pointer-events: none;
                opacity: 0.4;
            }
            .load{
                   background-image: url(../../wp-content/themes/base10layout/img/ajax-loader.gif);
                height: 32px;
                margin-right: auto;
                margin-top: 0;
                margin-left: auto;
                width: 32px;
                position: relative;
                top: 1px;
                margin-bottom: -30px;
                right: -58px;
            }
        
            ul.form-section, ul.form-section li{list-style: none;}
                .imagemlegenda{
                    float: left;
                }
                .form-textbox{width: 90%;}
                .titulolegenda{width: 100%; float: left; margin-bottom: 5px}
        
                .control_pagebreak{
                    margin: 20px 0;
                    text-align: right;
                }
        
                .iconInterno {
                    float: left !important;
                    margin: 0 !important;
                    top: 7px !important;
                    position: relative !important;
                    margin-top: -4px !important;
                    width: 13px;
                }
        
                #legenda span {
                    float: left;
                    margin-top: 10px;
                    text-transform: uppercase;
                    margin-left: 3px;
                }
                #legenda {
                    font-family: Arial, sans-serif;
                    background: #fff;
                    padding: 10px;
                    margin: 10px;
                    border: 3px solid #000;
                    z-index: 0;
                    position: absolute;
                    top: 144px !important;
                    left: 5px !important;
                    background:  #fff;
                    width: 200px;
                    background: #fff;
                    height:270px;
                }
        
                .titulolegenda2 {
                    width: 100%;
                    float: left;
                    margin-top: 6px;
                    text-transform: uppercase;
                    font-weight: bold;
                    font-size: 10px;
                    text-align: center;
                }
        
                #filtro {
                    font-family: Arial, sans-serif;
                    background: #fff;
                    padding: 0px 11px 0px 9px;
                    z-index: 0;
                    /*position: absolute;*/
                    /* top: 10px !important; */
                    /* left: 45% !important; */
                    background: #fff;
                    /* width: 200px; */
                    background: #fff;
                    /* height: 230px; */
                    float: left;
                    width: 100%;
                }
        
                #filtro span {
                    float: left;
                    margin-top: 8px;
                    text-transform: uppercase;
                    margin-left: 3px;
                    width: 100%;
                }
                #filtro img{
                    width: 18px;
                        margin-top: -5px;
                }
        
                #filtro a{
                    color: #000;
                }
        
                #filtro a:hover{text-decoration: none; opacity: 0.7; }
        
                #filtro p {
                    float: left;
                        margin: 0 0 8px;
                }
        
        
                #filtro .removerFiltro {
                    background: #000;
                    color: #fff;
                    padding: 3px 9px;
                    border-radius: 15px;
                    margin: -3px -6px 0px 5px;
                    float: left;
                }
        
              #cadEmpresa {
                    font-family: Arial, sans-serif;
                    padding: -5px;
                    margin: 5px;
                    z-index: 0;
                    position: absolute;
                    top: 100px !important;
                    left: 0px !important;
                    width: 200px;
                    height: 45px;
           
              }
        
              #cadEmpresa a{       
                    width: 220px;
           
              padding-top: 2px;
              }
        
              #cadEmpresa a {
                box-shadow: 3px 4px 0px 0px #899599;
                background: linear-gradient(to bottom, #ededed 5%, #bab1ba 100%);
                background-color: #ededed;
                border-radius: 15px;
                border: 1px solid #d6bcd6;
                display: inline-block;
                cursor: pointer;
                color: #000000;
                font-family: Arial;
                font-size: 15px;
                padding: 7px 10px;
                text-decoration: none;
                text-shadow: 0px 1px 0px #e1e2ed;
                text-align: center;
                font-weight: bold;
            }
            
              #cadEmpresa a:hover {
                background:linear-gradient(to bottom, #bab1ba 5%, #ededed 100%);
                background-color:#bab1ba;
              }
              #cadEmpresa a:active {
                position:relative;
                top:1px;
              }
        
        
              #btnCadastro{
                z-index: 0;
                position: absolute;
                top: 255px !important;
                left: 10px !important;
                width: 196px;
                background: #000;
                height: 31px;
                color: #fff !important;
                text-align: center;
                padding: 13px 0 0 0;
                font-size: 15px;
              }
              #btnCadastro a{color: #fff; text-decoration: none;}
              #btnCadastro:hover{background: red;}
              #legenda h3 {
                margin-top: 0;
                width: 100%;
                float: left;
                text-align: center;
                color: #000;
              }
              #legenda img {
                vertical-align: middle;
              }
                .gmnoprint.gm-bundled-control{
                    left: -3px !important;
                }
        
                .gmnoprint.gm-bundled-control.gm-bundled-control-on-bottom, .gmnoprint.gm-style-mtc, button.gm-fullscreen-control{
                    display: none;
                }
        
        
                body{background: #000;    overflow: hidden;}
                .lds-ripple {
                    display: inline-block;
                    position: relative;
                    width: 64px;
                    height: 64px;
                    top: -4px;
                }
                .lds-ripple div {
                  position: absolute;
                  border: 4px solid #fff;
                  opacity: 1;
                  border-radius: 50%;
                  animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
                }
                .lds-ripple div:nth-child(2) {
                  animation-delay: -0.5s;
                }
                @keyframes lds-ripple {
                  0% {
                    top: 28px;
                    left: 28px;
                    width: 0;
                    height: 0;
                    opacity: 1;
                  }
                  100% {
                    top: -1px;
                    left: -1px;
                    width: 58px;
                    height: 58px;
                    opacity: 0;
                  }
                }

                .loading {
                    display: block;
                    text-transform: uppercase;
                    color: white;
                    position: relative;
                    top: 20px;
                    letter-spacing: 3px;
                    height: 8px;
                }
                .loa_ding{
                    display: block;
                    margin: 0 auto;
                    text-align: center;
                    /* margin-top: -73px; */
                    position: absolute;
                    z-index: 9999999999;
                    width: 100%;
                    background: rgba(0,0,0,0.7);
                    /* margin-left: -177px; */
                    position: fixed;
                    z-index: 9999999;
                    top: 0;
                    left: 0;
                }
        
                .loading_falha{
                    display: none;
                    margin: 0 auto;
                    text-align: center;
                    margin-top: -27px;
                    position: absolute;
                    z-index: 9999999999;
                    width: 100%;
                    background: rgba(0,0,0,0.7);
                    height: 90px;      
                }
        
                .info2{
                    font-size: 12px;
                    margin-top: 3px;
                    /* color: red; */
                    letter-spacing: 2px;
                }
        
                .loading{
                    display: block;
                    text-transform: uppercase;
                    color: white;
                    position: relative;
                    top: 40px;
                        letter-spacing: 3px;
                }
                
        
                    #map{
                        overflow: hidden;
                        padding-bottom: 56.25%;
                        position: relative;
                        height: 100%;
                        float: left;
                        width: 100%;
                    }
                    #map iframe{
                        left:0;
                        top:0;
                        height:100%;
                        width:100%;
                        position:absolute;
                    }
        
                    #infoPanel {
                        float: left;
                        margin-left: 10px;
                    }
                    #infoPanel div {
                        margin-bottom: 5px;
                    }
                    #block {
                        margin-bottom:10px;
                        background: #cccccc;
                        padding:4px;
                    }
        
                    .gm-style-iw {
                            width: 350px !important;
                        top: 2px !important;
                        left: 40px !important;
                        background-color: #fff;
                        box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
                        border: 1px solid rgba(72, 181, 233, 0.6);
                        border-radius: 2px 2px 10px 10px;
                    }
                    #iw-container {
                        margin-bottom: 10px;
                    }
                    #iw-container .iw-title {                
                        /* font-family: 'Open Sans Condensed', sans-serif; */
                        font-size: 17px;
                        font-weight: bold;
                        padding: 10px;
                        background-color: #48b5e9;
                        color: white;
                        margin: 0;
                        border-radius: 2px 2px 0 0;
                        width: 290px;
                        text-align: center;
                    }
                    #iw-container .iw-content {
                        font-size: 13px;
                        line-height: 12px;
                        font-weight: 400;
                        margin-right: 1px;
                        padding: 5px 5px 5px 5px;
                        max-height: 140px;
                        overflow-y: auto;
                        overflow-x: hidden;
                    }
        
                    #iw-container .iw-content p{
                        padding: 0;
                        margin: 3px 0;
                    }
        
                    .iw-content img {
                        float: right;
                        margin: 0 5px 5px 10px; 
                    }
                    .iw-subTitle {
                        font-size: 13px;
                      font-weight: normal;
                      line-height: 15px;
                      float: left;
                      width: 89%;
                      margin-bottom: 13px;
                    }
        
                    .badge {
                        display: inline-block;
                        min-width: 10px;
                        padding: 4px 7px !important;
                        font-size: 12px;
                        font-weight: bold;
                        line-height: 1;
                        color: #fff;
                        text-align: center;
                        white-space: nowrap;
                        vertical-align: baseline;
                        background-color: #517fff !important;
                        border-radius: 10px;
                        float: left;
                             margin-bottom: 5px;
                        margin-top: 5px;
                    }
        
                    .modal-content .iw-subTitle {
                           font-size: 15px;
                        font-weight: 700;
                        line-height: unset;
                        float: left;
                        width: 95%;
                        margin-bottom: 0;
                    }
        
                    .iw-bottom-gradient {
                        position: absolute;
                        width: 268px;
                        height: 25px;
                        bottom: 10px;
                        right: 18px;
                        background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
                        background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
                        background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
                        background: -ms-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
                    }
        
                    .outrasinfos{display: none;}
                    #mapLegend img.logosocial {
                        max-width: none;
                        background: rgba(0,0,0,0.3);
                        padding: 15px;
                    }
        
        
                    .myButton {
                      box-shadow: 3px 4px 0px 0px #899599;
                      background:linear-gradient(to bottom, #ededed 5%, #bab1ba 100%);
                      background-color:#ededed;
                      border-radius:15px;
                      border:1px solid #d6bcd6;
                      display:inline-block;
                      cursor:pointer;
                      color:#000;
                      font-family:Arial;
                      font-size:17px;
                      padding:7px 25px;
                      text-decoration:none;
                      text-shadow:0px 1px 0px #e1e2ed;
                    }
                    .myButton:hover {
                      background:linear-gradient(to bottom, #bab1ba 5%, #ededed 100%);
                      background-color:#bab1ba;
                    }
                    .myButton:active {
                      position:relative;
                      top:1px;
                    }
        
                    .modal-header .close {
                      margin-top: 0px;
                      position: relative;
                      z-index: 9999999999;
                    }
        
                    .divInfoFunTotalBtn{
                      float: left;
                      margin-left: 10px;
                    }
        
                    .divInfoFunTotal{
                      float: left;
                    }
        
                    .divInfo{
                      float: left;
                      width: 100%;
                    }
        
                    .modal-content {float: left;     width: 100%;}
        
                    .modal-content td, .modal-content th {
                        padding: 0;
                        font-size: 13px;
                        padding: 2px 8px;
                    }
        
                    .modal-header {
                        min-height: 16.42857143px;
                        padding: 10px;
                        border-bottom: 1px solid #e5e5e5;
                        padding-bottom: 6px;
                        margin-bottom: 7px;
                        float: left;
                        width: 100%;
                    }
        
                    p {
                        margin: 0;
                    }
        
                    .detInfos h3 {
                          font-size: 14px;
                          font-weight: bold;
                          text-transform: uppercase;
                    }

                    .legendafooter{
                            width: 100%;
                        float: left;
                        position: fixed;
                        bottom: 0;
                        background: #404040;
                        color: #fff;
                        padding-top: 8px;
                        z-index: 999;
                        }
                        #legendainfosmobile{
                                width: 100%;
                            float: left;
                            text-align: center;
                        }

                        #legendainfosmobile div{
                            /* float: left; */
                            display: inline-block;
                            margin: 0px 3px;
                        }

                        .icones{
                            text-align: center;
                            font-size: 10px;
                            text-transform: uppercase;
                        }

                        .icones img {
                            width: 17px;
                            margin-top: -3px;
                        }

                        .btnMenuMobile{
                            display: none;
                        background: none;
                        border: none;
                        color: #fff;
                        width: 25px;
                    }

                    .btnMenuMobile .fa-align-justify{
                        font-size: 21px;
                    }
        
                    @media only screen and (max-width: 960px) {
                         .btnMenuMobile{   display: block; }

                        .sidebar-header{display: block;}
                         #sidebar {
                                width: 100%;
                                    left: -100%;
                                    position: fixed !important;
                            }

                            .wrapper {
                                display: block !important;
                                    align-items: normal;
                            }

                            #dismiss{
                                display: block;
                            }

                      
                      #legenda {
                          display: none;
                      }
        
                      #cadEmpresa {
                          font-family: Arial, sans-serif;
                          padding: -5px;
                          margin: 5px;
                          z-index: 0;
                          position: absolute;
                          top: unset !important;
                          left: 49% !important;
                          width: 200px;
                          height: 45px;
                          bottom: 41px !important;
                          margin-left: -100px;
                      }
        
                      

                      #sidebar.active {
                            width: 100%;
                        }


                    }
        
                    .modal {z-index: 999999 !important;}

                    .modal ul{
                        float: left;
                        width: 100%;
                        margin: 0;
                        padding: 0;
                    }

                    .modal-title{
                            width: 100%;
                        float: left;
                    }

                    .modal-header .close {
                        padding: 0;
                        margin: 0;
                    }

                    span.logo{
                        margin: 0 auto;
                    }

                    

                    a.btnCadEmpresa {
                        background: #256CC9;
                        color: #fff !important;
                        border-radius: 30px;
                        text-align: center;
                    }

                    a.btnCadEmpresa:hover {
                        background: #256CC9 !important;
                        color: #fff !important;
                        border-radius: 30px;
                        text-align: center;
                        opacity: 0.7;
                    }
        </style>
        <script src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry&key=AIzaSyBCnPBV5cJPNNJsstg9BUqZOhTEOa7O3PE" type="text/javascript"></script>
        <script type="text/javascript">
            //<![CDATA[
    
            var detailsInfos= [];
            var lookup = [];
            var markers = [];
            var auxCnpjs = [];
            var map;
    
            function isLocationFree(search) {
              for (var i = 0, l = lookup.length; i < l; i++) {
                if (lookup[i][0] === search[0] && lookup[i][1] === search[1]) {
                  return false;
                }
              }
              return true;
            }

            $(window).resize(function () {
                var width = $(window).width();
                if (width <= 980) {
                    $('body').addClass('mobile');
                }
            });
            
            $(document).ready(function() {

                var width = $(window).width();
                if (width <= 980) {
                    $('body').addClass('mobile');
                }
            
    
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = window.location.search.substring(1),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;
    
                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
    
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                        }
                    }
                };
                
                localStorage.clear();
                sessionStorage.clear();
                sessionStorage.setItem('dados', "");
    
                 
                var markersData = new Object();     
                var zoomLevel = 13;
                var zoomAcumulado = 13;
                
                var infoWindow;
                var icone = "";
                var iconBase = '/wp-content/themes/base10layout/img/';
                var icons = {
                  adimplente: {
                    name: 'Adimplente',
                    icon: iconBase + 'adimplente.png'
                  },
                  inadimplente: {
                    name: 'Inadimplente',
                    icon: iconBase + 'inadimplente.png'
                  },
                  emcobranca:{
                    name: 'Em Cobrança',
                    icon: iconBase + 'emcobranca.png'
                  },
                  mesmoendereco: {
                    name: 'Múltiplas Empresas',
                    icon: iconBase + 'mesmoendereco.png'
                  }
                };
    
                var urlApi = '<?php echo urlBsf; ?>/api-v1/integracao/empresas?lat=-23.541400&long=-46.629671&raio=4';
                var latitude = "<?php echo $geolocalicazao['lat']; ?>";
                var longitude = "<?php echo $geolocalicazao['long']; ?>";
                var raioemkm = 4;
                
                //function load() {
                    $(".loa_ding").show();                        
    
                    $("#map").addClass('lockdiv');
                    
            
                    //Configurações do mapa
                    map = new google.maps.Map(document.getElementById("map"), {
                        center: new google.maps.LatLng(<?php echo $geolocalicazao['lat']; ?>, <?php echo $geolocalicazao['long']; ?>),
                        zoom: 13,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.TOP_CENTER
                        },
                        mapTypeControl: true,
                        setClickableIcons: false,
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                            mapTypeIds: ['roadmap', 'terrain']
                        },
                        styles: [
                              {
                                elementType: "geometry",
                                stylers: [
                                  {
                                    color: "#f5f5f5"
                                  }
                                ]
                              },
                              {
                                elementType: "labels.icon",
                                stylers: [
                                  {
                                    visibility: "off"
                                  }
                                ]
                              },
                              {
                                elementType: "labels.text.fill",
                                stylers: [
                                  {
                                    color: "#616161"
                                  }
                                ]
                              },
                              {
                                elementType: "labels.text.stroke",
                                stylers: [
                                  {
                                    color: "#f5f5f5"
                                  }
                                ]
                              },
                              {
                                featureType: "administrative.land_parcel",
                                elementType: "labels.text.fill",
                                stylers: [
                                  {
                                    color: "#bdbdbd"
                                  }
                                ]
                              },
                              {
                                featureType: "poi",
                                stylers: [
                                  {
                                    visibility: "off"
                                  }
                                ]
                              },
                              {
                                featureType: "poi",
                                elementType: "geometry",
                                stylers: [
                                  {
                                    color: "#eeeeee"
                                  }
                                ]
                              },
                              {
                                featureType: "poi",
                                elementType: "labels.text.fill",
                                stylers: [
                                  {
                                    color: "#757575"
                                  }
                                ]
                              },
                              {
                                featureType: "poi.park",
                                elementType: "geometry",
                                stylers: [
                                  {
                                    color: "#e5e5e5"
                                  }
                                ]
                              },
                              {
                                featureType: "poi.park",
                                elementType: "labels.text.fill",
                                stylers: [
                                  {
                                    color: "#9e9e9e"
                                  }
                                ]
                              },
                              {
                                featureType: "road",
                                elementType: "geometry",
                                stylers: [
                                  {
                                    color: "#ffffff"
                                  }
                                ]
                              },
                              {
                                featureType: "road.arterial",
                                elementType: "labels.text.fill",
                                stylers: [
                                  {
                                    color: "#757575"
                                  }
                                ]
                              },
                              {
                                featureType: "road.highway",
                                elementType: "geometry",
                                stylers: [
                                  {
                                    color: "#dadada"
                                  }
                                ]
                              },
                              {
                                featureType: "road.highway",
                                elementType: "labels.text.fill",
                                stylers: [
                                  {
                                    color: "#616161"
                                  }
                                ]
                              },
                              {
                                featureType: "road.local",
                                elementType: "labels.text.fill",
                                stylers: [
                                  {
                                    color: "#9e9e9e"
                                  }
                                ]
                              },
                              {
                                featureType: "transit",
                                stylers: [
                                  {
                                    visibility: "off"
                                  }
                                ]
                              },
                              {
                                featureType: "transit.line",
                                elementType: "geometry",
                                stylers: [
                                  {
                                    color: "#e5e5e5"
                                  }
                                ]
                              },
                              {
                                featureType: "transit.station",
                                elementType: "geometry",
                                stylers: [
                                  {
                                    color: "#eeeeee"
                                  }
                                ]
                              },
                              {
                                featureType: "water",
                                elementType: "geometry",
                                stylers: [
                                  {
                                    color: "#c9c9c9"
                                  }
                                ]
                              },
                              {
                                featureType: "water",
                                elementType: "labels.text.fill",
                                stylers: [
                                  {
                                    color: "#9e9e9e"
                                  }
                                ]
                              }
                            ]
    
                    });
    
                    var radius = raioemkm * 1400;
                    var circulo = new google.maps.Circle(
                    {
                        map: map,
                        center: new google.maps.LatLng(latitude, longitude),
                        radius: radius, // 1000 metros = 1k.
                        strokeColor: "black",
                        fillColor: "black",
                        fillOpacity: 0.1,
                    });
    
                    function zoomChanged(){
                        zoomLevel = map.getZoom();
                        if (zoomLevel < zoomAcumulado) {
                            if(raioemkm > 18) {  
                                if(raioemkm > 80)                        
                                    raioemkm = (raioemkm * 2.8);
                                else
                                    raioemkm = (raioemkm * 1.5); 
                            }                    
                            else
                                raioemkm = (zoomAcumulado * 1.5);
                            zoomAcumulado = zoomLevel;
                            urlApi = '<?php echo urlBsf; ?>/api-v1/integracao/empresas?lat='+latitude+'&long='+longitude+'&raio='+raioemkm;
                            manipulaMapa();
                        } 
                    }
    
                    function dragEnd(){
                        currentMapCenter = map.getCenter();
    
                        var centerLat = currentMapCenter.lat();
                        var centerLong = currentMapCenter.lng();
    
                        var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(latitude, longitude), new google.maps.LatLng(centerLat, centerLong));
    
                        //console.log(distance);
    
                        if(distance > raioemkm){
                            //requestGeoLocation.abort();
                            //requestMarkers.abort();
                            latitude = centerLat;
                            longitude = centerLong;
                            urlApi = '<?php echo urlBsf; ?>/api-v1/integracao/empresas?lat='+centerLat+'&long='+centerLong+'&raio='+raioemkm;
                            map.setCenter(new google.maps.LatLng(latitude, longitude));
                            manipulaMapa();
                        }          
                    }
    
                    var myTimeout;
    
                    map.addListener('zoom_changed', function() {
                        if (myTimeout) {
                            window.clearTimeout(myTimeout);
                        }
    
                        myTimeout = window.setTimeout(zoomChanged, 1000);
                        
                    });
    
                    map.addListener('dragend', function () {
    
                        if (myTimeout) {
                            window.clearTimeout(myTimeout);
                        }
    
                        myTimeout = window.setTimeout(dragEnd, 2000);
    
                                  
                    });
    
                   
    
                    // var legenda = document.getElementById('legenda');
                    // var titulo = document.createElement('div');
                    // titulo.innerHTML = '<div class="tituloLegenda"><h3>LEGENDA</h3></div>';
                    // legenda.appendChild(titulo);
                    // for (var key in icons) {
                    //   var type = icons[key];
                    //   var name = type.name;
                    //   var icon = type.icon;
                    //   var divLegenda = document.createElement('div');
                    //   divLegenda.innerHTML = '<div class="titulolegenda"><div class="imagemlegenda"><img src="' + icon + '"></div><span>' + name +'</span></div>';
                    //   legenda.appendChild(divLegenda);
                    // }
    
                    // var div_Legenda = document.createElement('div');
                    // div_Legenda.innerHTML = '<div id="legendainfos"></div>';
                    // legenda.appendChild(div_Legenda);
    
                    // map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legenda);
    
                    var cadEmpresa = document.getElementById('cadEmpresa');
    
                    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(cadEmpresa);
    
                    // var filtro = document.getElementById('filtro');
    
                    // map.controls[google.maps.ControlPosition.TOP_CENTER].push(filtro);
    
                    /*
    
                    
    
                    var btnCadastro = document.getElementById('btnCadastro');
                    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(btnCadastro);
    
                    */
                
                    //Janela de informações
                    infoWindow = new google.maps.InfoWindow({maxWidth: 350 });
    
                    function setHeader(xhr) {
                        xhr.setRequestHeader('authorization', '<?php echo 'Bearer '.$token; ?>');
                    }
    
                    var requestGeoLocation;
    
                    function getGeoLocation(){
    
                        
                        latitude = "<?php echo $geolocalicazao['lat']; ?>";
                        longitude = "<?php echo $geolocalicazao['long']; ?>";
                        urlApi = 'https://cobranca.bsfonline.com.br/api-v1/integracao/empresas?lat='+latitude+'&long='+longitude+'&raio='+raioemkm;
                        map.setCenter(new google.maps.LatLng(latitude, longitude)); 
                        manipulaMapa();    
                        /*
                        requestGeoLocation = $.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBCnPBV5cJPNNJsstg9BUqZOhTEOa7O3PE", function(success) {
                            latitude = success.location.lat;
                            longitude = success.location.lng;
                            urlApi = urlBsf.'/api-v1/integracao/empresas?lat='+latitude+'&long='+longitude+'&raio='+raioemkm;
                            map.setCenter(new google.maps.LatLng(latitude, longitude)); 
                            manipulaMapa();                      
                        })
                        .fail(function(err) {
                            console.log("API Geolocation error! \n\n"+err);
                            latitude = -23.541400;
                            longitude = -46.629671;
                            urlApi = urlBsf.'/api-v1/integracao/empresas?lat='+latitude+'&long='+longitude+'&raio='+raioemkm;
                            
                        });
                        */
                    }
    
                    var requestMarkers;
    
                    function manipulaMapa(){
    
                        circulo.setMap(null);
    
                        $(".loa_ding").show();
                        $("#map").addClass('lockdiv');
                        $(".loading_falha").hide();          
    
                        //console.log(urlApi);
    
                        var contador = 0;
                        requestMarkers = $.ajax({
                          url: urlApi,
                          type: 'GET',
                          dataType: 'json',
                          success: function(data) {
                            if(data.content.length == 0) {
                                $(".loa_ding").hide();
                                $("#map").removeClass('lockdiv');
                                $(".loading_falha").show();
                            }
                            else{                           
    
                                //console.log(data);
    
                                var totalEmpresas = 0;
                                var totalEmpresasSemEndereco = <?php echo $totalEmpresasSemEndereco; ?>;
                                
                                // $.each(data, function(idx, obj) {
                                     var dataJson = {};
    
                                //     console.log(obj);
                                    
                                //     if(obj.length > 0){
                                        dataJson = data.content;
                                //         //console.log(dataJson);                                    
                                //     }
    
    
                                    for ( i=0; i < dataJson.length; i++) {
                                        
    
                                        var span = document.createElement('span');
                                        span.innerHTML = '<a onclick="clickbusca(this.id)" id="'+dataJson[i].razao+'|'+dataJson[i].geo.lat+'|'+dataJson[i].geo.long+'|'+dataJson[i].cnpj+'" href="#'+dataJson[i].razao+'">'+dataJson[i].razao+'</a>';
                                 
                                        myDropdown.appendChild(span);                                                
                                        
                                        
                                    }
                                    
                                    $.each(data.content, function(idx2, obj2) {
                                        //console.log(obj2.razao);
    
                                      var search = [obj2.geo.lat, obj2.geo.long];
    
                                      
    
                                      
    
                                        var validaEndereco = false;
                                        try {
                                            if(obj2.geo.lat)
                                                validaEndereco = true;
                                        } catch (e) {
                                            validaEndereco = false;
                                            console.log(e);
    
                                        }
                                        
                                        if (validaEndereco){   
    
                                          if(isLocationFree(search)) {
    
                                          lookup.push([obj2.geo.lat, obj2.geo.long]);
    
                                            var situacao_sindicato = obj2.cobranca;                                       
    
                                            icone = "";
    
                                            switch(situacao_sindicato){
                                                case 'A':
                                                    icone = "adimplente";
                                                break;
    
                                                case 'C':
                                                    icone = "emcobranca";
                                                break;
    
                                                case 'I':
                                                    icone = "inadimplente";
                                                break;
                                            }
    
    
    
    
                                            var filtro = getUrlParameter('filtro');
    
                                            if(!filtro || filtro == icone || filtro == ""){
                                               
    
    
                                                var arrayEMPRESAS = {};
                                                var arrayNomes = {};
                                                var contEmpresas = 0;
    
                                                var cnpjLocal = "";
                                                
                                                var name = obj2.razao;
                                                var address = obj2.endereco.logradouro+', '+obj2.endereco.numero;
                                                var type = "";
                                                var regiao = obj2.endereco.bairro+' - '+obj2.endereco.cidade;
                                                var trabalhadores = obj2.qtdeTrabalhadores;
    
                                                var telefone = obj2.telefone;
                                                var email = obj2.email;
                                                //console.log(email);
    
                                                if(email){
                                                    email = email.replace(",", "<br>");
                                                    email = email.replace(",", "<br>");
                                                    email = email.toLowerCase();
                                                }
    
                                                var latitude = obj2.geo.lat;                   
                                                var longitude = obj2.geo.long;
    
                                                //console.log('Latitude: '+latitude);
    
                                                //var address = document.getElementById('address').value;
                                                // geocoder.geocode( { 'address': address}, function(results, status) {
                                                //   console.log(status);
                                                //   if (status == 'OK') {
                                                //    console.log(results[0].geometry.location);                                            
                                                //   }
                                                // });
    
                                                var html = '';
    
                                                var i=0, k=0;
                                                var controle = false;
                                                //if ((dataJson[i].geo.lat == latitude) && (dataJson[i].geo.long == longitude) && (!$.inArray(dataJson[i].razao,arrayNomes)))
                                                  //  {
    
                                                for ( i=0; i < dataJson.length; i++) {
                                                    if ((dataJson[i].geo.lat == latitude) && (dataJson[i].geo.long == longitude)){           
                                                        controle = true;  
                                                        arrayEMPRESAS[dataJson[i].cnpj] = dataJson[i];
                                                        arrayNomes[contEmpresas] = name;
                                                        contEmpresas++;
                                                        
                                                    }                                                
                                                    
                                                    
                                                }
    
                                                $.each(arrayEMPRESAS, function(idx3, obj3) {
                                                  k++;
                                                });

                                                var auxCont = 0;
                                                    auxCnpjs = [];
    
    
    
                                                if(k == 1){
    
                                                    cnpjLocal = obj2.cnpj;
                                                    detailsInfos[obj2.cnpj] = [];
                                                    detailsInfos[obj2.cnpj]["name"] = name;
                                                    detailsInfos[obj2.cnpj]["cnpj"] = obj2.cnpj;
                                                    detailsInfos[obj2.cnpj]["razao"] = obj2.razao;
                                                    detailsInfos[obj2.cnpj]["qtdeTrabalhadores"] = obj2.qtdeTrabalhadores;
                                                    detailsInfos[obj2.cnpj]["adimplente"] = obj2.adimplente;
                                                    detailsInfos[obj2.cnpj]["logradouro"] = obj2.endereco.logradouro;
                                                    detailsInfos[obj2.cnpj]["numero"] = obj2.endereco.numero;
                                                    detailsInfos[obj2.cnpj]["comp"] = obj2.endereco.comp;
                                                    detailsInfos[obj2.cnpj]["bairro"] = obj2.endereco.bairro;
                                                    detailsInfos[obj2.cnpj]["cidade"] = obj2.endereco.cidade;
                                                    detailsInfos[obj2.cnpj]["uf"] = obj2.endereco.uf;
                                                    detailsInfos[obj2.cnpj]["telefone"] = obj2.telefone;
                                                    detailsInfos[obj2.cnpj]["cobranca"] = obj2.cobranca;
                                                    detailsInfos[obj2.cnpj]["email"] = obj2.email;
                                                    detailsInfos[obj2.cnpj]["sindicato"] = obj2.sindicato;
                                                    detailsInfos[obj2.cnpj]["regiao"] = regiao;
                                                    
                                                    html = '<div id="iw-container">' +
                                                                    '<div class="iw-content">' +
                                                                      '<div class="iw-subTitle">'+name+'</div>' +
                                                                      '<a class="öpenDetail badge badge-pill badge-primary" data-toggle="modal" data-target="#modalDetail" id="'+obj2.cnpj+'">+</a>'
                                                                    '</div>' +
                                                                    '<div class="iw-bottom-gradient"></div>' +
                                                                  '</div>';
                                                }
                                                else{
                                                    
                                                                                        
    
                                                    html = html+'<div id="iw-container">' +
                                                                    '<div class="iw-content">'; 
    
                                                    
                                                    $.each(arrayEMPRESAS, function(idx3, obj3) {
    
                                                        var latitude_obj3 = obj3.geo.lat;                   
                                                        var longitude_obj3 = obj3.geo.long;
                                                        var name_obj3 = obj3.razao;
    
    
                                                        //console.log(idx3+' '+obj3.razao);
    
                                                        //if ((latitude_obj3 == latitude) && (longitude_obj3 == longitude)){
                                                            //var name_obj3 = obj3.razao;
                                                            //if (html.toLowerCase().indexOf(name_obj3.toLowerCase()) == -1){
    
                                                                icone = "mesmoendereco";
    
                                                                var address_obj3 = obj3.endereco.logradouro+', '+obj3.endereco.numero;
                                                                var type_obj3 = "";
                                                                var regiao_obj3 = obj3.endereco.bairro+' - '+obj3.endereco.cidade;
                                                                var trabalhadores_obj3 = obj3.qtdeTrabalhadores;
    
                                                                var telefone_obj3 = obj3.telefone;
                                                                var email_obj3 = obj3.email;
    
                                                                var situacao_sindicato = obj2.cobranca;
    
                                                                var iconeCobranca = iconBase;
    
                                                                switch(situacao_sindicato){
                                                                    case 'A':
                                                                        iconeCobranca = iconeCobranca + "adimplente.png";
                                                                    break;
    
                                                                    case 'C':
                                                                        iconeCobranca = iconeCobranca + "emcobranca.png";
                                                                    break;
    
                                                                    case 'I':
                                                                        iconeCobranca = iconeCobranca + "inadimplente.png";
                                                                    break;
                                                                }
    
    
                                                                if(email_obj3){
                                                                    email_obj3 = email_obj3.replace(",", "<br>");
                                                                    email_obj3 = email_obj3.replace(",", "<br>");
                                                                    email_obj3 = email_obj3.toLowerCase();
                                                                }
    
                                                                cnpjLocal = obj3.cnpj;

                                                                auxCnpjs[auxCont] = cnpjLocal;
                                                                auxCont++;
    
                                                                detailsInfos[obj3.cnpj] = [];
                                                                detailsInfos[obj3.cnpj]["name"] = name_obj3;
                                                                detailsInfos[obj3.cnpj]["cnpj"] = obj3.cnpj;
                                                                detailsInfos[obj3.cnpj]["razao"] = obj3.razao;
                                                                detailsInfos[obj3.cnpj]["qtdeTrabalhadores"] = obj3.qtdeTrabalhadores;
                                                                detailsInfos[obj3.cnpj]["adimplente"] = obj3.adimplente;
                                                                detailsInfos[obj3.cnpj]["logradouro"] = obj3.endereco.logradouro;
                                                                detailsInfos[obj3.cnpj]["numero"] = obj3.endereco.numero;
                                                                detailsInfos[obj3.cnpj]["comp"] = obj3.endereco.comp;
                                                                detailsInfos[obj3.cnpj]["bairro"] = obj3.endereco.bairro;
                                                                detailsInfos[obj3.cnpj]["cidade"] = obj3.endereco.cidade;
                                                                detailsInfos[obj3.cnpj]["uf"] = obj3.endereco.uf;
                                                                detailsInfos[obj3.cnpj]["telefone"] = obj3.telefone;
                                                                detailsInfos[obj3.cnpj]["cobranca"] = obj3.cobranca;
                                                                detailsInfos[obj3.cnpj]["email"] = email_obj3;
                                                                detailsInfos[obj3.cnpj]["sindicato"] = obj3.sindicato;
                                                                detailsInfos[obj3.cnpj]["regiao"] = regiao_obj3;
    
                                                                html = html+'<div class="iw-subTitle"><img class="iconInterno" src="'+iconeCobranca+'" alt="Situação da Cobrança" /> '+name_obj3+'</div>'+
                                                                 '<a class="öpenDetail badge badge-pill badge-primary" data-toggle="modal" data-target="#modalDetail" id="'+obj2.cnpj+'">+</a>';
                                                            //}
                                                        //}
    
                                                    }); 
    
                                                    html = html+'</div>' +
                                                                '<div class="iw-bottom-gradient"></div>' +
                                                                '</div>'; 
    
                                                }        
    
                                                if(latitude != 0){
                                                    totalEmpresas++;
    
                                                    //if(!detailsInfos[cnpjLocal]) {
    
                                                      var point = new google.maps.LatLng(
                                                          parseFloat(latitude),
                                                          parseFloat(longitude)
                                                      );  
    
                                                      //console.log(icons[icone]);                                                                         
    
                                                      var marker = new google.maps.Marker({
                                                          map: map,
                                                          position: point,
                                                          icon: icons[icone].icon,
                                                          title: name
                                                      });
    
                                                      bindInfoWindow(marker, map, infoWindow, html);
    
                                                        // markers[latitude+longitude] = [];                
                                                        // markers[latitude+longitude]['infowindow'] = infoWindow;
                                                        // markers[latitude+longitude]['marker'] = marker;
                                                        // markers[latitude+longitude]['lat'] = latitude;
                                                        // markers[latitude+longitude]['long'] = longitude;

                                                        detailsInfos[cnpjLocal]["marker"] = marker;

                                                        for (i=0; i < auxCnpjs.length; i++) {
                                                            detailsInfos[auxCnpjs[i]]["marker"] = marker;
                                                        }
    
                                                    }


    
                    
                                                    // marker.addListener('click', function() {
                                                    //     $.post("<?php echo home_url('mapas');?>",
                                                    //     {  
                                                    //         geraRecolhimento: "geraRecolhimento",
                                                    //         cnpj: obj2.cnpj,
                                                    //         token: '<?php echo $token; ?>'
                                                    //     },
                                                    //         function(dataRecolhimento){
                                                    //             clicaMapa(dataRecolhimento, obj2.cnpj); 
                                                                
                                                    //         }
                                                    //     );
    
                                                    //     if(k > 1){
                                                    //         var obj3cnpj = "";
                                                    //         $.each(arrayEMPRESAS, function(idx3, obj3) {
    
                                                    //             var latitude_obj3 = obj3.geo.lat;                   
                                                    //             var longitude_obj3 = obj3.geo.long;
                                                    //             var name_obj3 = obj3.razao;
    
                                                    //             if ((obj3cnpj != obj3.cnpj) && (obj2.cnpj != obj3.cnpj) && (latitude_obj3 == latitude) && (longitude_obj3 == longitude)){
                                                    //                 //var name_obj3 = obj3.razao;
                                                    //                 if (html.toLowerCase().indexOf(name_obj3.toLowerCase()) > 0){
                                                    //                     obj3cnpj = obj3.cnpj;
                                                    //                     $.post("<?php echo home_url('mapas');?>",
                                                    //                     {  
                                                    //                         geraRecolhimento: "geraRecolhimento",
                                                    //                         cnpj: obj3.cnpj,
                                                    //                         token: '<?php echo $token; ?>'
                                                    //                     },
                                                    //                         function(dataRecolhimento){
                                                    //                             clicaMapa(dataRecolhimento, obj3.cnpj); 
                                                    //                         }
                                                    //                     )
                                                    //                 }
                                                    //             }
    
                                                    //         }); 
                                                    //     }
                                                     
                                                    // });
    
                                                    contador++;
    
                                                    //console.log(contador);                                            
    
                                                    if(contador == data.content.length){
                                                        $(".loa_ding").hide();
                                                        $("#map").removeClass('lockdiv');
    
                                                        //var legenda = document.getElementById('legendainfos');
                                                        //legenda.innerHTML = "";
                                                        var titulo = document.createElement('div');
                                                        
                                                        var raioMapa = document.createElement('div');
                                                        raioMapa.innerHTML = '<div class="titulolegenda2">Raio de ' + raioemkm +' Km</div>';
                                                        //legenda.appendChild(raioMapa);
    
                                                        var divLegenda = document.createElement('div');
                                                        divLegenda.innerHTML = '<div class="titulolegenda2">Empresas no mapa: ' + totalEmpresas +'</div>';
                                                        //legenda.appendChild(divLegenda);
    
                                                        // if(totalEmpresasSemEndereco > 0){
                                                        var divLegenda0 = document.createElement('div');
                                                        divLegenda0.innerHTML = '<div class="titulolegenda2">Endereços inválidos: ' + totalEmpresasSemEndereco +'</div>';
                                                        //legenda.appendChild(divLegenda0);
                                                        //}

                                                        var legendamobile = document.getElementById('legendainfosmobile');
                                                        legendamobile.innerHTML = "";
                                                        legendamobile.appendChild(raioMapa);
                                                        legendamobile.appendChild(divLegenda);
                                                        legendamobile.appendChild(divLegenda0);
                                                        
    
                                                        // map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legenda);
                                                    }
                                                }
                                            //}
                                          }
                                        }
                                        else{
                                            // totalEmpresasSemEndereco++;
                                        }
                                    });                                
                                //});
                                
                                $(".loa_ding").hide();
                                $("#map").removeClass('lockdiv');
    
                                // console.log(totalEmpresas);
                                // console.log(totalEmpresasSemEndereco);
    
                                //var legenda = document.getElementById('legendainfos');
                                //legenda.innerHTML = "";
                                var titulo = document.createElement('div');
                                
                                var raioMapa = document.createElement('div');
                                raioMapa.innerHTML = '<div class="titulolegenda2">Raio de ' + raioemkm +' Km</div>';
                                //legenda.appendChild(raioMapa);
    
                                var divLegenda = document.createElement('div');
                                divLegenda.innerHTML = '<div class="titulolegenda2">Empresas no mapa: ' + totalEmpresas +'</div>';
                                //legenda.appendChild(divLegenda);
    
                                // if(totalEmpresasSemEndereco > 0){
                                var divLegenda0 = document.createElement('div');
                                divLegenda0.innerHTML = '<div class="titulolegenda2">Endereços inválidos: ' + totalEmpresasSemEndereco +'</div>';
                                //legenda.appendChild(divLegenda0);
                                //}

                                var legendamobile = document.getElementById('legendainfosmobile');
                                legendamobile.innerHTML = "";
                                legendamobile.appendChild(raioMapa);
                                legendamobile.appendChild(divLegenda);
                                legendamobile.appendChild(divLegenda0);
    
                                var radius2 = raioemkm * 1400;
                                circulo = new google.maps.Circle(
                                {
                                    map: map,
                                    center: new google.maps.LatLng(latitude, longitude),
                                    radius: radius2, // 1000 metros = 1k.
                                    strokeColor: "black",
                                    fillColor: "black",
                                    fillOpacity: 0.1,
                                });
                            }
                            
                          },
                          error: function() {
                            console.log('Erro');
                          },
                          beforeSend: setHeader
                        });
    
                    }
    
                    <?php
                        if($token){
                    ?>    
                        getGeoLocation();
                    <?php
                        }
                    ?>
    
                    var iframe = document.getElementsByTagName("IFRAME");
                    var i;
                    for (i = 0; i < iframe.length; i++) {
                        iframe[i].style.height = iframe[i].contentWindow.document.body.scrollHeight + 'px';
                    }
                
    
                //}
         
                function resizeIframe(obj) {
                    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
                  }
    
                //Inclui a janela de informações ao marcador
                function bindInfoWindow(marker, map, infoWindow, html) {
                       
    
                    google.maps.event.addListener(marker, 'click', function() {
                        infoWindow.setContent(html);
                        infoWindow.open(map, marker);
                        
                        geocodePosition(false, marker.getPosition());
                    });
    
                      // *
                      // START INFOWINDOW CUSTOMIZE.
                      // The google.maps.event.addListener() event expects
                      // the creation of the infowindow HTML structure 'domready'
                      // and before the opening of the infowindow, defined styles are applied.
                      // *
                      google.maps.event.addListener(infoWindow, 'domready', function() {
    
                        // Reference to the DIV that wraps the bottom of infowindow
                        var iwOuter = $('.gm-style-iw');
    
                        /* Since this div is in a position prior to .gm-div style-iw.
                         * We use jQuery and create a iwBackground variable,
                         * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
                        */
                        var iwBackground = iwOuter.prev();
    
                        // Removes background shadow DIV
                        iwBackground.children(':nth-child(2)').css({'display' : 'none'});
    
                        // Removes white background DIV
                        iwBackground.children(':nth-child(4)').css({'display' : 'none'});
    
                        // Moves the infowindow 115px to the right.
                        iwOuter.parent().parent().css({right: '115px'});
    
                        // Moves the shadow of the arrow 76px to the left margin.
                        iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
    
                        // Moves the arrow 76px to the left margin.
                        iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
    
                        // Changes the desired tail shadow color.
                        iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});
    
                        // Reference to the div that groups the close button elements.
                        var iwCloseBtn = iwOuter.next();
    
                        // Apply the desired effect to the close button
                        iwCloseBtn.css({opacity: '1', left: '60px', top: '3px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});
    
                        var iwCloseBtn2 = iwCloseBtn.next();
                        iwCloseBtn2.css({opacity: '1', left: '60px', top: '3px'});
    
                        // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
                        if($('.iw-content').height() < 140){
                          $('.iw-bottom-gradient').css({display: 'none'});
                        }
    
                        // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
                        iwCloseBtn.mouseout(function(){
                          $(this).css({opacity: '1'});
                        });
                      });
                }
    
                //Parseia o XML
                function downloadUrl(url, callback) {
                    var request = window.ActiveXObject ?
                        new ActiveXObject('Microsoft.XMLHTTP') :
                        new XMLHttpRequest;
    
                    request.onreadystatechange = function() {
                        if (request.readyState == 4) {
                            request.onreadystatechange = doNothing;
                            callback(request, request.status);
                        }
                    };
    
                    request.open('GET', url, true);
                    request.send(null);
                }
    
                function doNothing() {}
    
                /////////////////////////////////////////////
                // Funções dos Marcadores
                /////////////////////////////////////////////
                
                //Adiciona novo marcador ao mapa
                function newMarker(title, type) {
                    
                    //Verifica se title ou type não são vazios
                    if (!title || !type)
                        return false;
                        
                    //Pega os limites do mapa
                    var bounds = map.getBounds();
                    
                    //Obtém as coordenadas da posição central do mapa
                    var center = bounds.getCenter();
                    
                    //Ícone de acordo com o tipo de marcador
                    var icon = customIcons[type] || {};
                    
                    //Cria um novo marcador
                    var marker = new google.maps.Marker({
                        position: center,
                        title: title,
                        map: map,
                        icon: icon.icon,
                        draggable: true
                    });
                    
                    
                    //Grava as informações no array
                    markersData[title] = new Object();
                    markersData[title]['name'] = title;         
                    markersData[title]['type'] = type;
                    
                    //Obtém o endereço da posição global
                    geocodePosition(marker.getTitle(), marker.getPosition());
                              
                    //Adiciona os eventos de arrastar ao marcador
                    //Estes irão atualizar as informações do painel de informações
                     
                    //Exibe as informações do marcador selecionado no painel
                    google.maps.event.addListener(marker, 'click', function() {
                        
                        geocodePosition(marker.getTitle(), marker.getPosition());
                    });
                  
                    //Limpa os inputs
                    clearInputs();
                    
                    return false;
                }
                
                //Essa função recebe coordenadas de latitude e longitude
                // e retorna o endereço desta coordenada
                //Se title == false, então não atualiza dados de novos marcadores
                var geocoder = new google.maps.Geocoder();
                function geocodePosition(title, pos) {
                    
                    if (title) {
                        //Grava a latitude e longitude
                        markersData[title]['lat'] = pos.lat();
                        markersData[title]['lng'] = pos.lng();
                    }
                    
                    //Obtém o endereço das coordenadas
                    geocoder.geocode({
                        latLng: pos
                    }, function(responses) {
                        if (responses && responses.length > 0) {
                            
                            if (title)
                                markersData[title]['address'] = responses[0].formatted_address;
                        } else {
                            
                            if (title)
                                markersData[title]['address'] = "erro";
                        }
                    });
                }
                     
                
                
                
                /////////////////////////////////////////////
                // Funções de Busca de Endereços
                /////////////////////////////////////////////
                function geocode() {
                    var address = document.getElementById("address2").value;
                    geocoder.geocode({
                        'address': address,
                        'partialmatch': true}, geocodeResult);
                }
         
                function geocodeResult(results, status) {
                    if (status == 'OK' && results.length > 0) {
                        map.fitBounds(results[0].geometry.viewport);
                    } else {
                        alert("Geocode was not successful for the following reason: " + status);
                    }
                }
                
                
                /////////////////////////////////////////////
                // Funções Ajax
                /////////////////////////////////////////////
                
                //Cria um objeto para requisições XML
                function getXMLObject() {
                    var xmlHttp = false;
                    try {
                        xmlHttp = new ActiveXObject("Msxml2.XMLHTTP")  // For Old Microsoft Browsers
                    }
                    catch (e) {
                        try {
                            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP")  // For Microsoft IE 6.0+
                        }
                        catch (e2) {
                            xmlHttp = false;   // No Browser accepts the XMLHTTP Object then false
                        }
                    }
                    if (!xmlHttp && typeof XMLHttpRequest != 'undefined') {
                        xmlHttp = new XMLHttpRequest();        //For Mozilla, Opera Browsers
                    }
                    return xmlHttp;  // Mandatory Statement returning the ajax object created
                }
                     
                var xmlhttp = new getXMLObject();
                
            });
    
            function clicaMapa(dataRecolhimento,contador){                    
                //ativo, AfastadoAte12Meses, AfastadoApos12Meses, idadeMaxima, semdireito, opositor
                var cont = 0; 
                
                $.each(dataRecolhimento, function(idxData, objData) { 
                    cont++;
                });
    
                if(cont > 0){    
    
                     var outrasinfos = document.getElementById('outrasinfos'+contador);  
                     outrasinfos.style.display = 'block';   
    
                    $.each(dataRecolhimento, function(idxRec, objRec) { 
                        $.each(objRec, function(idxRec2, objRec2) {
                          
                            var dataRecolhe = objRec2.pagamento;
                            dataRecolhe = dataRecolhe.split('-');
                            mesRecolhimento = dataRecolhe[1];
                            dataRecolhe = dataRecolhe[2]+"/"+dataRecolhe[1]+"/"+dataRecolhe[0];
    
                            var mesRecolhe = '';
    
                            switch (mesRecolhimento) {
                              case '01':
                                mesRecolhe = 'Janeiro';
                                break;
                              case '02':
                                mesRecolhe = 'Fevereiro';
                                break;
                                case '03':
                                mesRecolhe = 'Março';
                                break;
                                case '04':
                                mesRecolhe = 'Abril';
                                break;
                                case '05':
                                mesRecolhe = 'Maio';
                                break;
                                case '06':
                                mesRecolhe = 'Junho';
                                break;
                                case '07':
                                mesRecolhe = 'Julho';
                                break;
                                case '08':
                                mesRecolhe = 'Agosto';
                                break;
                                case '09':
                                mesRecolhe = 'Setembo';
                                break;
                                case '10':
                                mesRecolhe = 'Outubro';
                                break;
                                case '11':
                                mesRecolhe = 'Novembro';
                                break;
                                case '12':
                                mesRecolhe = 'Dezembro';
                                break;
                            }
    
                            var total_Trab = document.getElementById('funcTotal'+contador);
                            var divTrab = document.createElement('div');
                            divTrab.innerHTML = '';
                            divTrab.innerHTML = '<div>'+detailsInfos[contador]["qtdeTrabalhadores"]+' FUNCIONÁRIOS</div>';
                            total_Trab.appendChild(divTrab);
    
                            var ultimorec = document.getElementById('ultimorec'+contador);
                            var divultimorec = document.createElement('div');
                            divultimorec.innerHTML = '<span>Ultimo Recolhimento: '+mesRecolhe+'</span>';
                            ultimorec.appendChild(divultimorec);   
                           
                            $.each(objRec2.grupo, function(idxGrupo, objGrupo) {
                                var grupo = objGrupo.nome;
                                var total = objGrupo.total;
    
                                if(total > 0) {
                                    switch (grupo) {
                                        case 'ativo':
                                            var ativo = document.getElementById('ativo'+contador);
                                            var divAtivo = document.createElement('p');
                                            divAtivo.innerHTML = '<div>Func. Ativos: '+total+'</div>';
                                            ativo.appendChild(divAtivo);
                                            ativo.style.display = 'block';  
                                        break;
    
                                        case 'AfastadoAte12Meses':
                                            var afastdos1 = document.getElementById('AfastadoAte12Meses'+contador);
                                            var divAfastadoAte12Meses = document.createElement('p');
                                            divAfastadoAte12Meses.innerHTML = '<div>Afastado Ate 12 Meses: '+total+'</div>';
                                            afastdos1.appendChild(divAfastadoAte12Meses);
                                            afastdos1.style.display = 'block';  
                                        break;
    
                                        case 'AfastadoApos12Meses':
                                            var afastados2 = document.getElementById('AfastadoApos12Meses'+contador);
                                            var divAfastadoApos12Meses = document.createElement('p');
                                            divAfastadoApos12Meses.innerHTML = '<div>Afastado Após 12 Meses: '+total+'</div>';
                                            afastados2.appendChild(divAfastadoApos12Meses);
                                            afastados2.style.display = 'block';  
                                        break;
    
                                        case 'idadeMaxima':
                                            var idadeMaxima = document.getElementById('idadeMaxima'+contador);
                                            var dividadeMaxima = document.createElement('p');
                                            dividadeMaxima.innerHTML = '<div>Trabalhadores acima de 70 anos: '+total+'</div>';
                                            idadeMaxima.appendChild(dividadeMaxima);
                                            idadeMaxima.style.display = 'block';  
                                        break;
    
                                        case 'semdireito':
                                            var semdireito = document.getElementById('semdireito'+contador);
                                            var divsemdireito = document.createElement('p');
                                            divsemdireito.innerHTML = '<div>Sem direito: '+total+'</div>';
                                            semdireito.appendChild(divsemdireito);
                                            semdireito.style.display = 'block';  
                                        break;
    
                                        case 'opositor':
                                            var opositor = document.getElementById('opositor'+contador);
                                            var divopositor = document.createElement('p');
                                            divopositor.innerHTML = '<div>Opositor: '+total+'</div>';
                                            opositor.appendChild(divopositor);
                                            opositor.style.display = 'block';  
                                        break;
                                    }
                                }
                            });
                        });
    
                    });
                }
            }
    
            function openDetail(cnpj){
                    
    
    
                    detalhesTitulo = '<div class="iw-subTitle">'+detailsInfos[cnpj]["name"]+'</div>';
    
                    listEmails = detailsInfos[cnpj]["email"];
                    listEmails = listEmails+",";
                    //listEmails = listEmails.replace(",", "<br>");
    
                    list_Emails = "";
    
                    listEmails.split(",").forEach(function (item) {
                        list_Emails = list_Emails+"<p>"+item+"</p>";
                    });
    
                    detalhes = '<h3>Dados da Empresa</h3>' +
                                '<div class="cnpj">CNPJ: '+detailsInfos[cnpj]["cnpj"]+'</div>' +                            
                                '<p>REGIÃO '+detailsInfos[cnpj]["regiao"] +'</p>' +
                                '<p>'+detailsInfos[cnpj]["logradouro"]+', '+detailsInfos[cnpj]["numero"]+' '+detailsInfos[cnpj]["comp"]+'</p>'+                                 
                                '<p>'+detailsInfos[cnpj]["bairro"]+'</p>'+
                                '<p>'+detailsInfos[cnpj]["cidade"]+' - '+detailsInfos[cnpj]["uf"] +'</p>'+                                  
                                '<p>'+detailsInfos[cnpj]["telefone"]+'</p>'+
                                list_Emails+
                                '<h3>Demais Informações</h3>' +
                                '<div class="divInfoFunTotal" id="funcTotal'+detailsInfos[cnpj]["cnpj"]+'"></div>' +
                                '<div class="divInfoFunTotalBtn" id="verFuncionarios'+detailsInfos[cnpj]["cnpj"]+'"></div>' +
                                '<div class="divInfo outrasinfos" id="outrasinfos'+detailsInfos[cnpj]["cnpj"]+'">' +  
                                '<div class="divInfo" id="ultimorec'+detailsInfos[cnpj]["cnpj"]+'"></div>' +
                                '<div class="divInfo" id="ativo'+detailsInfos[cnpj]["cnpj"]+'"></div>'+
                                '<div class="divInfo" id="AfastadoAte12Meses'+detailsInfos[cnpj]["cnpj"]+'"></div>'+
                                '<div class="divInfo" id="AfastadoApos12Meses'+detailsInfos[cnpj]["cnpj"]+'"></div>'+
                                '<div class="divInfo" id="idadeMaxima'+detailsInfos[cnpj]["cnpj"]+'"></div>'+
                                '<div class="divInfo" id="semdireito'+detailsInfos[cnpj]["cnpj"]+'"></div>'+
                                '<div class="divInfo" id="opositor'+detailsInfos[cnpj]["cnpj"]+'"></div>'+
    
                                '<a class="openFuncionarios badge badge-pill badge-primary" data-toggle="modal" data-target="#modalFuncionarios" id="'+detailsInfos[cnpj]["cnpj"]+'">Ver Funcionários</a>';
    
    
                    var detalhesInfos = document.getElementById('detalhesinfos');
                    detalhesInfos.innerHTML = "";
                    var detalhes_Infos = document.createElement('div');
                    detalhes_Infos.innerHTML = '<div class="detInfos">'+ detalhes +'</div>';
                    detalhesInfos.appendChild(detalhes_Infos);
    
                    var detalhesInfosTitulo = document.getElementById('detalhesinfosTitulo');
                    detalhesInfosTitulo.innerHTML = "";
                    var detalhes_InfosTitulo = document.createElement('div');
                    detalhes_InfosTitulo.innerHTML = '<div class="detInfostitulo">'+ detalhesTitulo +'</div>';
                    detalhesInfosTitulo.appendChild(detalhes_InfosTitulo);
    
                    // $.post("<?php echo home_url('mapas');?>",
                    // {  
                    //     geraRecolhimento: "geraRecolhimento",
                    //     cnpj: cnpj,
                    //     token: '<?php echo $token; ?>'
                    // },
                    //     function(dataRecolhimento){
                    //         clicaMapa(dataRecolhimento, cnpj); 
                            
                    //     }
                    // );                
        
                    //var urlRecolhimento = '<?php echo urlBsf; ?>/api-v1/integracao/'+cnpj+'/recolhimento';
                    var urlRecolhimento = 'https://cobranca.bsfonline.com.br/api-v1/integracao/'+cnpj+'/recolhimento';
                   
                    $.ajax({
                      url: urlRecolhimento,
                      headers: {
                          'Authorization': 'Bearer <?php echo $token; ?>',
                          'Content-type': 'application/json'
                      },
                      method: 'GET',
                      success: function(dataRecolhimento){
                        clicaMapa(dataRecolhimento, cnpj);
                      }
                    });
            }
    
            function listaFuncionario(data) {
              var listFuncionarios = '<table style="width:100%">' +
                                      '<tr>' +
                                        '<th></th>' +
                                        '<th>Nome</th>' +
                                        '<th>CPF</th>' +
                                        '<th>Função</th>' +
                                      '</tr>';
              var contNum = 0;
              $.each(data.content, function(idx, obj) { 
                contNum++;
                listFuncionarios = listFuncionarios + '<tr>' +
                                                        '<td>'+contNum+'</td>' +
                                                        '<td>'+obj.nome+'</td>' +
                                                        '<td>'+obj.cpf+'</td>' +
                                                        '<td>'+obj.funcao+'</td>' +
                                                      '</tr>';        
                    
              });
    
              listFuncionarios = listFuncionarios + '</table>';
    
              var detalhesInfosFun = document.getElementById('detalhesinfosfuncionarios');
              detalhesInfosFun.innerHTML = "";
              var detalhes_InfosFunc = document.createElement('div');
              detalhes_InfosFunc.innerHTML = '<div class="detInfosFunc">'+ listFuncionarios +'</div>';
              detalhesInfosFun.appendChild(detalhes_InfosFunc);
              $('#loadFuncionarios').hide();
    
            }
    
            function openFuncionarios(cnpj){
              detalhesTitulo = '<div class="iw-subTitle">'+detailsInfos[cnpj]["name"]+'</div>';
    
              var detalhesInfosFun = document.getElementById('detalhesinfosfuncionarios');
              detalhesInfosFun.innerHTML = "";
    
              var detalhesInfosTitulo = document.getElementById('detalhesinfosTitulofuncionarios');
              detalhesInfosTitulo.innerHTML = "";
              var detalhes_InfosTitulo = document.createElement('div');
              detalhes_InfosTitulo.innerHTML = '<div class="detInfostitulo">'+ detalhesTitulo +'</div>';
              detalhesInfosTitulo.appendChild(detalhes_InfosTitulo);
    
              //var urlRecolhimento = '<?php echo urlBsf; ?>/api-v1/integracao/'+cnpj+'/trabalhador';
              var urltrabalhador = 'https://cobranca.bsfonline.com.br/api-v1/integracao/'+cnpj+'/trabalhador';
             
              $.ajax({
                url: urltrabalhador,
                headers: {
                    'Authorization': 'Bearer <?php echo $token; ?>',
                    'Content-type': 'application/json'
                },
                method: 'GET',
                success: function(dataRecolhimento){
                  //clicaMapa(dataRecolhimento, cnpj);
                  listaFuncionario(dataRecolhimento);
                }
              });
            }
    
    
    
            function addClassDrop(showClass) {
                document.getElementById("myDropdown").classList.add(showClass);
            }
    
            function rmClassDrop(rmClass) {
                document.getElementById("myDropdown").classList.remove(rmClass);
            }
    
            function filterFunction() {
              var input, filter, ul, li, a, i;
              input = document.getElementById("myInput");
              filter = input.value.toUpperCase();
              div = document.getElementById("myDropdown");
              a = div.getElementsByTagName("a");
              if(filter.length > 0) {
                if(!document.getElementById("myDropdown").classList.contains('showdrop'))
                    addClassDrop('showdrop');
              } else {
                rmClassDrop('showdrop');
              }
    
              for (i = 0; i < a.length; i++) {
                txtValue = a[i].textContent || a[i].innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                  a[i].style.display = "";
                } else {
                  a[i].style.display = "none";
                }
              }
            }
    
    
    
        function clickbusca(point,marker) {
                var clicado = point.split('|');

                //innerHTML = '<a onclick="clickbusca(this.id)" id="'+dataJson[i].razao+'|'+dataJson[i].geo.lat+'|'+dataJson[i].geo.long+'|'+dataJson[i].cnpj+'" href="#'+dataJson[i].razao+'">'+dataJson[i].razao+'</a>';

                map.setCenter(new google.maps.LatLng(clicado[1], clicado[2])); 


                $('.mobile #dismiss').click();

                new google.maps.event.trigger( detailsInfos[clicado[3]]["marker"], 'click' );

    
                var idclicado = clicado[3];
                //$('#modalDetail').modal('show').delay(500); 
                //openDetail(idclicado); 
                //document.getElementById("myDropdown").classList.remove('showdrop');
    
                //markers[clicado[1]+clicado[2]]['infowindow'].open(map, markers[clicado[1]+clicado[2]]['marker']);
    
                //geocodePosition(false, markers[clicado[1]+clicado[2]]['marker'].getPosition());
              
              //map.setCenter({lat:Number(markers[clicado[1]+clicado[2]]['lat']), lng:Number(markers[clicado[1]+clicado[2]]['long'])});
    
              //document.getElementById(clicado[3]).click();
            }
    
    
    
    
    
            $(document).on('click', "a.öpenDetail", function(event) {
                event.stopPropagation();
                event.stopImmediatePropagation();
                var idclicado = $(this).attr('id');
                $('#modalDetail').modal('show').delay(500); 
                openDetail(idclicado);     
            });
    
            $(document).on('click', "a.openFuncionarios", function(event) {
                event.stopPropagation();
                event.stopImmediatePropagation();
                var idclicado = $(this).attr('id');
                $('#modalFuncionarios').modal('show').delay(500); 
                $('#loadFuncionarios').show();
                openFuncionarios(idclicado);     
            });
    
            $(document).on('click', "button.close", function(event) {
               $(".gm-ui-hover-effect").click(); 
            });
    
            //]]>
        </script>
</head>

<!-- Executa a função load() ao carregar o corpo do documento -->

<body>
 <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btnMenuMobile">
                            <i class="fa fa-align-justify" aria-hidden="true"></i>
                    </button>
                    <span class="logo">
                        <img width="195" src="https://mapeamento.bsfonline.com.br/wp-content/themes/base10layout/img/marcadagua.png">
                    </span>
                </div>
            </nav>
    <div class="wrapper">


        <!-- Sidebar -->
        <nav id="sidebar" class="active">

            <div id="dismiss">
                <i class="fas fa-arrow-left"></i>
            </div>

            <div class="sidebar-header">
                <h3>Menu</h3>
            </div>

            <ul class="list-unstyled components">
               <li>
                    <div class="dropdown">
                        <!-- <button onclick="myFunction()" class="dropbtn">Buscar Empresa</button> -->
                        <input type="text" placeholder="Buscar empresa..." id="myInput" onkeyup="filterFunction()">
                        <div id="myDropdown" class="dropdown-content">

                        </div>
                    </div>
                </li>
                <li>
                    <a class="btnCadEmpresa" href="#" data-toggle="modal" data-target="#myModal">Cadastrar Empresa</a>
                </li>

                <li>
                    <div id="filtro">
                        <?php 
                            $urlPagina = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 

                            $urlPagina = str_replace("&filtro=inadimplente", "", $urlPagina);
                            $urlPagina = str_replace("&filtro=adimplente", "", $urlPagina);                
                            $urlPagina = str_replace("&filtro=emcobranca", "", $urlPagina);
                        ?>
                            <span><p>Filtrar Por: </p></span>
                            <span>
                            <a href="<?php echo $urlPagina.'&filtro=adimplente'; ?>">
                                <img src="/wp-content/themes/base10layout/img/adimplente.png">
                                Adimplente
                            </a>
                        </span>
                            <span>
                            <a href="<?php echo $urlPagina.'&filtro=inadimplente'; ?>">
                                <img src="/wp-content/themes/base10layout/img/inadimplente.png">
                                Inadimplente
                            </a>
                        </span>
                            <span>
                            <a  href="<?php echo $urlPagina.'&filtro=emcobranca'; ?>">
                                <img src="/wp-content/themes/base10layout/img/emcobranca.png">
                                Em Cobrança
                            </a>
                        </span>
                            <span>
                            <a class="removerFiltro" href="<?php echo $urlPagina; ?>">
                                X Sem filtro
                            </a>
                        </span>
                        </div>
                </li>
                 
            </ul>
        </nav>

        <!-- Page Content -->
        <div id="content">

            

            <input type="hidden" value="<?php echo $geolocalicazao['lat']; ?>" />

            <div class="loa_ding">
                <span class="loading">Atualizando pontos no mapa</span>
                <div class="lds-ripple">
                    <div></div>
                    <div></div>
                </div>
            </div>
            <div class="loading_falha">
                <span class="loading">Não foram encontradas empresas nas proximidades</span>
                <span class="info2">Arraste para outra localização ou diminua o zoom para ampliar a área</span>
            </div>
            <div id="mapLegend">
            </div>
           
            <div id="legenda">
            </div>

            


            <!--div id="btnCadastro">
            <a href="#" class="form-submit-button btn btn-default">CADASTRE-SE</a>
        </div-->

            <!-- Div onde será carregado o mapa -->
            <div id="map"></div>

            

            <?php
            if(isset($_REQUEST['empresa_cadastrar'])){

              $empresa_nome = $_REQUEST['empresa_nome'];
              $empresa_cnpj = $_REQUEST['empresa_cnpj'];
              $empresa_email = $_REQUEST['empresa_email'];
              $empresa_logradouro = $_REQUEST['empresa_logradouro'];
              $empresa_cidade = $_REQUEST['empresa_cidade'];
              $empresa_uf = $_REQUEST['empresa_uf'];
              $empresa_telefone = $_REQUEST['empresa_telefone'];
              $empresa_contato = $_REQUEST['empresa_contato'];
              $syndicate_doc = "--";          
              $empresa_numero = $_REQUEST['empresa_numero'];
              $empresa_bairro = $_REQUEST['empresa_bairro'];
              $empresa_complemento = $_REQUEST['empresa_complemento'];
                  
              $emailenvio = "fernando.compuway@gmail.com";
                         
              $ip = $_SERVER["REMOTE_ADDR"];        
              $data = date("d/m/Y - H:i:s", time()); 

              $nomeAnexo = "";


              $postData = array( 
                "nome" => $empresa_nome,
                "cnpj" => $empresa_cnpj,
                "email" => $empresa_email,
                "endereco" => $empresa_logradouro,
                "cidade" => $empresa_cidade,
                "uf" => $empresa_uf,
                "telefone" => $empresa_telefone,
                "contato" => "required",
                "numero" => $empresa_numero,
                "bairro" => $empresa_bairro,
                "complemento"=> $empresa_complemento
              );
            
              
            

              // Setup cURL
              $ch = curl_init('https://www.beneficiosocial.com.br/cadastroempresasapp');
              curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
              ));

              // Send the request
              $response_CadEmpresa = "";
              $responseCadEmpresa = curl_exec($ch);

              // Check for errors
              if($responseCadEmpresa === FALSE){
                //die(curl_error($ch));
                $mensagem = curl_error($ch);
              }
              else{
                $response_CadEmpresa = json_decode($responseCadEmpresa, TRUE);
                
                if (count($response_CadEmpresa) == count($response_CadEmpresa, COUNT_RECURSIVE)) 
                {
                  foreach ($response_CadEmpresa as $_response_CadEmpresa) { 
                    $mensagem = $_response_CadEmpresa;
                  } 
                }
                else
                {
                  foreach ($response_CadEmpresa["mensagem"] as $_response_CadEmpresa) { 
                    $mensagem = $_response_CadEmpresa;
                  } 
                  
                }
                
              
                if($mensagem != "sucesso"){
                  //$mensagem = $response_CadEmpresa;
                }
                else{


                  include("wp-content/themes/base10layout/mpdf61/mpdf.php"); 

                  function render_email_anexo($empresa_nome,$empresa_cnpj,$empresa_email,$empresa_logradouro,$empresa_numero,$empresa_complemento,$empresa_bairro,$empresa_cidade,$empresa_uf,$empresa_telefone,$empresa_contato) {
                      ob_start();
                      
                      $_templateSindicato = "wp-content/themes/base10layout/templates_cadastro/email-template-".$syndicate_doc.".phtml";

                      include ''.$_templateSindicato.'';

                      $fichaanexo = ob_get_contents(); 

                      echo $fichaanexo;

                      ob_end_clean();
                      ob_start();
                      
                            
                      $mpdf=new mPDF(); 
                      $mpdf->SetDisplayMode('fullpage');
                      $mpdf->WriteHTML($fichaanexo);
                      ob_clean();
                      $time = date("dmYHis", time());

                      $mpdf->showImageErrors = true;
                      $nomeAnexo = "Ficha-".uniqid().".pdf";
                      $mpdf->Output($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/base10layout/templates/fichas/'.$nomeAnexo, 'F');

                      //$ficha_anexo = ob_get_contents(); 
                      ob_end_clean();
                      return $nomeAnexo;        
                  }

                  function render_email($empresa_nome,$empresa_cnpj,$empresa_email,$empresa_logradouro,$empresa_numero,$empresa_complemento,$empresa_bairro,$empresa_cidade,$empresa_uf,$empresa_telefone,$empresa_contato) {
                      ob_start();
                      
                      include "wp-content/themes/base10layout/templates_cadastro/email-template-padrao.phtml";
                      $emailPadrao = ob_get_contents();

                      ob_end_clean();

                      return $emailPadrao;
                  }


                  $mens = render_email($empresa_nome,$empresa_cnpj,$empresa_email,$empresa_logradouro,$empresa_numero,$empresa_complemento,$empresa_bairro,$empresa_cidade,$empresa_uf,$empresa_telefone,$empresa_contato);

                  require("wp-content/themes/base10layout/phpmailer/PHPMailerAutoload.php");        
                  try {
                      $emailContato = "fernando.compuway@gmail.com";

                      $mail = new PHPMailer(true);    
                      // Tell PHPMailer to use SMTP
                      $mail->isSMTP();          
                      $mail->Username = "AKIAIQXP4STRWT6I4NUA";
                      $mail->Password = "ArqyCGexrIu2LeCM4G7YRI40DF6K3WzOccCIirk2cCh9";              
                      //$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
                      $mail->Debugoutput = "html";
                      $mail->Host = "email-smtp.us-east-1.amazonaws.com";          
                      $mail->SMTPAuth = true;
                      $mail->SMTPSecure = "tls";
                      //$mail->SMTPDebug = 3;
                      $mail->Port = 587;
                      $mail->AltBody = "Cadastro de Empresa BSFOnLine";

                      $mail->isHTML(true);

                      $mail->From = "filiacao@bsfonline.com.br";
                      $mail->FromName = "BSFOnLine";
                      $mail->CharSet = "UTF-8"; // Charset da mensagem (opcional) 
                      $mail->AddAddress($emailContato);
                      $mail->AddBCC("itdev@beneficiosocial.com.br");
                      $mail->AddBCC("fernando.compuway@gmail.com");
                      $mail->Subject = "Cadastro de Empresa BSFOnLine";    

                      $templateSindicato = "templates_cadastro/email-template-".$syndicate_doc.".phtml";  

                      $mail->MsgHTML($mens);
                      
                      if(!$mail->send()) {

                           $mensagem = "<p >Desculpe o transtorno, ocorreu um problema ao enviar sua filiação, tente novamente mais tarde. Obrigado.</p>";
                           $mensagem .= $mail->ErrorInfo;
                      } else {
                          $mensagem = "<p><h4>Solicitação enviada com sucesso</h4><br/>Agradecemos o contato. <br/>Em breve entraremos em contato. <br/></p>"; 
                          //echo "Email sent!" , PHP_EOL;
                      }

                  }
                  catch (phpmailerException $e) {
                      $mensagem = "<p >Desculpe o transtorno, ocorreu um problema ao enviar sua filiação, tente novamente mais tarde. Obrigado.</p>";
                      $mensagem .= $e->errorMessage();
                  }
                  catch (Exception $e) {
                      $mensagem = "<p >Desculpe o transtorno, ocorreu um problema ao enviar sua filiação, tente novamente mais tarde. Obrigado.</p>";
                      $mensagem .= "<p >".$e->getMessage()."</p>";
                  }

                }

                $empresa_nome = "";
                $empresa_cnpj = "";
                $empresa_email = "";
                $empresa_logradouro = "";
                $empresa_cidade = "";
                $empresa_uf = "";
                $empresa_telefone = "";
                $empresa_contato = "";
                $syndicate_doc = "";          
                $empresa_numero = "";
                $empresa_bairro = "";
                $empresa_complemento = "";  
              }
          }

        ?>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content" style="float: left;">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Cadastro de Empresa</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                
                            </div>
                            <div class="modal-body">
                                <div class="buscaform">
                                    <div class="col-xs-12 col-md-12">
                                        <form class="jotform-form" action="#" method="post" name="form_73455838382669" id="73455838382669" accept-charset="utf-8">
                                            <input type="hidden" name="formID" value="73455838382669" />

                                            <div class="retorno-ajax" id="retorno-ajax"></div>

                                            <div class="form-all" id="camposForm">

                                                <ul class="form-section page-section">
                                                    <li id="cid_87" class="form-input-wide" data-type="control_head">
                                                        <div class="form-header-group ">
                                                            <div class="header-text httac htvam">



                                                            </div>
                                                        </div>
                                                    </li>


                                                    <input type="hidden" id="entidade_sigla" name="entidade_sigla" value="<?php echo $entidade_sigla; ?>" />
                                                    <input type="hidden" id="entidade_nome" name="entidade_nome" value="<?php echo $entidade_nome; ?>" />
                                                    <input type="hidden" id="entidade_cnpj" name="entidade_cnpj" value="<?php echo $entidade_cnpj; ?>" />

                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              CEP
                              <span class="form-required">
                                *
                              </span>
                            </label>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="text" id="empresa_cep" name="empresa_cep" data-type="input-textbox" class="form-textbox validate[required]" size="20" data-component="textbox" required="" value="<?php echo $empresa_nome; ?>" required="" />
                                
                              </span>
                                                        </div>
                                                    </li>

                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              Nome
                              <span class="form-required">
                                *
                              </span>
                            </label>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="text" id="empresa_nome" name="empresa_nome" data-type="input-textbox" class="form-textbox validate[required]" size="20" data-component="textbox" required="" value="<?php echo $empresa_nome; ?>" required="" />
                                
                              </span>
                                                        </div>
                                                    </li>

                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              CNPJ
                            </label>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="text" id="empresa_cnpj" name="empresa_cnpj" data-type="input-textbox" class="form-textbox validate[required]" size="20" Data-component="textbox" value="<?php echo $empresa_cnpj; ?>"  />
                                
                              </span>
                                                        </div>
                                                    </li>

                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              Contato
                              <span class="form-required">
                                *
                              </span>
                            </label>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="text" id="empresa_contato" name="empresa_contato" data-type="input-textbox" class="form-textbox validate[required]" size="20" data-component="textbox" required="" value="<?php echo $empresa_contato; ?>" required="" />
                                
                              </span>
                                                        </div>
                                                    </li>

                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              Email
                            </label>
                                                        <span class="form-required">
                                *
                              </span>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="email" id="empresa_email" name="empresa_email" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="<?php echo $empresa_email; ?>" data-component="textbox" required=""/>
                                <label class="form-sub-label" for="input_39" style="min-height:13px;"></label>
                              </span>
                                                        </div>
                                                    </li>

                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              Endereço
                            </label>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="text" id="empresa_logradouro" name="empresa_logradouro" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="<?php echo $empresa_logradouro; ?>" data-component="textbox" />
                                <label class="form-sub-label" for="input_39" style="min-height:13px;"></label>
                              </span>
                                                        </div>
                                                    </li>

                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              Numero
                            </label>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="text" id="empresa_numero" name="empresa_numero" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="<?php echo $empresa_numero; ?>" data-component="textbox" />
                                <label class="form-sub-label" for="input_39" style="min-height:13px;"></label>
                              </span>
                                                        </div>
                                                    </li>

                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              Complemento
                            </label>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="text" id="empresa_complemento" name="empresa_complemento" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="<?php echo $empresa_complemento; ?>" data-component="textbox" />
                                <label class="form-sub-label" for="input_39" style="min-height:13px;"></label>
                              </span>
                                                        </div>
                                                    </li>

                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <lbabel class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                                                            Bairro
                                                            </label>
                                                            <div id="cid_39" class="input-field ">
                                                                <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="text" id="empresa_bairro" name="empresa_bairro" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="<?php echo $empresa_bairro; ?>" data-component="textbox" />
                                <label class="form-sub-label" for="input_39" style="min-height:13px;"></label>
                              </span>
                                                            </div>
                                                    </li>

                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              Cidade
                              <span class="form-required">
                                *
                              </span>
                            </label>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="text" id="empresa_cidade" name="empresa_cidade" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="<?php echo $empresa_cidade; ?>" data-component="textbox" required="" />
                                <label class="form-sub-label" for="input_39" style="min-height:13px;"> </label>
                              </span>
                                                        </div>
                                                    </li>
                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              Estado
                              <span class="form-required">
                               *
                              </span>
                            </label>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="text" id="empresa_uf" name="empresa_uf" data-type="input-textbox" class="form-textbox validate[required]" required=""  size="20" value="<?php echo $empresa_uf; ?>" data-component="textbox" />
                                <label class="form-sub-label" for="input_39" style="min-height:13px;"> </label>
                              </span>
                                                        </div>
                                                    </li>
                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              Telefone
                              <span class="form-required">
                                *
                              </span>
                            </label>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="phone" id="empresa_telefone" name="empresa_telefone" data-type="input-textbox" class="form-textbox validate[required]" required=""  size="20" value="<?php echo $empresa_telefone; ?>" data-component="textbox" required="" />
                                
                              </span>
                                                        </div>
                                                    </li>

                                                    <li class="input-field col-xs-12 col-md-12 m12 jf-required" data-type="control_textbox" id="id_39">
                                                        <label class="form-label form-label-left form-label-auto" id="label_39" for="input_39">
                              Segmento
                              <span class="form-required">
                                *
                              </span>
                            </label>
                                                        <div id="cid_39" class="input-field ">
                                                            <span class="form-sub-label-container" style="vertical-align:top;">
                                <input type="phone" id="empresa_segmento" name="empresa_segmento" data-type="input-textbox" class="form-textbox validate[required]" required=""  size="20" value="<?php echo $empresa_segmento; ?>" data-component="textbox" required="" />
                                
                              </span>
                                                        </div>
                                                    </li>



                                                    <li class="input-field col-xs-12 col-md-12 m12 control_pagebreak" data-type="control_button" id="id_85">
                                                        <div id="cid_85" class="form-input-wide">
                                                            <div style="" class="form-buttons-wrapper">
                                                                <div id="load" style="display:none;" class="load"> </div>
                                                                <button id="input_85" type="button" class="form-submit-button btn btn-success" data-component="button" name="empresa_cadastrar">
                                  Cadastrar
                                </button>

                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
                                                            </div>
                                                        </div>
                                                    </li>



                                                </ul>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
        </div>

        <script type="text/javascript">
            $(window).resize(function(){
              if(window.innerWidth < 980){
                $('#sidebar').removeClass('active');
              }
            });

            $(document).ready(function(){

                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                if(window.innerWidth < 980){
                    $('#sidebar').removeClass('active');
                  }

                $('#dismiss, .overlay').on('click', function () {
                    // hide sidebar
                    $('#sidebar').removeClass('active');
                    // hide overlay
                    $('.overlay').removeClass('active');
                });

                $('#sidebarCollapse').on('click', function () {
                    // open sidebar
                    $('#sidebar').addClass('active');
                    // fade in the overlay
                    $('.overlay').addClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
    
               $('#empresa_cnpj').mask('00.000.000/0000-00', {reverse: true});
               $('#empresa_telefone').mask('(00) 0000-00009');
        
               // $('#empresa_telefone')
               //  .mask("(99) 9999-9999?9")
               //  .focusout(function (event) {  
               //      var target, phone, element;  
               //      target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
               //      phone = target.value.replace(/\D/g, '');
               //      element = $(target);  
               //      element.unmask();  
               //      if(phone.length > 10) {  
               //          element.mask("(99) 99999-999?9");  
               //      } else {  
               //          element.mask("(99) 9999-9999?9");  
               //      }  
               //  });
        
                
        
               $("#cadastrarempresa").click(function(){
                $('#camposForm').show();           
        
                    $("#retorno-ajax").empty().html('');
               }); 
        
               function limpa_formulário_cep() {
                        // Limpa valores do formulário de cep.
                        $("#empresa_logradouro").val("");
                        $("#empresa_bairro").val("");
                        $("#empresa_cidade").val("");
                        $("#empresa_uf").val("");
                        
        
                    }
                    
                    //Quando o campo cep perde o foco.
                    $("#empresa_cep").blur(function() {
        
                        //Nova variável "cep" somente com dígitos.
                        var cep = $(this).val().replace(/\D/g, '');
        
                        //Verifica se campo cep possui valor informado.
                        if (cep != "") {
        
                            //Expressão regular para validar o CEP.
                            var validacep = /^[0-9]{8}$/;
        
                            //Valida o formato do CEP.
                            if(validacep.test(cep)) {
        
                                //Preenche os campos com "..." enquanto consulta webservice.
                                $("#empresa_logradouro").val("...");
                                $("#empresa_bairro").val("...");
                                $("#empresa_cidade").val("...");
                                $("#empresa_uf").val("...");
                                
        
                                //Consulta o webservice viacep.com.br/
                                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
        
                                    if (!("erro" in dados)) {
                                        //Atualiza os campos com os valores da consulta.
                                        $("#empresa_logradouro").val(dados.logradouro);
                                        $("#empresa_bairro").val(dados.bairro);
                                        $("#empresa_cidade").val(dados.localidade);
                                        $("#empresa_uf").val(dados.uf);
                                        
                                    } //end if.
                                    else {
                                        //CEP pesquisado não foi encontrado.
                                        limpa_formulário_cep();
                                        alert("CEP não encontrado.");
                                    }
                                });
                            } //end if.
                            else {
                                //cep é inválido.
                                limpa_formulário_cep();
                                alert("Formato de CEP inválido.");
                            }
                        } //end if.
                        else {
                            //cep sem valor, limpa formulário.
                            limpa_formulário_cep();
                        }
                    });
         
        
        
               $("#input_85").click(function(){
        
                // remove as mensagens de erro
                $(".erromsg").remove();
        
                // verificar se os campos foram preenchidos
                var empresa_nome = $("#empresa_nome");
                var empresa_cnpj = $("#empresa_cnpj");
                var empresa_email = $("#empresa_email");
                var empresa_cep = $("#empresa_cep");
        
                var empresa_logradouro  = $("#empresa_logradouro");
                var empresa_numero  = $("#empresa_numero");
                var empresa_bairro  = $("#empresa_bairro");
        
                var empresa_cidade = $("#empresa_cidade");
                var empresa_uf = $("#empresa_uf");
                var empresa_telefone = $("#empresa_telefone");
                var empresa_segmento = $("#empresa_segmento");
        
                
        
                // Mensagem de erro padrão a ser inserida após o campo
                var erromsg = '<div class="erromsg">Preencha o campo <span></span></div>';
        
                if(!empresa_cep.val() || empresa_cep.val().length < 8){
                   empresa_cep.after(erromsg);
                   $(".erromsg span").text("cep da empresa corretamente");
                   return;
                }
        
                if(!empresa_nome.val() || empresa_nome.val().length < 2){
                   empresa_nome.after(erromsg);
                   $(".erromsg span").text("nome da empresa corretamente");
                   return;
                }
        
                if(!empresa_cnpj.val() || empresa_cnpj.val().length < 14){
                   empresa_cnpj.after(erromsg);
                   $(".erromsg span").text("CNPJ da empresa corretamente");
                   return;
                }
        
                if(!empresa_email.val() || empresa_email.val().length < 5){
                   empresa_email.after(erromsg);
                   $(".erromsg span").text("email da empresa corretamente");
                   return;
                }   
        
                if(!empresa_logradouro.val() || empresa_logradouro.val().length < 2){
                   empresa_logradouro.after(erromsg);
                   $(".erromsg span").text("endereço da empresa corretamente");
                   return;
                }
        
                if(!empresa_numero.val() || empresa_numero.val().length < 2){
                   empresa_numero.after(erromsg);
                   $(".erromsg span").text("numero do endereço da empresa corretamente");
                   return;
                }
        
                if(!empresa_bairro.val() || empresa_bairro.val().length < 2){
                   empresa_bairro.after(erromsg);
                   $(".erromsg span").text("bairro da empresa corretamente");
                   return;
                }     
        
                if(!empresa_cidade.val() || empresa_cidade.val().length < 2){
                   empresa_cidade.after(erromsg);
                   $(".erromsg span").text("cidade da empresa corretamente");
                   return;
                }
        
                if(!empresa_uf.val() || empresa_uf.val().length < 2){
                   empresa_uf.after(erromsg);
                   $(".erromsg span").text("UF da empresa corretamente");
                   return;
                }
        
                if(!empresa_telefone.val() || empresa_telefone.val().length < 5){
                   empresa_telefone.after(erromsg);
                   $(".erromsg span").text("telefone da empresa corretamente");
                   return;
                }
        
                if(!empresa_segmento.val() || empresa_segmento.val().length < 2){
                   empresa_segmento.after(erromsg);
                   $(".erromsg span").text("segmento da empresa corretamente");
                   return;
                }
        
                $("#load").show();
                $.post("<?php echo home_url('/');?>cadastroempresa.php",
                {   
                    empresa_nome: $('#empresa_nome').val(),
                    empresa_cnpj: $('#empresa_cnpj').val(),
                    empresa_email: $('#empresa_email').val(),
                    empresa_logradouro: $('#empresa_logradouro').val(),
                    empresa_cidade: $('#empresa_cidade').val(),
                    empresa_uf: $('#empresa_uf').val(),
                    empresa_telefone: $('#empresa_telefone').val(),
                    empresa_contato: $('#empresa_contato').val(),
                    empresa_complemento: $('#empresa_complemento').val(),
                    empresa_numero: $('#empresa_numero').val(),
                    empresa_bairro: $('#empresa_bairro').val(),
                    entidade_sigla: $('#entidade_sigla').val(),
                    entidade_nome: $('#entidade_nome').val(),
                    entidade_cnpj: $('#entidade_cnpj').val(),
                    empresa_segmento: $('#empresa_segmento').val(),
        
                    empresa_cadastrar: 'empresa_cadastrar'
                },
                    function(data){
                        $("#load").hide();
                        $("#retorno-ajax").delay(2000).empty().html(data); 
                        if (data.indexOf("sucesso") >= 0){
                            $('#empresa_cep').val('');
                            $('#empresa_nome').val('');
                            $('#empresa_cnpj').val('');
                            $('#empresa_logradouro').val('');
                            $('#empresa_email').val('');
                            $('#empresa_cidade').val('');
                            $('#empresa_uf').val('');
                            $('#empresa_telefone').val('');
                            $('#empresa_contato').val('');
                            $('#empresa_numero').val('');
                            $('#empresa_bairro').val('');
                            $('#empresa_complemento').val(''); 
                            $('#camposForm').hide();           
        
                        }
        
                    }
                )
               });
        
        
            });
        </script>

        <?php
    if($mensagem){
        ?>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#myModal').modal('show');    
                });
            </script>
            <?php
    }


?>

                <!-- Modal -->
                <div id="modalDetail" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                
                                <h4 class="modal-title" id="detalhesinfosTitulo"></h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body" id="detalhesinfos">

                            </div>
                        </div>

                    </div>
                </div>

                <!-- Modal -->
                <div id="modalFuncionarios" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                
                                <h4 class="modal-title" id="detalhesinfosTitulofuncionarios"></h4>
                                <div id="loadFuncionarios" style="display:none;" class="load"> </div>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body" id="detalhesinfosfuncionarios">

                            </div>
                        </div>

                    </div>
                </div>



                <div class="legendafooter">
                <div id="legendainfosmobile"></div>
                <div class="icones">
                    <span>
                        <img src="/wp-content/themes/base10layout/img/adimplente.png">
                        Adimplente
                        
                    </span>
                    <span>
                        <img src="/wp-content/themes/base10layout/img/inadimplente.png">
                        Inadimplente
                    </span>
                    <span>
                        <img src="/wp-content/themes/base10layout/img/emcobranca.png">
                        Em Cobrança
                    </span>
                    <span>
                        <img src="/wp-content/themes/base10layout/img/mesmoendereco.png">
                        Multiplas
                    </span>
                </div>
            </div>


    </div>
    <!-- Dark Overlay element -->
    <div class="overlay"></div>
    </div>

</body>

</html>