<?php
	get_header();

require_once 'pagseguro/config.php';

    $naoEncontrado = false;

    $ambiente = "Dev";
    $urlBsf = "https://api.bsfonline.com.br";
    if($ambiente == "teste"){
      $urlBsf = "https://api.preprod.bsfonline.com.br";
    }


    if(isset($_REQUEST['doc']) || isset($_REQUEST['cpf']))
    {

      if(isset($_REQUEST['doc']))
      {
        $doc = $_REQUEST['doc'];
        $birthDate = $_REQUEST['birthDate'];

      } else {
        $doc = $_REQUEST['cpf'];
        $birthDate = $_REQUEST['birthDate'];
      }

        //var_dump($birthDate);

        // $var = '20/04/2012';
        //   $date = str_replace('/', '-', $var);
        //   echo date('Y-m-d', strtotime($date));

        $postData = array(
            "doc"=>$doc,
            "birthDate"=>$birthDate
        );

        $ch = curl_init($urlBsf.'/trabalhadores/auth');
        curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));

        // Send the request
        $response = curl_exec($ch);

        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }

        $responseData = json_decode($response, TRUE);

        //var_dump($responseData);       

        $contador = 0;

        $employee = array();    
        $employer = array();
        $syndicate = array();

        //foreach ($responseData as $_responseData) { 
        $employee = $responseData['employee'];
        $employer = $responseData['employer'];
        $syndicate = $responseData['syndicate'];
        //} 

        if(is_null($employee['birthDate'])){
            $naoEncontrado = true;
        } 

    }
?>
<style type="text/css">
    .busca {
        height: 310px;
        width: 450px;
        padding: 45px 0;
        /* border: 2px solid #ccc; */
        border-radius: 8px;
        background: rgba(255,255,255,0.8);
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        text-align: left;
    }

    .btnBuscarHome{
        margin-top: 20px;
    }
    .inputbusca {
        width: 80%;
        margin-bottom: 20px;
        margin-top: 0px;
        padding: 7px 4px;
        text-align: left;
        font-size: 16px;
        border: none;
        border-radius: 5px;
    }
    .naoEncontrado{
        color: red;
        font-size: 12px;
        font-weight: bold;
    }
</style>
<!--******************** INICIO CONTEUDO ********************-->
    <?php if(is_null($employee['birthDate'])) { ?>
    <div class="container">
        <div class="row">
            <div class="busca">
                <div class="col-xs-12 col-md-12">             
                  <div class="col-xs-12 col-md-12">
                    <p class="titulo">INFORME OS DADOS</p>
                  </div>
                </div>
                <div class="col-xs-12 col-md-12">
                    <?php if($naoEncontrado == true) { ?>
                    <div class="input-field col-xs-12 col-md-12">
                        <label class="naoEncontrado">Não encontramos seu cadastro em nosso sistema. Tente novamente.</label>
                    </div>
                    <?php } ?>
                    <form class="pure-form pure-form-stacked formHome" action="<?php echo home_url('consulta-associado'); ?>" id="buscaestado" method="post">
                        <div class="input-field col-xs-12 col-md-12">
                            <label>CPF</label>
                        </div>
                        <div class="input-field col-xs-12 col-md-12">
                            <input type="number" name="cpf" id="" required="true" class="inputbusca" />
                        </div>

                        <div class="input-field col-xs-12 col-md-12">
                            <label>DATA DE NASCIMENTO</label>
                        </div>
                         <div class="input-field col-xs-12 col-md-12">
                            <input type="date" id="birthDate" name="birthDate" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" required="true" />
                        </div>

                        
                       
                        <div class="input-field col-xs-12 col-md-12 m12 btnBuscarHome">
                            <button class="btn waves-effect waves-light" type="submit" name="buscaCidade">BUSCAR                      
                            </button>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php }  else { ?>
        <link href="<?php echo home_url('/'); ?>wp-content/themes/base10layout/css/pagseguro.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            .formaPagamento label{
              float: left;
                margin-left: 5px;
                margin-right: 12px;
                padding: 0 !important;
                width: auto !important;
            }

            .formaPagamento input {
              float: left;
              box-shadow: 0;
            }
            .creditCardFields{
              width: 100%;
              float: left;
            }
            form#payment fieldset {
                width: 100%;
                float: left;
                padding: 0;
                border: none !important;
                    text-align: left;
            }
            .field input, .field select, .field textarea {
                font-size: 1.6em;
                padding: .162em .15em;
                border: .0625em solid #BCC6D0;
                background-color: #fff;
                border-radius: .1875em;
                box-shadow: 0 0.125em 0.125em #DDDDDD inset;
                color: #4F4F4F;
                outline: 0!important;
            }
         </style>

         <div class="container" id="pagando">
            <div class="row">
               <div class="buscaform">
                  <div class="col-xs-12 col-md-12">
                     <section id="all">
                        <aside>
                           <div id="cart" class="cart">
                              <h2>Resumo da compra</h2>
                              <h3>Autorizo a instituição <?php echo $syndicate['name']; ?> a debitar, imediatamente o valor abaixo em meu cartão.</h3>
                              <table>
                                 <thead>
                                    <tr>
                                       <th>Descrição</th>
                                       <th>Valor</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <h3 title="Boné lettering">Filiação Sindical <?php echo $syndicate['name']; ?></h3>
                                          Quantidade: 1<br>
                                          Valor do item: R$ <?php echo number_format((float)$syndicate['valor'], 2, '.', ''); ?><br>
                                       </td>
                                       <td>R$<strong><?php echo number_format((float)$syndicate['valor'], 2, '.', ''); ?></strong></td>
                                    </tr>
                                 </tbody>
                                 <tfoot>
                                    <tr id="totalRow">
                                       <td>
                                          <h3>Total a pagar</h3>
                                       </td>
                                       <td>
                                          <span id="cartTotalAmount">R$<strong><?php echo number_format((float)$syndicate['valor'], 2, '.', ''); ?></strong></span>
                                       </td>
                                    </tr>
                                 </tfoot>
                              </table>
                           </div>
                        </aside>
                        <div id="content">
                           <div class="sms-component--msg-anchor"></div>
                           <form id="payment" action="<?php //echo home_url('filiar'); ?>" method="post" class="all tsmcustom">

                              <input type="hidden" id="employee_id" name="employee_id" value="<?php if (!is_null($employee['id'])) { echo $employee['id']; } ?>" />
                              <input type="hidden" id="employee_name" name="employee_name" value="<?php if (!is_null($employee['name'])) { echo $employee['name']; } ?>" />
                              <input type="hidden" id="employee_doc" name="employee_doc" value="<?php if (!is_null($employee['doc'])) { echo $employee['doc']; } ?>" />
                              <input type="hidden" id="employee_birthDate" name="employee_birthDate" value="<?php if (!is_null($employee['birthDate'])) { echo $employee['birthDate']; } ?>" />
                              <input type="hidden" id="employer_id" name="employer_id" value="<?php if (!is_null($employer['id'])) { echo $employer['id']; } ?>" />
                              <input type="hidden" id="employer_name" name="employer_name" value="<?php if (!is_null($employer['name'])) { echo $employer['name']; } ?>" />
                              <input type="hidden" id="employer_doc" name="employer_doc" value="<?php if (!is_null($employer['doc'])) { echo $employer['doc']; } ?>" />

                              <input type="hidden" id="employer_cep" name="employer_cep" value="<?php if (!is_null($employer['cep'])) { echo $employer['cep']; } ?>" />
                              <input type="hidden" id="employer_street" name="employer_street" value="<?php if (!is_null($employer['street'])) { echo $employer['street']; } ?>" />
                              <input type="hidden" id="employer_number" name="employer_number" value="<?php if (!is_null($employer['number'])) { echo $employer['number']; } ?>" />
                              <input type="hidden" id="employer_complement" name="employer_complement" value="<?php if (!is_null($employer['complement'])) { echo $employer['complement']; } ?>" />
                              <input type="hidden" id="employer_neighborhood" name="employer_neighborhood" value="<?php if (!is_null($employer['neighborhood'])) { echo $employer['neighborhood']; } ?>" />
                              <input type="hidden" id="employer_city" name="employer_city" value="<?php if (!is_null($employer['city'])) { echo $employer['city']; } ?>" />
                              <input type="hidden" id="employer_uf" name="employer_uf" value="<?php if (!is_null($employer['uf'])) { echo $employer['uf']; } ?>" />

                              <input type="hidden" id="syndicate_id" name="syndicate_id" value="<?php if (!is_null($syndicate['id'])) { echo $syndicate['id']; } ?>" />
                              <input type="hidden" id="syndicate_company" name="syndicate_company" value="<?php if (!is_null($syndicate['company'])) { echo $syndicate['company']; } ?>" />
                              <input type="hidden" id="syndicate_name" name="syndicate_name" value="<?php if (!is_null($syndicate['name'])) { echo $syndicate['name']; } ?>" />
                              <input type="hidden" id="syndicate_doc" name="syndicate_doc" value="<?php if (!is_null($syndicate['doc'])) { echo $syndicate['doc']; } ?>" />
                              <input type="hidden" id="syndicate_email" name="syndicate_email" value="<?php if (!is_null($syndicate['email'])) { echo $syndicate['email']; } ?>" />

                              <input type="hidden" id="syndicate_plano" name="syndicate_plano" value="<?php if (!is_null($syndicate['plano'])) { echo $syndicate['plano']; } ?>" />
                              <input type="hidden" id="syndicate_assinatura" name="syndicate_assinatura" value="<?php if (!is_null($syndicate['assinatura'])) { echo $syndicate['assinatura']; } ?>" />

                              <input type="hidden" id="syndicate_plano_valor" name="syndicate_plano_valor" value="<?php if (!is_null($syndicate['valor'])) { echo number_format((float)$syndicate['valor'], 2, '.', ''); } ?>" />

                              <input type="hidden" id="referencia" name="referencia" value="<?php echo $syndicate['name'].'-'.$employee['doc']; ?>" />


                              <fieldset class="payment-step content" data-omniture-section="Meios de pagamento">
                                 <h1>Forma de Pagamento</h1>
                                 
                                 <div class="creditCardFields">
                                   <div class="step" id="cardDataContainer" step-title="Forma de Pagamento">
                                      <div class="double-columns first-block">
                                        <div id="ccBrandConsult" class="field">
                                             <label for="creditCardNumber">Informe como deseja usar o cartão</label>

                                             <div class="ccBrandConsultWrapper">
                                               <div class="form-single-column formaPagamento" data-component="radio">
                                                 
                                                  <input type="radio" class="form-radio" id="formaPagamentoC" name="formaPagamento[]" value="C" required="" checked="" />
                                                  <label id="label_formaPagamentoC" for="formaPagamentoC">Crédito</label>
                                               
                                                  <input type="radio" class="form-radio" id="formaPagamentoD" name="formaPagamento[]" value="D" required="" />
                                                  <label id="label_formaPagamentoD" for="formaPagamentoD">Débito</label>
                                                
                                              </div>
                                             </div>                                             
                                          </div>
                                      </div>
                                    </div>

                                  </div>

                                 
                                 
                                 <div class="creditCardFields">
                                    
                                    <div class="step subcontent" id="cardDataContainer" step-title="Alterar o cartão">
                                       <h1>Pagamento</h1>
                                       <div class="double-columns first-block">
                                          <div id="ccBrandConsult" class="field">
                                             <label for="creditCardNumber">Número do cartão</label>
                                             <div class="ccBrandConsultWrapper">
                                                <input data-title="Número do cartão" type="text" id="creditCardNumber" value="" maxlength="16" autocomplete="off">
                                                <input type="hidden" id="creditCardToken" value="">
                                                <p class="insertion-guide">APENAS NUMEROS</p>
                                             </div>
                                             
                                          </div>                                          


                                          <div class="field cardDueDate ">
                                             <label for="creditCardDueDate_Month">Data de validade</label>
                                             <input data-title="Data de validade do cartão" type="text" data-autotab-target="creditCardDueDate_Year" placeholder="MM" maxlength="2" class="small" id="creditCardDueDate_Month" required="true">
                                             <input data-title="Data de validade do cartão" type="text" placeholder="AA" maxlength="2" class="small" id="creditCardDueDate_Year" required="true">
                                          </div>

                                          <div class="clear-all"></div>

                                          <div class="field cardHolderName  a">
                                             <label for="creditCardHolderName">
                                             Nome do dono do cartão
                                             </label>
                                             <input data-title="Nome do dono do cartão" class="bigger" type="text" id="creditCardHolderName" required="true" name="holderName">
                                             <p class="insertion-guide">Ex.: CARLOS A F DE OLIVEIRA</p>
                                          </div>

                                          <div class="field cardCvv ">
                                             <label for="creditCardCVV">Código de segurança <a class="cvvDetail questionMark omniture-click" data-omniture-action="Código de segurança (CVV) - Tooltip de ajuda" href="#creditCardCvvInfo" rel="tooltip-0">?</a></label>
                                             <input data-title="Código de segurança do cartão" class="medium" type="text" id="creditCardCVV" required="true" value="" autocomplete="off" maxlength="3">
                                          </div>

                                          
                                       </div>
                                    </div>
                                    <!-- step -->
                                    <div class="clear-all"></div>
                                    <div class="step subcontent" id="cardHolderContainer" step-title="Informar dados do dono do cartão">
                                       <div id="cardHolderDataFields">
                                          <div id="cardHolderData" class="holderData double-columns first-block">
                                             <h1>Dados do dono do cartão</h1>
                                             <div class="holderCPFField field  a ">
                                                <label for="holderCPF">
                                                CPF do dono do cartão
                                                </label>
                                                <input class="bigger" type="text" id="holderCPF" name="holderCPF" required="true" value="" data-title="CPF do dono do cartão" maxlength="14" autocomplete="off">
                                             </div>
                                             <div class="holderPhoneField field ">
                                                <label for="holderAreaCode">
                                                Celular do dono do cartão
                                                </label>
                                                <input class="small" type="text" data-autotab-target="holderPhone" maxlength="2" id="holderAreaCode" name="holderAreaCode" value="" data-title="Celular do dono do cartão">
                                                <input class="big" type="text" maxlength="9" id="holderPhone" required="true" name="holderPhone" value="" data-title="Celular do dono do cartão">
                                             </div>
                                             <input type="hidden" name="holderCanEditPhone" value="true">
                                             <div class="holderBornDateField field  a ">
                                                <label for="holderBornDate">
                                                Data de nascimento
                                                </label>
                                                <input class="medium" type="text" id="holderBornDate" required="true" name="holderBornDate" value="" data-title="Data de nascimento">
                                                <p class="insertion-guide">Ex.: 20/05/1980</p>
                                             </div>
                                             <div class="clear-all"></div>
                                          </div>
                                       </div>
                                      
                                      
                                       <div class="clear-all"></div>
                                    </div>
                                    <!-- step -->
                                 </div>
                                 
                              </fieldset>
                              <div id="mainAction" class="content">
                                 

                                 <div id="progressLoader" class="loadBox">
                                    <div id="progress-message" class="uolMsg uolMsg-medium uolMsg-loading ">
                                       <h3>Processando o seu pagamento. Aguarde...</h3>
                                    </div>
                                    <div id="credicCardProgress">
                                       <div class="bar">
                                          <div class="pct themeBgColor themeTextColor" style="width:0%;" data-progress="0"></div>
                                       </div>
                                       <p><strong class="rounded themeTextColor">0</strong></p>
                                    </div>
                                 </div>

                                 <button type="button" id="continueToPayment" class="pagseguro-button mainActionButton large">Confirmar o pagamento</button>
                                 <span id="avisoAssinatura" style="color: red;text-align: center;width: 100%;float: left;font-size: 12px;font-weight: bold; display: none;"><br>POR FAVOR, PREENCHA OS CAMPOS ACIMA CORRETAMENTE
                                  <br><p id="erroApi"></p>
                                 </span>
                                 <p class="mainActionObs">Seu pagamento passará por uma análise interna e estará sujeito à confirmação feita por telefone ou e-mail. Você sempre será notificado, por e-mail, sobre qualquer alteração no fluxo de processamento do seu pedido.</p>

                              </div>
                           </form>
                        </div>
                     </section>
                  </div>
               </div>
            </div>
        </div>

        <script type="text/javascript" src="<?php echo URL_STC; ?>/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>

        <?php
          $curl = curl_init();
        
          curl_setopt_array($curl, array(
              CURLOPT_URL => URL_WS."/v2/sessions?email=".EMAIL_PGS."&token=".TOKEN_PGS,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => ""
          ));

          $getSession = curl_exec($curl);
          $err = curl_error($curl);
          $sessionID = "";

          curl_close($curl);

          if ($err) {
              // echo "cURL Error #:" . $err;
          } else {
             $xmlSession = simplexml_load_string($getSession);
             $sessionID = (string)$xmlSession->id;
          }

        ?>

        <script type="text/javascript">  
          PagSeguroDirectPayment.setSessionId('<?php echo $sessionID; ?>');

                  var bandeiraCartao = "";
                  var card_Token = "";
                  var hash = "";
                  var codigoAssinatura = "";

                  PagSeguroDirectPayment.onSenderHashReady(function(response){                      
                      hash = response.senderHash; 
                  });

                  function get_Brand(cardBin) {
                      
                      PagSeguroDirectPayment.getBrand({
                          cardBin: cardBin,
                          success: function(response) {
                            bandeiraCartao = response.brand.name;
                            $('#creditCardToken').val(bandeiraCartao);
                          },
                          error: function(response) {
                            //tratamento do erro
                          },
                          complete: function(response) {
                            //tratamento comum para todas chamadas
                          }
                      });
                  }

                  function cardToken(callback) {
                      PagSeguroDirectPayment.createCardToken({
                          cardNumber: $("#creditCardNumber").val(), // Número do cartão de crédito
                          brand: bandeiraCartao, // Bandeira do cartão bandeiraCartao
                          cvv: $("#creditCardCVV").val(), // CVV do cartão
                          expirationMonth: $("#creditCardDueDate_Month").val(), // Mês da expiração do cartão
                          expirationYear: '20'+$("#creditCardDueDate_Year").val(), // Ano da expiração do cartão, é necessário os 4 dígitos.
                          success: function(response) {
                              // Retorna o cartão tokenizado.
                              card_Token = response.card.token;
                              //console.dir(card_Token);
                              callback();
                              // 
                          },
                          error: function(response) {
                            // Callback para chamadas que falharam.
                            console.log("error: ");
                            console.dir(response);

                            $("#erroApi").text('Cartão de Crédito Inválido');
                            $("#erroApi").show();
                            $("#avisoAssinatura").show();
                            $("#progressLoader").hide();
                          },
                          complete: function(response) {
                              
                          }
                      });
                  }                


                  function efetivaTransacao () {
                      console.log("efetivaTransacao");
                      $.post("<?php echo home_url('cadastro');?>",
                      {  
                          // email: $("#email").val(), 
                          // areaCode: $("#ddd").val(),
                          // numCelular: $("#numCelular").val(),
                          // street: $("#logradouro").val(),
                          // number: $("#numero").val(),
                          // complement: $("#comp").val(),
                          // district: $("#bairro").val(),
                          // city: $("#cidade").val(),
                          // state: $("#uf").val(),
                          // country: 'BRA',
                          // postalCode: $("#cep").val(),

                          pagar: 'pagar',
                          plan: $("#syndicate_assinatura").val(),
                          reference: $("#referencia").val(),
                          nomePessoa: $("#employee_name").val(),                          
                          ip: $("#numip").val(),

                          
                          valueCPF: $("#employee_doc").val(),
                          paymentMethodBirthDate: $("#holderBornDate").val(),
                          paymentMethodCPF: $("#holderCPF").val(),
                          paymentName: $("#creditCardHolderName").val(),
                          sindicatonome: $("#syndicate_name").val(),
                          valorplano: $("#syndicate_plano_valor").val(),
                          syndicate_plano: $("#syndicate_plano").val(),
                          flag: $('#creditCardToken').val(),
                          creditCardNumber: $("#creditCardNumber").val(), 
                          paymentMethodType: $('input[name=formaPagamento]:checked').val(),
                          cvv: $("#creditCardCVV").val(),
                          validate: $("#creditCardDueDate_Month").val() +'/20'+$("#creditCardDueDate_Year").val(),
                          valor: $("#syndicate_plano_valor").val()
                      },
                          function(data){
                              console.log(data); 
                              $("#progressLoader").hide();
                              codigoAssinatura = data; 
                              console.log(codigoAssinatura);
                              if(codigoAssinatura.indexOf("sucesso_") >= 0) { 
                                $("#pagando").hide();
                                $("#finalizacao").show(); 
                                $("#idAssinatura").val(codigoAssinatura);
                                $('form#payment').hide();
                              } else {
                                $("#erroApi").text(data);
                                $("#erroApi").show();
                                $("#avisoAssinatura").show();
                              }

                                                  
                          }
                      )
                  }                  
            

                  // $('#continueToPayment').on( "click", function(e) {     
                  //     e.preventDefault();
                  //     $("#erroApi").hide();
                  //     $("#avisoAssinatura").hide();
                  //     $("#progressLoader").show();
                  //     cardToken(
                  //         function () { 
                  //             efetivaTransacao(); 
                  //         }
                  //     );                                               
                  // });  

                  $('#creditCardNumber').on('input',function(e){
                      var numeroCartao = $('#creditCardNumber').val();

                      if(numeroCartao.length == 6){
                          get_Brand(numeroCartao);
                      }
                  });

                  $('#continueToPayment').on( "click", function(e){


                      var required = $('[required="true"]'); // change to [required] if not using true option as part of the attribute as it is not really needed.
                      var error = false;

                      for(var i = 0; i <= (required.length - 1);i++)
                      {
                        if(required[i].value == '') // tests that each required value does not equal blank, you could put in more stringent checks here if you wish.
                        {
                            required[i].style.backgroundColor = 'rgb(255,155,155)';
                            error = true; // if any inputs fail validation then the error variable will be set to true;     
                        }
                      }

                      if(error) // if error is true;
                      {
                          $("#avisoAssinatura").show();
                            return false; // stop the form from being submitted.
                      } else {
                          e.preventDefault();
                          $("#erroApi").hide();
                          $("#avisoAssinatura").hide();
                          $("#progressLoader").show();
                          cardToken(
                              function () { 
                                  efetivaTransacao(); 
                              }
                          );
                      }                            
                  });  
              </script>
    <?php } ?>
<!--******************** FIM CONTEUDO ********************-->
<?php
	get_footer();
?>