<?php
/*
  Template Name: Search Page
 */
?>
<?php
	get_header();
?>
<!--******************** INICIO CONTEUDO ********************-->
<div id="content">
    
    <div id="noticias">  


    		<h1 class="titulo">Resultados da busca</h1>
            <h2>Foram encontadas
                <?php
                    $allsearch = &new WP_Query("s=$s&showposts=-1");
                    $key = esc_html($s, 1);
                    $count = $allsearch->post_count;
                    echo $count . ' '; _e('ocorrências na pesquisa de: ');
                    _e('<span>');
                    echo $key; _e('</span>');
                    wp_reset_query();
                ?>
            </h2>
            <br />
            <p>
            <?php                                
                $posts=query_posts($query_string . '&posts_per_page=-1'); 
                if (have_posts()) : 
                	while (have_posts()) : the_post();  
			        get_template_part( 'content', get_post_format() );
			        ?>			                
			        <p> <a href="<?php the_permalink();?>"> <?php echo the_title();?></a></p>			    
				    <?php endwhile; ?> 
				    <?php else : ?>
				    <article id="post-0" class="post no-results not-found">
				        <header class="entry-header">
				            <h1 class="entry-title"><?php _e( 'Não encontrado', 'twentyeleven' ); ?></h1>
				        </header><!-- .entry-header -->
				        <div class="entry-content">
				            <p><?php _e( 'Desculpe, mas nada corresponde aos seus critérios de busca. Por favor, tente novamente com algumas palavras-chave diferentes.', 'twentyeleven' ); ?></p>
				            <?php get_search_form(); ?>
				        </div><!-- .entry-content -->
				    </article><!-- #post-0 -->
				<?php endif; ?>
			</p>				             
    </div>
    
</div>
<!--******************** FIM CONTEUDO ********************-->
<!--******************** FIM CONTEUDO ********************-->


<?php
	get_footer();
?>