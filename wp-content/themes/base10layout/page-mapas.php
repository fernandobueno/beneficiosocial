<?php
    if(isset($_REQUEST['getDistancia'])){
        header('Content-Type: application/json; charset=utf-8');

        header('Access-Control-Allow-Origin: *'); 

        $centerLat = $_REQUEST['centerLat'];
        $centerLong = $_REQUEST['centerLong'];
        $latitude = $_REQUEST['latitude'];
        $longitude = $_REQUEST['longitude'];

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$centerLat.",".$centerLong."&destinations=".$latitude.",".$longitude."&mode=driving&language=pt-BR&sensor=false";

        var_dump($url);

        $ch = curl_init();

        //$headers    = [];
        //$headers[]  = 'Authorization : '.$token;

        $headr = array();
        $headr[] = 'Content-length: 0';
        $headr[] = 'Content-type: application/json';   

        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_HTTPHEADER,$headr);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 0);        
        //curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_NTLM );
        //curl_setopt( $ch, CURLOPT_USERPWD, 'DOMAIN\\'.userERP.':'.passERP.'' );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 120 );
        //curl_setopt( $ch, CURLOPT_VERBOSE, true );

        $response = curl_exec($ch);

        curl_close($ch);        
       
        if($response === FALSE){
            die(curl_error($ch));
        }      

        echo $response;
    }

    if(isset($_REQUEST['geraRecolhimento'])){
        header('Content-Type: application/json; charset=utf-8');

        header('Access-Control-Allow-Origin: *'); 

        $cnpj = $_REQUEST['cnpj'];
        $token = $_REQUEST['token'];

        $postData = array(
            'token'=>$token
        );

        $urlapi = 'https://cobranca.bsfonline.com.br/api-v1/integracao/'.$cnpj.'/recolhimento';

        // Setup cURL
       
        $ch = curl_init();

        //$headers    = [];
        //$headers[]  = 'Authorization : '.$token;

        $headr = array();
        $headr[] = 'Content-length: 0';
        $headr[] = 'Content-type: application/json';
        $headr[] = 'Authorization: Bearer '.$token;       

        curl_setopt( $ch, CURLOPT_URL, $urlapi );
        curl_setopt( $ch, CURLOPT_HTTPHEADER,$headr);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        //curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_NTLM );
        //curl_setopt( $ch, CURLOPT_USERPWD, 'DOMAIN\\'.userERP.':'.passERP.'' );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 120 );
        //curl_setopt( $ch, CURLOPT_VERBOSE, true );

        $response = curl_exec($ch);

        //print_r(curl_getinfo($ch));
        //echo curl_error($ch);
        curl_close($ch);

        
        // Send the request
        //$response = curl_exec($ch);

        //var_dump($response);

        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }
        
        $responseData = json_decode($response, TRUE);

        //var_dump($response);

        $contador = 0;

        echo $response;
    }

if( !isset($_REQUEST['getDistancia']) && !isset($_REQUEST['geraRecolhimento']) ){
    get_header();
?>
<!--******************** INICIO CONTEUDO ********************-->



    <div class="container">
        <div class="row">
            <div class="busca">
                <div class="col-xs-12 col-md-12">
                                
                  <div class="col-xs-12 col-md-12">
                    <p class="titulo">SELECIONE O SINDICATO</p>
                  </div>
                </div>

                <div class="col-xs-12 col-md-12">
                   

                <?php 
                    if(isset($_REQUEST['sigla']))
                     $sigla = $_REQUEST['sigla'];
                    else
                     $sigla = "";

                    $viratela = false;

                    $postData = array(
                        'sigla'=>$sigla
                    );

                    // Setup cURL
                    $ch = curl_init('https://cobranca.bsfonline.com.br/api-v1/integracao/auth');
                    curl_setopt_array($ch, array(
                        CURLOPT_POST => TRUE,
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/json'
                        ),
                        CURLOPT_POSTFIELDS => json_encode($postData)
                    ));

                    // Send the request
                    $response = curl_exec($ch);

                    // Check for errors
                    if($response === FALSE){
                        die(curl_error($ch));
                    }
                    
                    $responseData = json_decode($response, TRUE);

                    $contador = 0;

                    foreach ($responseData as $_responseData) {                        
                        ?>

                            <form class="pure-form pure-form-stacked formHome" action="<?php echo home_url('mapas/gmaps'); ?>" id="form_<?php echo $contador; ?>" method="post">
                                <label><a href="" id="submit_<?php echo $contador; ?>"><?php echo $_responseData['entd']['sigla']; ?></a></label>
                                <input type="hidden" id="token" name="token" value="<?php echo $_responseData['token']; ?>" />
                            </form>                            

                            <script type="text/javascript">
                                $('#submit_<?php echo $contador; ?>').click(function(event) {
                                    event.preventDefault();
                                    $('#form_<?php echo $contador; ?>').submit(); 
                                });
                            </script>
                        <?php
                        $contador++;
                     } 
                     
                   
                ?>    
                </div>                         
            </div>
        </div>
    </div>

?>
<!--******************** FIM CONTEUDO ********************-->
<?php
    get_footer();
    }
?>