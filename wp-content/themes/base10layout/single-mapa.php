<?php

//print_r($array_data_utf8);

?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf8"/>
        <title>Google Maps</title>

        <script>window.jQuery || document.write('<script src="<?php bloginfo('template_directory')?>/js/jquery-1.10.2.min.js"><\/script>')</script>
<?php
    //the_post();

    $idPost = get_the_ID(); 
?>
<!--******************** INICIO CONTEUDO ********************-->
<style type="text/css"> 
            #map{
                overflow:hidden;
                padding-bottom:56.25%;
                position:relative;
                height:100%;
            }
            #map iframe{
                left:0;
                top:0;
                height:100%;
                width:100%;
                position:absolute;
            }

            #infoPanel {
                float: left;
                margin-left: 10px;
            }
            #infoPanel div {
                margin-bottom: 5px;
            }
            #block {
                margin-bottom:10px;
                background: #cccccc;
                padding:4px;
            }

            .gm-style-iw {
                width: 290px !important;
                top: 15px !important;
                left: 60px !important;
                background-color: #fff;
                box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
                border: 1px solid rgba(72, 181, 233, 0.6);
                border-radius: 2px 2px 10px 10px;
            }
            #iw-container {
                margin-bottom: 10px;
            }
            #iw-container .iw-title {
                font-family: 'Open Sans Condensed', sans-serif;
                font-size: 22px;
                font-weight: 400;
                padding: 10px;
                background-color: #48b5e9;
                color: white;
                margin: 0;
                border-radius: 2px 2px 0 0;
            }
            #iw-container .iw-content {
                font-size: 13px;
                line-height: 18px;
                font-weight: 400;
                margin-right: 1px;
                padding: 15px 5px 20px 15px;
                max-height: 140px;
                overflow-y: auto;
                overflow-x: hidden;
            }
            .iw-content img {
                float: right;
                margin: 0 5px 5px 10px; 
            }
            .iw-subTitle {
                font-size: 16px;
                font-weight: 700;
                padding: 5px 0;
            }
            .iw-bottom-gradient {
                position: absolute;
                width: 268px;
                height: 25px;
                bottom: 10px;
                right: 18px;
                background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
                background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
                background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
                background: -ms-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
            }
        </style> 
        <script src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBCnPBV5cJPNNJsstg9BUqZOhTEOa7O3PE" type="text/javascript"></script>
        <script type="text/javascript">
        //<![CDATA[
        
        //Array com novos marcadores
        var markersData = new Object();     
        
        //Variáveis globais
        var map;
        var infoWindow;
        
        function load() {
    
            //Configurações do mapa
            map = new google.maps.Map(document.getElementById("map"), {
                center: new google.maps.LatLng(-25.809061, -50.755800),
                zoom: 5,
                mapTypeId: 'roadmap'
            });

            var legend = document.getElementById('mapLegend');
            var div = document.createElement('div');
            div.innerHTML = '<span><img width="195" src="<?php bloginfo('template_directory')?>/img/marcadagua.png"></span>';
            legend.appendChild(div);
            /* Push Legend to Right Top */
            map.controls[google.maps.ControlPosition.RIGHT_TOP].push(legend);
        
            //Janela de informações
            infoWindow = new google.maps.InfoWindow({maxWidth: 350 });

            //Carrega o XML com as coordenadas salvas no banco de dados
            downloadUrl("<?php echo home_url('gerarxml'); ?>?idPost=<?php echo $idPost; ?>", function(data) {
                var xml = data.responseXML;
     
                var markers = xml.documentElement.getElementsByTagName("marker");

                var cont = 0;
                
                //Cria os marcadores
                for (var i = 0; i < markers.length; i++) {

                    var name = markers[i].getAttribute("name");
                    var address = markers[i].getAttribute("address");
                    var type = markers[i].getAttribute("type");
                    var regiao = markers[i].getAttribute("regiao");
                    var trabalhadores = markers[i].getAttribute("trabalhadores");


                    var geocoder = new google.maps.Geocoder();

                    var latitude = 0;                   
                    var longitude = 0;



                    geocoder.geocode({'address':address}, function (results,status){

                      if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                        wait = true;
                        setTimeout("wait = true", 2);
                        i--;
                      }
                      else {
                        latitude = results[0].geometry.location.lat();
                        longitude = results[0].geometry.location.lng();

                        if(latitude != 0){
                            var point = new google.maps.LatLng(
                                parseFloat(latitude),
                                parseFloat(longitude)
                            );


                            // InfoWindow content
                              var html = '<div id="iw-container">' +
                                                '<div class="iw-title">' + markers[cont].getAttribute("name") + '</div>' +
                                                '<div class="iw-content">' +
                                                  '<div class="iw-subTitle">'+markers[cont].getAttribute("trabalhadores")+' FUNCIONÁRIOS</div>' +
                                                  '<p>REGIÃO '+markers[cont].getAttribute("regiao") +'</p>' +
                                                  '<div class="iw-subTitle">Contato</div>' +
                                                  '<p>'+ markers[cont].getAttribute("address") +'<br>'+
                                                  '<br>Tel. +55 11 1234 1235<br>e-mail: teste@teste.com<br>site: www.teste.com</p>'+
                                                '</div>' +
                                                '<div class="iw-bottom-gradient"></div>' +
                                              '</div>';



                            var marker = new google.maps.Marker({
                                map: map,
                                position: point,
                                title: markers[cont].getAttribute("name")
                            });

                            cont++;

                            bindInfoWindow(marker, map, infoWindow, html);

                        }
                        
                      } 

                    }); 

                    
                    
                    
                }
            });

            var iframe = document.getElementsByTagName("IFRAME");
            var i;
            for (i = 0; i < iframe.length; i++) {
                iframe[i].style.height = iframe[i].contentWindow.document.body.scrollHeight + 'px';
            }

        }

        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
          }

        //Inclui a janela de informações ao marcador
        function bindInfoWindow(marker, map, infoWindow, html) {
               

            google.maps.event.addListener(marker, 'click', function() {
                infoWindow.setContent(html);
                infoWindow.open(map, marker);
                
                geocodePosition(false, marker.getPosition());
            });

              // *
              // START INFOWINDOW CUSTOMIZE.
              // The google.maps.event.addListener() event expects
              // the creation of the infowindow HTML structure 'domready'
              // and before the opening of the infowindow, defined styles are applied.
              // *
              google.maps.event.addListener(infoWindow, 'domready', function() {

                // Reference to the DIV that wraps the bottom of infowindow
                var iwOuter = $('.gm-style-iw');

                /* Since this div is in a position prior to .gm-div style-iw.
                 * We use jQuery and create a iwBackground variable,
                 * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
                */
                var iwBackground = iwOuter.prev();

                // Removes background shadow DIV
                iwBackground.children(':nth-child(2)').css({'display' : 'none'});

                // Removes white background DIV
                iwBackground.children(':nth-child(4)').css({'display' : 'none'});

                // Moves the infowindow 115px to the right.
                iwOuter.parent().parent().css({right: '115px'});

                // Moves the shadow of the arrow 76px to the left margin.
                iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

                // Moves the arrow 76px to the left margin.
                iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

                // Changes the desired tail shadow color.
                iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

                // Reference to the div that groups the close button elements.
                var iwCloseBtn = iwOuter.next();

                // Apply the desired effect to the close button
                iwCloseBtn.css({opacity: '1', left: '60px', top: '3px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});

                var iwCloseBtn2 = iwCloseBtn.next();
                iwCloseBtn2.css({opacity: '1', left: '60px', top: '3px'});

                // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
                if($('.iw-content').height() < 140){
                  $('.iw-bottom-gradient').css({display: 'none'});
                }

                // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
                iwCloseBtn.mouseout(function(){
                  $(this).css({opacity: '1'});
                });
              });
        }

        //Parseia o XML
        function downloadUrl(url, callback) {
            var request = window.ActiveXObject ?
                new ActiveXObject('Microsoft.XMLHTTP') :
                new XMLHttpRequest;

            request.onreadystatechange = function() {
                if (request.readyState == 4) {
                    request.onreadystatechange = doNothing;
                    callback(request, request.status);
                }
            };

            request.open('GET', url, true);
            request.send(null);
        }

        function doNothing() {}

        /////////////////////////////////////////////
        // Funções dos Marcadores
        /////////////////////////////////////////////
        
        //Adiciona novo marcador ao mapa
        function newMarker(title, type) {
            
            //Verifica se title ou type não são vazios
            if (!title || !type)
                return false;
                
            //Pega os limites do mapa
            var bounds = map.getBounds();
            
            //Obtém as coordenadas da posição central do mapa
            var center = bounds.getCenter();
            
            //Ícone de acordo com o tipo de marcador
            var icon = customIcons[type] || {};
            
            //Cria um novo marcador
            var marker = new google.maps.Marker({
                position: center,
                title: title,
                map: map,
                icon: icon.icon,
                draggable: true
            });
            
            
            //Grava as informações no array
            markersData[title] = new Object();
            markersData[title]['name'] = title;         
            markersData[title]['type'] = type;
            
            //Obtém o endereço da posição global
            geocodePosition(marker.getTitle(), marker.getPosition());
                      
            //Adiciona os eventos de arrastar ao marcador
            //Estes irão atualizar as informações do painel de informações
             
            //Exibe as informações do marcador selecionado no painel
            google.maps.event.addListener(marker, 'click', function() {
                
                geocodePosition(marker.getTitle(), marker.getPosition());
            });
          
            //Limpa os inputs
            clearInputs();
            
            return false;
        }
        
        //Essa função recebe coordenadas de latitude e longitude
        // e retorna o endereço desta coordenada
        //Se title == false, então não atualiza dados de novos marcadores
        var geocoder = new google.maps.Geocoder();
        function geocodePosition(title, pos) {
            
            if (title) {
                //Grava a latitude e longitude
                markersData[title]['lat'] = pos.lat();
                markersData[title]['lng'] = pos.lng();
            }
            
            //Obtém o endereço das coordenadas
            geocoder.geocode({
                latLng: pos
            }, function(responses) {
                if (responses && responses.length > 0) {
                    
                    if (title)
                        markersData[title]['address'] = responses[0].formatted_address;
                } else {
                    
                    if (title)
                        markersData[title]['address'] = "erro";
                }
            });
        }
             
        
        
        
        /////////////////////////////////////////////
        // Funções de Busca de Endereços
        /////////////////////////////////////////////
        function geocode() {
            var address = document.getElementById("address2").value;
            geocoder.geocode({
                'address': address,
                'partialmatch': true}, geocodeResult);
        }
 
        function geocodeResult(results, status) {
            if (status == 'OK' && results.length > 0) {
                map.fitBounds(results[0].geometry.viewport);
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        }
        
        
        /////////////////////////////////////////////
        // Funções Ajax
        /////////////////////////////////////////////
        
        //Cria um objeto para requisições XML
        function getXMLObject() {
            var xmlHttp = false;
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP")  // For Old Microsoft Browsers
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP")  // For Microsoft IE 6.0+
                }
                catch (e2) {
                    xmlHttp = false;   // No Browser accepts the XMLHTTP Object then false
                }
            }
            if (!xmlHttp && typeof XMLHttpRequest != 'undefined') {
                xmlHttp = new XMLHttpRequest();        //For Mozilla, Opera Browsers
            }
            return xmlHttp;  // Mandatory Statement returning the ajax object created
        }
             
        var xmlhttp = new getXMLObject();
        
        
        
        
        

        //]]>
    </script>


        

</head>

    <!-- Executa a função load() ao carregar o corpo do documento -->
    <body onload="load()">
        <div id="mapLegend">
        </div>
        <!-- Div onde será carregado o mapa -->
        <div id="map"></div>

        
        
    </body>
</html>
