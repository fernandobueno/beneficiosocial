<?php
  require_once 'header.php';
?>
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.php">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Sindicatos</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Sindicatos com planos ativos</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Sindicato</th>
                    <th>CNPJ</th>
                    <th>Plano</th>
                    <th>Data de Criação</th>
                    <th>Controle</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Sindicato</th>
                    <th>CNPJ</th>
                    <th>Plano</th>
                    <th>Data de Criação</th>
                    <th>Controle</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                    $result_planos = "SELECT * FROM Planos_Planos WHERE situacao = 'Ativo'";
                    // $resultado_planos = mysqli_query($conn, $result_planos);
                    // $resultado = mysqli_fetch_assoc($resultado_planos);

                    if ($resultado_planos = mysqli_query($conn, $result_planos)) {
                      while ($row = mysqli_fetch_assoc($resultado_planos)) {

                          echo '<td>'.$row["entidade"].'</td>';
                          echo '<td>'.$row["cnpj"].'</td>';
                          echo '<td>'.$row["id_plano"].'</td>';
                          echo '<td>'.$row["created"].'</td>';
                          echo '<td><a href="assinantes.php?codigoplano='.$row["id_plano"].'" class="btn btn-primary btn-block">VER</a></td>';
                      }
                      mysqli_free_result($resultado_planos);
                    }
                    mysqli_close($conn);
                  ?>

                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Atualizado em <?php echo date('d/m/Y'); ?></div>
        </div>

<?php
  require_once 'footer.php';
?>
