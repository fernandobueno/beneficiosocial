<?php
  require_once 'header.php';
  if (!isset($_SESSION['logado'])) {  
    header("Location: login.php");
  }
?>
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.php">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Sindicatos</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Detalhes do Plano assinado</div>
          <div class="card-body">
            <h3 id="retornoApi"></h3>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>Sindicato</th>
                    <th>Data Adesão</th>
                    <th>Status</th>
                    <th>Data Status</th>
                    <th>Controle</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>Sindicato</th>
                    <th>Data Adesão</th>
                    <th>Status</th>
                    <th>Data Status</th>
                    <th>Controle</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                    $ID_Plano = $_REQUEST["codigoplano"];
                    $result_planos = "SELECT * FROM Planos_Adesao WHERE ID_Adesao = '".$_REQUEST['adesao']."' AND CPF = '".$_SESSION['doc']."'";
                    // $resultado_planos = mysqli_query($conn, $result_planos);
                    // $resultado = mysqli_fetch_assoc($resultado_planos);

                    $DT_Status = "";
                    $Status = "";

                   

                    if ($resultado_planos = mysqli_query($conn, $result_planos)) {
                      while ($row = mysqli_fetch_assoc($resultado_planos)) {
                            
                            $statusplano = "SELECT DT_Status, Status 
                                            FROM Planos_Adesao_Status 
                                            WHERE ID_Planos_Adesao = ".$row["ID_Planos_Adesao"]."
                                            ORDER BY ID_Planos_Adesao_Status DESC";

                                    

                            if ($status_plano = mysqli_query($conn, $statusplano)) {
                              while ($_row = mysqli_fetch_assoc($status_plano)) {
                                $DT_Status = $_row["DT_Status"];
                                $Status = $_row["Status"];
                              }
                              mysqli_free_result($status_plano);
                            }

                          echo '<tr>';
                          echo '<td>'.$row["Nome"].'</td>';
                          echo '<td>'.$row["CPF"].'</td>';
                          echo '<td>'.$row["Sindicato"].'</td>';
                          echo '<td>'.$row["DT_Adesao"].'</td>';
                          echo '<td>'.$Status.'</td>';
                          echo '<td>'.$DT_Status.'</td>';

                          echo '<td><a href="#" id="cancelarAdesao" data-toggle="modal" data-target="#cancelarModal" class="btn btn-primary btn-block">Cancelar Adesão</a><input type="hidden" id="cancelarAdesaoId" value="'.$row["ID_Adesao"].'" /><input type="hidden" id="ID_Planos_Adesao" value="'.$row["ID_Planos_Adesao"].'" /></td>';
                          echo '</tr>';
                      }
                      mysqli_free_result($resultado_planos);
                    }
                    mysqli_close($conn);
                  ?>

                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Atualizado em <?php echo date('d/m/Y'); ?></div>
        </div>

        <!-- Logout Modal-->
        <div class="modal fade" id="cancelarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Deseja mesmo cancelar a adesão?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">Se deseja mesmo encerrar os pagamentos referentes a sua filiação clique em "Sim".</div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Não</button>
                <a class="btn btn-primary" id="cancelar">Sim</a>
              </div>
            </div>
          </div>
        </div>

<?php
  require_once 'footer.php';

//   SELECT Planos_Adesao.Nome
//        ,Planos_Adesao.CPF
//        ,Planos_Adesao.Sindicato
//        ,Planos_Adesao.DT_Adesao
//        ,Planos_Adesao.ID_Adesao
// FROM Planos_Adesao,
// LATERAL (
//     SELECT Planos_Adesao_Status.DT_Status
//          ,Planos_Adesao_Status.Status
//     FROM Planos_Adesao_Status
//     WHERE Planos_Adesao.ID_Planos_Adesao = Planos_Adesao_Status.ID_Planos_Adesao
//     ORDER BYPlanos_Adesao_Status.ID_Planos_Adesao_Satus DESC
//     LIMIT 1
//     ) ORD_HIST

?>

<script type="text/javascript">


  $('#cancelar').on( "click", function(e){
    $.post("http://aplicativoparasindicato.com.br/cadastro",
    {                                        
        
        cancelarAdesao: 'cancelarAdesao',
        idAdesao: $("#cancelarAdesaoId").val(),
        ID_Planos_Adesao: $("#ID_Planos_Adesao").val()
    },
        function(data){
          $("#retornoApi").text(data);
        }
    )
  
  });  
</script>
