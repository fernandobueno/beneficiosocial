<?php
  require_once 'header.php';
  if (!isset($_SESSION['logado'])) {  
    header("Location: login.php");
  }
?>
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.php">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Sindicatos</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Detalhes do Plano assinado</div>
          <div class="card-body">
            <h3 id="retornoApi"></h3>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>Sindicato</th>
                    <th>Data Adesão</th>
                    <th>Controle</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>Sindicato</th>
                    <th>Data Adesão</th>
                    <th>Controle</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                    $ID_Plano = $_REQUEST["codigoplano"];
                    $result_planos = "SELECT * FROM Planos_Adesao WHERE ID_Plano = '".$_SESSION['idPlano']."' AND CPF = '".$_SESSION['doc']."'";
                    // $resultado_planos = mysqli_query($conn, $result_planos);
                    // $resultado = mysqli_fetch_assoc($resultado_planos);

                    if ($resultado_planos = mysqli_query($conn, $result_planos)) {
                      while ($row = mysqli_fetch_assoc($resultado_planos)) {
                          echo '<tr>';
                          echo '<td>'.$row["Nome"].'</td>';
                          echo '<td>'.$row["CPF"].'</td>';
                          echo '<td>'.$row["Sindicato"].'</td>';
                          echo '<td>'.$row["DT_Adesao"].'</td>';
                          echo '<td><a href="assinante-detalhe.php?adesao='.$row["ID_Adesao"].'" class="btn btn-primary btn-block">Detalhes</a></td>';
                          echo '</tr>';
                      }
                      mysqli_free_result($resultado_planos);
                    }
                    mysqli_close($conn);
                  ?>

                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Atualizado em <?php echo date('d/m/Y'); ?></div>
        </div>

<?php
  require_once 'footer.php';
?>
