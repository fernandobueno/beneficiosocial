

       //alert('Geolocalização não suportada em seu navegador.')
       var inicial = {
          latitude: -25.428777,
          longitude: -49.3435829
        };
        var divDoMapa = document.getElementById("map_canvas")
        var opcoes = { 
          center: new google.maps.LatLng(inicial.latitude, inicial.longitude)
          , zoom: 10
          , mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(divDoMapa, opcoes);
   


function localizarUsuario(){
  if (window.navigator && window.navigator.geolocation) {
   var geolocation = window.navigator.geolocation;
   geolocation.getCurrentPosition(sucesso, erro);
  } else {
     alert('Geolocalização não suportada em seu navegador.')
  }
  function sucesso(posicao){
    //console.log(posicao);
    var latitude = posicao.coords.latitude;
    var longitude = posicao.coords.longitude;
    //alert('Sua latitude estimada é: ' + latitude + ' e longitude: ' + longitude )
  }
  function erro(error){
    //console.log(error)
  }
}


var latitude = -25.428777;
var longitude = -49.3435829;
//initialize();
var currentMapCenter = null; 

google.maps.event.addListener(map, 'bounds_changed', function () {
    currentMapCenter = map.getCenter();

    var centerLat = currentMapCenter.lat();
    var centerLong = currentMapCenter.lng();
    //console.log(currentMapCenter);
    $.ajax({
            url: "https://mapeamento.bsfonline.com.br/mapas?centerLat="+centerLat+"&centerLong="+centerLong+"&latitude="+latitude+"&longitude="+longitude+"&getDistancia=getDistancia", 
            type: "GET",   
            dataType: 'json',
            cache: false,
            success: function(response){                          
                console.log(response);                  
            }           
        });   
});



//var movimento = window.navigator.geolocation.watchPosition(function(posicao) {
//   console.log(posicao);
// });

 //para parar de monitorar:
// window.navigator.geolocation.clearWatch(movimento);